\begin{definition}
    Sia $A=(a_{ij}),\ i,j=1,\dots ,n$ una matrice a coefficienti interi. Siano $G_{-1},\ G_0,\ G_1$ gli $\Rb$-spazi vettoriali con basi $(f_i)_{i\le n},\ (h_i)_{i\le n},\ (e_i)_{i\le n}$ rispettivamente. Con una semplice verifica possiamo affermare che le seguenti relazioni definiscono una struttura di algebra di Lie $1$-locale su $\widehat{G}(A):=G_{-1}\oplus G_0\oplus G_1$:
    \begin{align*}
        [e_if_j]=\delta_{ij}h_i,\ \ \ [h_ih_j]=0,\ \ \ [h_ie_j]=A_{ij}e_j,\ \ \ [h_if_j]=-A_{ij}f_j.\\
    \end{align*}
    Denotiamo con $G(A)=\bigoplus G_i$ l'algebra di Lie graduata $1$-minimale su $\widehat{G}(A)$. Chiamiamo $G(A)$ \textit{algebra di Kac-Moody} con \textit{matrice di Cartan} $A$.
\end{definition}
Assumeremo sempre che la matrice di Cartan $A$ abbia le seguenti ulteriori proprietà:
\begin{itemize}
    \item[M1)] $A_{ij}=0$ se e solo se $A_{ji}=0$,
    \item[M2)] Per ogni stringa di indici $i_1,\dots,i_k$ vale che $$A_{i_1i_2}A_{i_2i_3}\cdots A_{i_ki_1}=A_{i_2i_1}A_{i_3i_2}\cdots A_{i_1i_k},$$
    \item[M3)] $A_{ij}\le 0,\ A_{ii}\in\{0,2\}\ \ \forall\ i\ne j.$
\end{itemize}
%\begin{observation}
%Non è restrittivo supporre anche che $A_{ii}\in\{0,2\}$, a meno di effettuare il cambio di generatori $e_i\mapsto 2e_i/A_{ii}$ e $h_i\mapsto 2h_i/A_{ii}$, per ogni $i$ tale che $A_{ii}\ne 0$.
%\end{observation}
\begin{proposition}\label{036}
Il centro $Z$ di $G(A)$ consiste in tutti e soli gli elementi in $G_0$ della forma $\sum a_ih_i$ dove $\sum_i A_{ij}a_i=0\ \ \forall\ j$. Se la matrice $A$ non contiene righe fatte di soli zeri allora $G'(A):=G(A)/Z$, con gradazione indotta, è transitiva.
\end{proposition}
\begin{proof}
Il centro è chiaramente un ideale omogeneo. Per la minimalità di $G(A)$, $Z$ dev'essere contenuto in $\widehat{G}(A)$ altrimenti lo spazio vettoriale generato dai suoi elementi omogenei di grado diverso da $-1,0,1$ sarebbe un ideale non banale che interseca banalmente $\widehat{G}(A)$. Inoltre non può avere termini omogenei di grado $1$, e analogamente di grado $-1$, in quanto per ogni $x=\sum c_ie_i\in G_1$ si ha $[(\sum c_ie_i)f_l]=c_lh_l\ne0\ \ \forall\ l$ tale che $c_l\ne 0$. Quindi $Z\subset G_0$.
Scriviamo quindi $z\in Z$ nella forma $\sum_i a_ih_i$ per opportuni coefficienti $a_i\in \Rb$. La condizione $\sum_iA_{ij}a_i=0\ \ \forall\ j$ è necessaria e sufficiente affinché $[zx]=0\ \ \forall\ x\in G_{-1}\oplus G_0\oplus G_1$. Tuttavia è anche sufficiente affinché $z$ stia nel centro, basta osservare che $[zx_1]=[zx_2]=0\Rightarrow [z[x_1x_2]]=0$ quindi $\Ad z$ agisce banalmente su tutti gli elementi dell'algebra di Lie $G(A)$. Infine riguardo la transitività, si verifica immediatamente che se non ci sono righe nulle in $A$, la parte $1$-locale di $G'(A)$ è transitiva. Per dire che $G'(A)$ è essa stessa transitiva serve un piccolo raffinamento del punto b) della proposizione \eqref{roar}: supponiamo per assurdo che esista un $k>1$ (il caso $k<-1$ è identico) tale che $[xG'_{-1}]=0$ per un qualche $x\in G'_k$. Ma visto che $G'_{k-1}=G_{k-1}$, concludiamo che anche $[xG_{-1}]=0$ e quindi contraddiremmo la minimalità di $G(A)$ considerando il solito ideale $$\sum_{i,j\ge 0}(\Ad G_1)^i(\Ad G_0)^jG_k.$$ 
\end{proof}
\begin{observation}
Essendo $G'(A)$ transitiva, per la proposizione \eqref{roar} è anche minimale sulla sua parte $1$-locale $\frac{\widehat{G}(A)}{Z}$.
\end{observation}
In quello che seguirà assumeremo sempre che $A$ sia indecomponibile (cioè che la relazione di equivalenza generata da $i\sim j$ se $A_{ij}\ne 0$ ha una sola classe): nel caso in cui $A$ si decomponesse in un tot di blocchi $A_1,\dots,A_k$ avremmo che $G(A)\simeq G(A_1)\oplus\dots\oplus G(A_k)$ come algebre di Lie, e potremmo ragionare componente per componente.

\medskip
Ciò che vogliamo fare adesso è costruire una forma simmetrica invariante su $G(A)$ che ci tornerà estremamente utile per studiare molte proprietà di $G(A)$. Grazie alla proposizione \eqref{wowty} ci basta definirne una sulla sua parte $1$-locale $\widehat{G}(A)$.
\begin{proposition}
Sia $A$ una matrice di Cartan indecomponibile di dimensione $n$. Se imponiamo che $(e_i,f_j)=0$ per $i\ne j$ e che $(e_1,f_1)=1$, possiamo estendere univocamente a una forma simmetrica invariante su $\widehat{G}(A)$.

Per il teorema \eqref{wowty}, questa si estende univocamente a una forma invariante $(\ ,\ )$ su $G(A)$.
\end{proposition}
\begin{proof}
Ricordiamo che per definizione di forma invariante si deve necessariamente avere che $(e_i,h_j)=(f_i,h_j)=0\ \ \forall\ i,j\le n$. Per avere l'invarianza occorrerà anche che \begin{align}\label{023}
    (h_i,h_j)=([e_if_i],h_j)=(e_i,[f_ih_j])=A_{ji}(e_i,f_i),\\ (h_i,h_j)=(h_i,[e_jf_j])=([h_ie_j],f_j)=A_{ij}(e_j,f_j).\label{024}
\end{align}Per ogni indice $k$ fissiamo $i_1,i_2,\dots,i_r$ una sequenza di indici con $i_1=1,\ i_r=k$ (esiste per indecomponibilità) tale che $A_{i_s i_{s+1}}\ne 0\ \ \forall s$ (gli indici sono intesi modulo $r$). Allora le equazioni \eqref{023} applicate a coppie di indici consecutivi nella sequenza ci dicono che occorre porre necessariamente $$(e_k,f_k)=\frac{A_{i_2i_1}A_{i_3i_2}\cdots A_{i_{r}i_{r-1}}A_{i_1i_r}}{A_{i_1i_2}A_{i_2i_3}\cdots A_{i_{r-1}i_r}A_{i_ri_1}},$$e da questo i prodotti scalari $(h_i,h_j)$ sono determinati tramite l'equazione \eqref{023}. L'equazione \eqref{024} è soddisfatta dal prodotto scalare così definito: siano infatti $a_1,\dots,a_l$ e $b_1,\dots,b_s$ le due sequenze di indici associate a $i$ e a $j$ rispettivamente. Allora 
\begin{align*}
    (e_i,f_i)=\frac{A_{a_2a_1}A_{a_3a_2}\cdots A_{a_{l}a_{l-1}}A_{a_1a_l}}{A_{a_1a_2}A_{a_2a_3}\cdots A_{a_{l-1}a_l}A_{a_la_1}}\\
    (e_j,f_j)=\frac{A_{b_2b_1}A_{b_3b_2}\cdots A_{b_{r}b_{r-1}}A_{b_1b_r}}{A_{b_1b_2}A_{b_2b_3}\cdots A_{b_{r-1}b_r}A_{b_rb_1}}
\end{align*}
che unite alla proprietà M2) applicata alla sequenza $1=a_1,a_2,\dots,a_l,b_s,b_{s-1},\dots,b_2$ rendono vera \eqref{024}. Per quanto detto si ha che la simmetria e l'invarianza di $(\ ,\ )$ sono verificate per coppie e per terne di elementi della base rispettivamente, quindi concludiamo per bilinearità.
\end{proof}
\begin{observation}
    Nel caso in cui $A$ sia una matrice simmetrica si ottiene che $$(e_i,f_j)=\delta_{ij},\ \ \ \ (h_i,h_j)=A_{ij}\ \ \forall\ i,j\le n.$$
\end{observation}
\begin{proposition}
Il prodotto scalare appena definito ha $Z$ come radicale.
\end{proposition}
\begin{proof}
    Dal momento che $(G_i,G_j)=0\ \ \forall\ i+j\ne 0$, il radicale $R=\bigoplus_{i\in\Zb} R_i$ è chiaramente un'ideale omogeneo. Sia $\sum a_ih_i\in R_0$, ovvero tale che $(\sum a_ih_i, h_j)=0\ \ \forall\ j\le n$. Ricordando che $(h_i,h_j)=A_{ij}(e_j,f_j)$, questo è equivalente a dire che $\sum_i a_iA_{ij}(e_j,f_j)=0\ \ \forall\ j\Leftrightarrow \sum a_iA_{ij}=0\ \ \forall\ j\Leftrightarrow\ \sum a_ih_i\in Z$.
    
    D'altronde anche $R_1$ (e analogamente $R_{-1}$) deve essere banale, in quanto preso un qualsiasi $x$ della forma $\sum a_ie_i$, con un certo $a_j\ne 0$ si ha $(\sum a_ie_i,f_j)=a_j(e_j,f_j)\ne0$.
    
    Adesso abbiamo concluso, in quanto $R/Z$ è il radicale della forma indotta su $G'(A)$, che per quanto dimostrato è un'ideale omogeneo che interseca banalmente la parte $1$-locale. Per minimalità di $G'(A)$, $R/Z$ dev'essere banale.
\end{proof}
\begin{observation}\label{incre}
Sia $\widetilde{G}$ l'algebra di Lie graduata massimale su $\widehat{G}(A)$, quindi con $\widetilde{G}/I=G(A)$ dove $I$ è ideale massimo a intersezione banale con $\widehat{G}(A)$. Allora la precedente osservazione dà una caratterizzazione di $I$ come la parte di grado non nullo del radicale di $(\ ,\ )$, vista come forma su $\widetilde{G}$. 
\end{observation}