Continuiamo sulla strada per caratterizzare ulteriormente $\Delta$.
\begin{proposition}\label{fuyt}
Sia $\alpha\in\Delta$ e $\eps_i$ una radice semplice. Allora esistono $p,q$ interi non negativi tali che $\alpha+k\eps_i\in\Delta$ se e solo $-p\le k\le q$. Inoltre $p-q=\alpha(h_i)$.
\end{proposition}
\begin{proof}
A meno di applicare l'automorfismo \eqref{037} possiamo supporre che $\alpha\in\Delta_+$. Sia $p$ massimo per cui $\alpha - p\eps_i\in\Delta$ (esiste!). Allora detto $S$ l'insieme di tutte le radici della forma $\alpha+k\eps_i$, lo possiamo dividere in \virg{blocchi} di radici consecutive, cioè esistono $-p=p_1\le q_1<p_2\le q_2<\dots<p_j\le q_j$, con $q_l+1\ne p_{l+1}\ \ \forall\ l\le j$, tali che $$S=\bigsqcup_{l=1}^j S_l,\ \text{ dove }\ S_l:=\{\alpha+p_l\eps_i,\alpha+(p_l+1)\eps_i,\dots,\alpha+q_l\eps_i\}.$$
Notiamo che $s_i(\alpha+k\eps_i)=s_i(\alpha)-k\eps_i\ \ \forall\ k\in \Zb$, quindi $s_i$ manda blocchi in blocchi e ne inverte l'ordine (cioè $s_i(S_1)=S_l,s_i(S_2)=S_{j-1},\dots$). Al tempo stesso però ogni spazio della forma $$\bigoplus_{k=p_l}^{q_l}G_{\alpha+k\eps_i}$$ con $l\le j$, è invariante tramite l'azione di $\Ad e_i$ e $\Ad f_i$ (ricordiamo infatti che $G_{\alpha+(q_l+1)\eps_i}=0$ e $G_{\alpha+(p_l-1)\eps_i}=0$ per costruzione), quindi per il lemma \eqref{040} anche gli insiemi $S_l$ devono essere invarianti. Da queste osservazioni segue che $S$ è in realtà costituito dal singolo blocco $\{\alpha-p\eps_i,\alpha-(p-1)\eps_i,\dots,\alpha+q\eps_i\}$. Inoltre, affinché esso sia invariante per azione di $s_i$ (che scambia l'ordine della sequenza) deve valere che $s_i(\alpha-p\eps_i)=\alpha+q\eps_i\Rightarrow \alpha(h_i)\eps_i=(p-q)\eps_i$, e questo conclude quanto volevamo dimostrare. 
\end{proof}

\medskip
%D'ora in poi supporremo per semplicità che una matrice di Cartan $A$ abbia la seguente proprietà aggiuntiva:
%\begin{itemize}
%    \item[M4)]$A_{ii}\ne 0$ (i.e. $A_{ii}=2$) per qualsiasi $i\le n$.
%\end{itemize}
%In particolare per ogni $i\le n$ è definita la riflessione semplice $s_i$.
%
Dato un vettore $u$ in $\Rb^n$, diciamo che $u\ge 0$) se $u_i\ge 0\ \ \forall\ i\le n$. Se vale che $u_i>0\ \ \forall\ i\le n$ (risp. $u_i<0$) diciamo che $u$ è \textit{strettamente positivo} (risp. \textit{strettamente negativo}).

Da ora in poi supporremo che $A_{ii}=2\ \ \forall\ i\le n,$ quindi che $\Pi=\{\eps_i;\ i\le n\}$, che è il caso che ci interessa per i nostri scopi.
\begin{lemma}
    Sia $A$ una matrice di Cartan indecomponibile di dimensione $n$ e $u\in\Rb^n$. Allora vale una e una sola delle seguenti:
    \begin{itemize}
        \item[P)] $A$ non degenere; se $Au\ge0$ allora $u=0$ o $u$ strettamente positivo;
        \item[Z)] $A$ ha rango $n-1$. Esiste $u$ strettamente positivo tale che $Au=0$; se $Au\ge 0$ allora $Au=0$;
        \item[N)] Esiste $u$ strettamente positivo con $Au$ strettamente negativo; se $u\ge 0$ e $Au\ge 0$ allora $u=0$.
    \end{itemize}
    A seconda dei casi diremo rispettivamente che $A$ è di tipo positivo, di tipo zero o di tipo negativo.
\end{lemma}
\begin{proof}
    La dimostrazione non è complessa, ma la omettiamo per non essere troppo dispersivi, rimandando a \cite{[9]} (Teorema 3 e Lemma 13) per una trattazione completa. Peraltro è possibile dare una semplice caratterizzazione per le matrici di tipo positivo e di tipo zero (nel caso simmetrico saltano fuori proprio i grafi di Dynkin e i grafi Euclidei).
\end{proof}

Introduciamo la seguente notazione:
\begin{itemize}
    \item[]$\Delta^{re}=W(A)\cdot\Pi$ è detto insieme delle \textit{radici reali} di $G(A)$, mentre $\Delta_+^{re}=\Delta^{re}\cap\Gamma_+$ sono le radici reali positive.
    \item[]$\Delta^{im}=\Delta\setminus\Delta^{re}$ è detto insieme delle \textit{radici immaginarie}, mentre $\Delta_+^{im}=\Delta_+\setminus\Delta_+^{re}$ sono le radici immaginarie positive.
\end{itemize}
    Si osservi che essendo $\Delta$ e $\Delta^{re}$ invarianti tramite $W(A)$ e tramite $\alpha\mapsto -\alpha$, anche $\Delta^{im}$ dovrà necessariamente esserlo.
\begin{proposition}\label{6ytt}
    Se $\alpha\in\Delta^{re}$ allora $\Qb\alpha\cap\alpha=\{\pm\alpha\}$.
\end{proposition}
\begin{proof}
    Sia $w$ tale che $w(\alpha)=\eps_i$. Segue direttamente dalla definizione degli spazi radice che $\Qb\eps_i$ contiene come uniche radici $\pm\eps_i$. Si conclude per linearità di $w$.
\end{proof}
\begin{proposition}
    $\Delta_+\setminus\{\eps_i\}$ è invariante per $s_i$ riflessione semplice. Di conseguenza $\Delta_+^{im}$ è invariante per l'azione di $W(A)$.    
\end{proposition}
\begin{proof}
    Quando applico la riflessione semplice $s_i$ a $\Delta_+$, devo ottenere altre radici, quindi elementi in $\Gamma_+$ o in $\Gamma_-$. Una radice positiva $\alpha\in\Delta_+\setminus\Qb\eps_i$ della forma $\sum_jk_j\eps_j$ ha almeno un $k_j>0$ con $j\ne i$, di conseguenza $s_i(\alpha)=(k_i-\alpha(h_i)\eps_i)+\sum_{j\ne i}k_j\eps_j$ non può stare in $\Delta_-$, quindi continua a stare in $\Delta_+\setminus\Qb\eps_i$.
    
    Quanto appena mostrato, unito al fatto che $\Delta^{im}$ è invariante tramite $W(A)$, permette di dire che $W(A)\cdot \Delta_+^{im}=\Delta_+^{im}$.
\end{proof}
\begin{proposition}\label{yeaaa}
    Sia $\alpha$ una qualsiasi radice positiva non in $\Pi$. Allora esiste $\beta\in\Pi$ tale che $\alpha-\beta\in\Delta_+$.
\end{proposition}
\begin{proof}
    Ricordiamo che $\alpha$ radice positiva significa $G_\alpha$ non banale; segue perciò dalla definizione di $G_\alpha$ che esiste un elemento $[e_{i_1}e_{i_2}\cdots e_{i_k}]\ne 0$, con $\sum_{l\le k}\eps_{i_l}=\alpha$ (quindi $k=|\alpha|>1$). Ma allora $[e_{i_1}e_{i_2}\cdots e_{i_{k-1}}]\ne 0$, da cui $\alpha-\eps_{i_{k}}\in\Delta_+$.
\end{proof}
\begin{proposition}\label{yeaaaa}
    Se $A$ è di tipo zero o negativo allora per qualsiasi $\alpha\in\Delta_+$ esiste $\beta\in\Pi$ tale che $\alpha+\beta\in\Delta_+$.
\end{proposition}
\begin{proof}
    Denotiamo con $p_i$ e $q_i$ gli interi non negativi per cui $\alpha+k\eps_i\in\Delta_+\Leftrightarrow -p_i<k<q_i$, che per la proposizione \eqref{fuyt} sappiamo essere tali che $p_i-q_i=\alpha(h_i)$. Supponiamo per assurdo che $q_i=0\ \ \forall\ i\le n$. Ma allora $\alpha(h_i)\ge 0\ \ \forall\ i$, in altre parole $A\alpha\ge 0$. Se $A$ è di tipo negativo questo già porterebbe a un assurdo in quanto significherebbe che $\alpha=0$. Se $A$ è di tipo zero otteniamo che $\alpha(h_i)= 0\ \ \forall\ i\le n$. Ma per la proposizione precedente esiste $j$ tale che $\alpha-\eps_j\in\Delta_+$, cioè tale che $p_j\ge 1$. Di conseguenza $q_j\ge 1$, e questo conclude.
\end{proof}
\begin{definition}
    Il \textit{diagramma di Dynkin-Coxeter} associato ad $A$ è il grafo senza loop $S(A)$ con $n$ vertici (numerati da $1$ a $n$) dove ogni coppia di vertici distinti $i,j$ è collegata da un numero di archi pari a $\min(A_{ij},A_{ji})$. Per poter risalire ad $A$ da $S(A)$, se $A_{ij}\ne A_{ji}$ etichettiamo gli archi tra $i$ e $j$ con la coppia $(A_{ij},A_{ji})$.
    
    Il supporto di $\alpha\in\Gamma$ si definisce associando al vertice $i\in S(A)$ il valore $\alpha_i$, e considerando il sottografo completo con vertici di valore non nullo.
\end{definition}
\begin{observation}\label{absurd}
    Sia $\alpha\in\Delta$. Allora il supporto di $\alpha$ in $S(A)$ è connesso.
\end{observation}
\begin{proof}
    Sia $A'$ il minore di $A$ dove consideriamo solo le posizioni $(i,j)$ con $\alpha_i,\alpha_j\ne 0$. Supponiamo per assurdo che $\Supp \alpha$ non sia connesso, cioè che $A'$ sia decomponibile. Allora posso decomporre $G(A')$ in $G(B)\oplus G(C)$ con $B$ e $C$ minori complementari in $A'$. Segue che $$G(A')=\bigoplus_{\beta_\in\Delta_{B}} G_{\beta}(B)\oplus\bigoplus_{\gamma\in\Delta_C}G_\gamma(C)=\bigoplus_{\beta\in\Delta_B\cup\Delta_C}G_\beta(A'),$$il che porta a dire che nella decomposizione di $G(A')$ in spazi radice non compare il termine $G_\alpha(A')$, che quindi deve essere per forza nullo. Segue che dev'essere nullo $G_\alpha(A)$, per \eqref{embei}.
\end{proof}
\begin{definition}
    Chiamiamo \textit{camera fondamentale} l'insieme $M$ costituito da elementi $\alpha\in\Gamma_+$ con supporto connesso in $S(A)$ e con $\alpha(h_i)\le 0\ \ \forall\ i\le n$.
\end{definition}
\begin{proposition}\label{9344}
    Se $\alpha\in\Delta_+^{im}$ allora $W(A)\cdot \alpha$ contiene un elemento di $M$.
\end{proposition}
\begin{proof}
    Sappiamo già che $W(A)\cdot\alpha\subset\Delta_+^{im}$. Sia $\beta\in W(A)\cdot\alpha$ di altezza minima (ricordiamo che l'altezza di $\beta$ positivo della forma $\sum k_j\eps_j$ è $|\beta|=\sum_j k_j$). Deduciamo che $\beta(h_i)\le 0\ \ \forall\ i\le n$, altrimenti applicando una riflessione semplice abbasseremmo l'altezza. Inoltre il supporto di $\beta$ in $S(A)$ è chiaramente connesso per l'osservazione \eqref{absurd}.
\end{proof}
\begin{observation}
    Se $A$ è di tipo positivo la camera fondamentale è necessariamente vuota. Segue che non esistono radici immaginarie positive, e quindi neanche negative.
\end{observation}
\begin{proposition}\label{9345}
    Sia $A$ una matrice di Cartan di tipo zero o negativo. Allora la camera fondamentale $M$ è interamente contenuta in $\Delta_+^{im}$.
\end{proposition}
\begin{proof}
    Fissiamo $\alpha=\sum k_i\eps_i\in M$, e dimostriamo innanzitutto che $\alpha\in \Delta_+$. Sia $\mathcal{R}=\{\gamma\in\Delta_+;\ \alpha-\gamma\ge0\}$ (non vuoto in quanto contiene almeno una radice semplice). Chiamiamo $\beta=\sum m_i\eps_i$ un elemento di altezza massima in $\mathcal{R}$. Se per assurdo $\alpha\not\in\Delta_+$ si ha $\alpha-\beta\in\Gamma_+$. Per la proposizione \eqref{yeaaaa} l'insieme di vertici $$P=\{i:\ k_i=m_i\}$$ è non vuoto. Inoltre, $\beta+\eps_i\not\in\Delta_+$ se $k_i>m_i$ (altrimenti $\beta$ non avrebbe altezza massima), che implica $\beta(h_i)\ge 0$ se $k_i>m_i$. Fissiamo una componente connessa $Q$ del grafo ottenuto da $S(A)$ eliminando tutti i vertici in $P$ e gli archi che li coinvolgono. Si ponga $$\beta=\sum_{i\in Q} m_i\eps_i;\ \ \ \ \ \ \beta''=\beta-\beta'.$$Avendo $\beta''$ supporto fuori da $Q$ possiamo dire che $\beta''(h_i)\le0\ \ \forall\ i\in Q$. Questo, unito al fatto che $\beta(h_i)\ge 0\ \ \forall\ i\in Q$ ci porta a dire che $\beta'(h_i)\ge0\ \ \forall\ i\in Q$, ci fa escludere quindi che $Q$ sia di tipo negativo. Se $Q$ fosse di tipo zero allora $\beta'(h_i)=0\ \ \forall\ i\in Q$ comporta che $\beta''(h_i)=0\ \ \forall\ i\in Q$. Questo è assurdo in quanto, scelto un vertice $j$ di $Q$ che sia collegato a un vertice in $P$ tramite un arco (un tale $j$ esiste!) si deve per forza avere $\beta''(h_j)<0$.
    
    Abbiamo appena mostrato che $Q$ dev'essere di tipo positivo. Sia ora $$\alpha'=\sum_{i\in Q}(k_i-m_i)\eps_i.$$Essendo $Q$ una componente connessa del grafo $S(A)\setminus P$, avremo $\alpha'(h_i)=(\alpha-\beta)(h_i)\ \ \forall\ i\in Q$. Ma sappiamo che $\alpha(h_i)\le 0,\ \ \beta'(h_i)\ge 0\ \ \forall\ i\in Q$ (qui è entrata in gioco l'ipotesi che $\alpha$ sta in $M$), da cui segue $\alpha'(h_i)\le 0\ \ \forall\ i\in Q$, il che contraddice la positività di $Q$.
    
    Abbiamo così dimostrato che gli elementi di $M$ sono radici positive. Tuttavia per definizione di camera fondamentale $\alpha\in M\Rightarrow 2\alpha\in M$, quindi per la proposizione \eqref{6ytt} $M$ è composto unicamente da radici immaginarie.
\end{proof}
\begin{observation}
    La proposizione \eqref{9345}, unita a \eqref{9344}, ci permette di definire in modo alternativo il sistema di radici immaginarie positive di $G(A)$:$$\Delta_+^{im}=W(A)\cdot M$$(per matrici di tipo positivo gli insiemi di entrambi i lati sono vuoti).
\end{observation}
















%Sia $A$ una matrice di Cartan di dimensione $n$, $\Gamma$ il reticolo associato, e $L=\Rb\otimes_{\Zb}\Gamma$. L'isomorfismo $x\mapsto h_x$ tra $L$ e $G_0(A)$ definito nella scorsa sezione ci permette di indurre una forma simmetrica su $L$ a partire da quella definita su $G(A)$, ponendo $(x,y):=(h_x,h_y)$.

%Per esprimere esplicitamente il prodotto scalare su $L$ calcoliamo, per una qualsiasi coppia di indici $a,b\le n$, il valore di $(\eps_a,\eps_b)$. Nello svolgere il calcolo supporremo $A$ non degenere, cosa che ci è concessa per come abbiamo definito l'assegnazione $x\mapsto h_x$ nel caso degenere.

%\begin{proposition}
%Per ogni coppia di indici $a,b\le n$ vale che $$(\eps_a,\eps_b)=\frac{A_{ab}}{(e_a,f_a)}=\frac{(h_a,h_b)}{(e_a,f_a)(e_b,f_b)}.$$
%\end{proposition}
%\begin{proof}
%Sia $B$ la matrice inversa di $A$. Ricordiamo che per ogni indice $i\le n$ si ha $h_{\eps_i}=\sum_jc_jh_j$, dove $$c_j=\sum_l\frac{B_{lj}A_{li}}{(e_l,f_l)}.$$
%Ma allora per definizione di $h_{\eps_a}$ otteniamo che $$(\eps_a,\eps_b)=(h_{\eps_a},h_{\eps_b})=\left(h_{\eps_a}\ ,\ \sum_{l,m}\frac{ h_lB_{ml}A_{mb}}{(e_m,f_m)}\right)=\sum_{l,m}\frac{A_{la}B_{ml}A_{mb}}{(e_m,f_m)}=\frac{A_{ab}}{(e_a,f_a)}.$$
%\end{proof}
%\begin{observation}
%Se $A$ è una matrice simmetrica allora $(\eps_a,\eps_b)=A_{ab}$.
%\end{observation}
%\begin{observation}
%Sebbene l'assegnazione $x\mapsto h_x$ nel caso di $A$ degenere dipendesse fortemente dalla scelta dell'embedding $G(A)\rightarrow G(A')$, per quanto appena visto la definizione del prodotto scalare su $L$ è invece qualcosa di intrinseco in $G(A)$, cosa non scontata a priori. 
%\end{observation}

%\bigskip