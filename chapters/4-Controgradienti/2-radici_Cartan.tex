Data una matrice di Cartan $A$ di dimensione $n$, siano $\eps_i,\ i\le n$ delle applicazioni lineari con dominio $G_0(A)$ (i.e. la parte di grado $0$ di $G(A)$) definite da $$\eps_i(h_j)=A_{ji}\ \ \forall\ i,j\le n$$(N.B. anche se due di esse sono la stessa applicazione, le considereremo come oggetti distinti).

Sia $\Gamma$ il reticolo con base formale $(\eps_i)_{i\le n}$, in cui ciascun elemento $\alpha=\sum \alpha_i\eps_i$ definisce un'applicazione lineare da $G_0(A)$ in $\Rb$. Analogamente, anche gli elementi di $L:=\Rb\otimes_{\Zb}\Gamma$ definiscono delle applicazioni lineari su $G_0$. Si indichi con $\Gamma_+$ (risp. $\Gamma_-$) le combinazioni intere degli $\eps_i$ a coefficienti non negativi, non tutti nulli (risp. non positivi, non tutti nulli). Preso $i\le n$ definiamo $G_{\eps_i}:=\Rb e_i$, $G_{-\eps_i}:=\Rb f_i$, $G_0:=G_0(A)$ (il primo $0$ indica lo zero di $\Gamma$, mentre il secondo indica il termine di grado zero di $G$). Ora per $\alpha\in \Gamma_+$ si ponga $$G_\alpha:=\Span([e_{i_1}e_{i_2}\cdots e_{i_k}]:\ \sum_l \eps_{i_l}=\alpha),$$ mentre per $\alpha\in \Gamma_-$ $$G_\alpha:=\Span([f_{i_1}f_{i_2}\cdots f_{i_k}]:\ -\sum_l \eps_{i_l}=\alpha).$$Se invece $\alpha$ ha coefficienti di segno discorde si ponga $G_\alpha:=0$. 

Denotando con $\widetilde{G}(A)$ l'algebra di Lie graduata massimale su $\widehat{G}(A)$, preso $\alpha\in \Gamma$ definiamo $\widetilde{G}_\alpha$ in modo analogo a $G_\alpha$ (stavolta ci troviamo in uno spazio più grande, dove ancora non abbiamo quozientato per l'ideale massimo con intersezione banale con $\widehat{G}(A)$).

Preso $\alpha\in \Gamma$, saremo soliti indicare con $e_\alpha$ un elemento qualsiasi di $G_\alpha$, o di $\widetilde{G}_\alpha$ a seconda del contesto.

\begin{observation}
I sottospazi $G_\alpha$, al variare di $\alpha$ generano tutta $G(A)$.
\end{observation}
\begin{definition}
    Data $\alpha=\sum k_i\eps_i\in\Gamma$, chiamiamo \textit{altezza} di $\alpha$ l'intero positivo $|\alpha|:=\lvert\sum k_i\rvert$.
\end{definition}
\begin{proof}
È una diretta conseguenza della proposizione \eqref{diod}.
\end{proof}
\begin{proposition}\label{primoo}
        Sia $I$ l'ideale massimo in $\widetilde{G}(A)$ che interseca banalmente $\widehat{G}(A)$. Allora valgono i seguenti fatti:
    \begin{itemize}
        \item[a)] Se $\alpha,\beta\in \Gamma$ e $\alpha+\beta\ne0$, allora $(\widetilde{G}_\alpha,\widetilde{G}_\beta)=0$.
        \item[b)] Se $x\in \widetilde{G}_\alpha$ con $\alpha\in \Gamma\setminus\{0\}$ è tale che $(x,\widetilde{G}_{-\alpha})=0$, allora $x\in I$.
    \end{itemize}
\end{proposition}
\begin{proof}
    Per il punto a), l'unico caso non banale è $\alpha\in \Gamma_-$ e $\beta\in \Gamma_+$, con almeno uno dei due che non sta nella parte $1$-locale. Si fissi una coppia $\alpha\in \Gamma_+,\ \beta\in \Gamma_-\setminus\{-\alpha\}$, con somma delle altezze di $\alpha$ e $\beta$ minima, per cui $(\widetilde{G}_\alpha,\widetilde{G}_\beta)\ne 0$ (supponiamo per assurdo che esista). Allora esistono $y=[f_{i_1}f_{i_2}\cdots f_{i_l}]\in \widetilde{G}_\beta$ e $x=[e_{j_1}e_{j_2}\cdots e_{j_k}]\in \widetilde{G}_\alpha$ tali che $(x,y)\ne 0$. Il caso $l=k=1$ lo possiamo escludere, supponiamo quindi $k\ge 2$ (il caso $l\ge 2$ è analogo). Per l'invarianza si ha $$(x,y)=([e_{i_1}\cdots e_{i_{k-1}}],[e_{i_k}y])\stackrel{\eqref{030}\eqref{021}}{\in} (\widetilde{G}_{\alpha+\eps_{i_k}},\widetilde{G}_{\beta-\eps_{i_k}})=0,$$dove l'ultimo passaggio segue dalla minimalità di $\alpha,\beta$.
    
    Il punto b) è una diretta conseguenza di quanto appena dimostrato, del fatto che i $\widetilde{G}_\alpha$ generino $\widetilde{G}(A)$, e del fatto che il radicale di $(\ ,\ )$ in $\widetilde{G}$ sia $I\oplus Z$ (osservazione \eqref{incre}).
    
\end{proof}
\begin{observation}
Quanto appena mostrato ci dice anche che il punto a) resta valido se mettiamo $G(A)$ al posto di $\widetilde{G}$, mentre il punto b) diventa: \virg{se $x\in G_\alpha$ con $\alpha\in \Gamma\setminus\{0\}$ è tale che $(x, G_{-\alpha})=0$ allora $x=0$.}
\end{observation}
Molte proprietà delle algebre di Lie graduate passano alle sottoalgebre omogenee. Come vedremo in seguito, per molte dimostrazioni è più facile lavorare con matrici di Cartan non degeneri. Di conseguenza, considerato che molte caratteristiche delle algebre di Lie graduate passano alle sottoalgebre omogenee, la seguente proposizione ci tornerà spesso comoda.
\begin{proposition}\label{embys}
    Per ogni algebra di Lie controgradiente $G(A)$, associata a una matrice $A$ di dimensione $n$, esiste un'algebra di Lie controgradiente $G(A')$ con matrice $A'$ non degenere di dimensione $n'$, in cui è naturalmente embeddata, e dove $A$ è minore principale di $A'$.
\end{proposition}
\begin{proof}
    Si costruisca $A'$ nel modo seguente:
    \begin{equation*}
    \begin{pmatrix}
    A & -cI \\
    -2I & -2I 
    \end{pmatrix}
    \end{equation*}
    dove $c$ è un intero che non sia autovalore di $A$. Si verifica immediatamente che le proprietà M1), M2), M3) continuano ad essere verificate dalla nuova matrice $A'$, l'unica cosa non scontata è che la sottoalgebra di Lie di $G(A')$ generata da $\{f'_1,\dots,f'_n,h'_1,\dots,h'_n,e'_1,\dots,e'_n\}$ sia effettivamente isomorfa a $G(A)$. Prendiamo $\widetilde{G}(A)$ e $\widetilde{G}(A')$ algebre di Lie graduate massimali su $\widehat{G}(A)$ e $\widehat{G}(A')$ rispettivamente, con $G(A)=\widetilde{G}/I$ e $G(A')=\widetilde{G}(A')/I'$. Per la proposizione \eqref{embei} l'embedding $\widehat{G}(A)\rightarrow \widehat{G}(A')$ induce un embedding $i:\ \widetilde{G}\rightarrow\widetilde{G}'$ (identificheremo $\widetilde{G}$ con l'immagine). Ora, $\widetilde{G}(A)\cap I'\subset I$ perché $I$ è massimo in $G(A)$. Viceversa, preso $x\in I$, con $x\in G_\alpha(A)$ e $\alpha\ne 0$, ho che $(x,\widetilde{G}_\alpha(A))=0$. Ma $\widetilde{G}_\alpha(A)=\widetilde{G}_\alpha(A')$, quindi per la proposizione \eqref{primoo} $x\in I'$.
\end{proof}
\begin{lemma}\label{033}
Siano $\alpha\in \Gamma$ e $h\in G_0(A)$. Allora $[he_\alpha]=\alpha(h)e_\alpha$.
\end{lemma}
\begin{proof}
Per linearità ci basta verificarlo per elementi del sistema di generatori di $G_\alpha$ dato da $\{[e_{i_1}e_{i_2}\cdots e_{i_k}]\ \text{con}\ \sum_j\eps_{i_j}=\alpha\}$. Ma per questi elementi il lemma è un'immediata conseguenza dell'osservazione \eqref{032}.
\end{proof}
\begin{proposition}\label{gradz}
Gli spazi $G_\alpha$ costituiscono una gradazione in $\Zb^n$ di $G(A)$, cioè:
\begin{itemize}
    \item[a)] $[G_{\alpha}G_{\beta}]\subset G_{\alpha+\beta}\ \ \forall\ \alpha,\beta\in \Gamma$;
    \item[b)] $G(A)=\bigoplus_{\alpha\in \Gamma} G_{\alpha}$.
\end{itemize}
\end{proposition}
\begin{proof}
Per il punto a) l'unico caso interessante è $\alpha,\beta\in \Gamma_+\cup \Gamma_-\cup\{0\}$. Se stanno entrambi in $\Gamma_+$ o in $\Gamma_-$ è una diretta conseguenza di \eqref{030}. Se $\alpha=0$, il caso $\beta=0$ è banale, quindi diciamo $\beta\in \Gamma_+$ (il caso $\beta\in \Gamma_-$ è identico): adesso basta osservare che per ogni $h\in G_0$ tutti gli elementi di $G_\beta$ sono autovettori di $\Ad h$, grazie al lemma \eqref{033}.
Supponiamo infine che $\alpha\in \Gamma_+$ e $\beta\in \Gamma_-$. Grazie al lemma \eqref{030} ci basta mostrare il seguente fatto:
\begin{lemma}
Siano $S_1,S_2$ due multi-insiemi non vuoti di indici minori o uguali di $n$. Denotiamo con $e_{S_1}$ (risp. $f_{S_2}$) il multi-insieme non vuoto contenente $e_i$ tante volte quante $i$ compare in $S_1$ (risp. $f_i$ tante volte quante $i$ compare in $S_2$). Siano inoltre $\alpha=\sum_{i\in S_1} \eps_i$ e $\beta=\sum_{i\in S_2}\eps_i$. Allora $$[x_1x_2\cdots x_{k}]\in G_{\alpha+\beta},$$ogni volta che il multi-insieme contenente gli $x_i$ (contati con molteplicità) è uguale a $e_{S_1}\cup f_{S_2}$.
\end{lemma}
\begin{proof}
Supponiamo l'assurdo. Sia $S_1,S_2$ una coppia di multi-insiemi - di cui almeno uno non vuoto - con somma delle cardinalità minima, che non soddisfa la proprietà del testo per certi $[x_1x_2\cdots x_k]\ne 0$. Diciamo $x_1\in e_{S_1}$ (l'altro caso è identico). Se $S_2$ è vuoto abbiamo già concluso, altrimenti sia $m\le k$ il minimo indice $i$ tale che $x_i\in f_{S_2}$, diciamo $x_m=f_l$. Allora per il lemma \eqref{023} possiamo dire che $$[x_1x_2\cdots x_m]\in\Span([x_1x_2\cdots x_{j-1}[x_jx_m]x_{j+1}\cdots x_{m-1}];\ j=1,\dots,m-1),$$ che è nullo a meno che alcuni $x_j$ (con $j\le m-1$) siano proprio $e_l$. In tal caso $$\Span([x_1x_2\cdots x_{j-1}[x_jx_m]x_{j+1}\cdots x_{m-1}];\ j=1,\dots,m-1)=$$$$= \Span([x_1x_2\cdots x_{j-1}h_lx_{j+1}\cdots x_{m-1}];\ x_j=e_l)\stackrel{\eqref{021}}{\subset}$$$$\subset\Span([x_1x_2\cdots x_{j-1}x_{j+1}\cdots x_{m-1}];\ x_j=e_l),$$da cui $$[x_1x_2\cdots x_k]\in\Span( [x_1x_2\cdots x_{j-1}x_{j+1}\cdots x_{m-1}x_{m+1}\cdots x_k];\ j<m,\ x_j=e_l).$$Ma per la minimalità della scelta di $S_1$ e $S_2$ possiamo affermare che i generatori dello span di destra soddisfano il lemma, e questo porta a un assurdo.
\end{proof}
Veniamo al punto b). Sappiamo già che i sottospazi $G_{\alpha}$ generano $G(A)$. Per dire che sono in somma diretta, sfruttando la gradazione di $G(A)$ ci riduciamo fin da subito a dimostrare che i $G_\alpha$ al variare di $\alpha\in \Gamma_+$ sono in somma diretta. Grazie al lemma \eqref{embys} possiamo supporre che $A$ sia non degenere, cosicché elementi distinti di $\Gamma$ definiscano applicazioni lineari distinte. 
Ora, se per assurdo i $G_\alpha$ (con $\alpha\in \Gamma_+)$ non fossero in somma diretta, sia $x=e_\alpha+e_\beta+\dots =0$ la più corta relazione di dipendenza non banale tra questi spazi, e $h\in G_0(A)$ tale che $\alpha(h)\ne\beta(h)$. Allora per il lemma \eqref{033} $[hx]-\alpha(h)x=0$ è una relazione di dipendenza non banale più corta, assurdo.
\end{proof}
\begin{definition}
Dato un $\alpha\in \Gamma\setminus\{0\}$ per cui $G_\alpha$ è non banale (quindi per forza $\alpha\in \Gamma_+\cup \Gamma_-)$ diremo che $\alpha$ è una \textit{radice positiva} (risp. \textit{negativa}) se $\alpha\in \Gamma_+$ (risp. $\alpha\in \Gamma_-$), e chiameremo $G_\alpha$ lo \textit{spazio radice} associato ad $\alpha$. Denotiamo con $\Delta_+$ (risp. $\Delta_-$) l'insieme delle radici positive (risp. negative), e chiamiamo $\Delta:=\Delta_+\cup\Delta_-$ il \textit{sistema di radici} di $G(A)$. Siano infine $\Pi:=\{\eps_i;\ i\ \text{tale che}\ A_{ii}=2\}$ le \textit{radici semplici} di $G(A)$.
\end{definition}
%\begin{proposition}
%    Valgono i seguenti fatti:
%    \begin{itemize}
%        \item[a)] Se $\alpha,\beta\in \Gamma,\ \alpha+\beta\ne0$ allora $(G_\alpha,G_\beta)=0$.
%        \item[b)] Se $x\in G_\alpha$ con $\alpha\in \Gamma\setminus\{0\}$ è tale che $(x,G_{-\alpha})=0$ allora $x=0$.
%    \end{itemize}
%\end{proposition}
%    Per i primi due punti possiamo supporre che $A$ sia non degenere. Ma allora esiste $h\in G_0$ tale che $(\alpha-\beta)(h)\ne 0$, quindi $([e_\alpha h],e_\beta)-(e_\alpha,[he_\beta])=0\Rightarrow (-\alpha(h)+\beta(h))(e_\alpha,e_\beta)=0\Rightarrow (e_\alpha,e_\beta))= 0$ così il punto a) è dimostrato. 
Supponendo $A$ non degenere, preso $x\in L\ (=\Rb\otimes\Gamma)$, sia $h_x$ l'unico elemento in $G_0(A)$ tale che $(h_x,h)=x(h)\ \ \forall\ h\in G_0$. Basta porre $h_x=\sum_i c_ih_i$ dove i $c_i$ risolvono il sistema lineare $\sum_i c_iA_{ij}=\frac{x(h_j)}{(e_j,f_j)}\ \ \forall\ j\le n$. Si osservi che l'assegnazione $\alpha\mapsto h_\alpha$ è chiaramente una mappa lineare da $L$ in $G_0(A)$, ed è iniettiva in quanto altrimenti esisterebbero elementi distinti $x,y\in L$ che definiscono le stesse applicazioni lineari. Quindi è un isomorfismo.

Se adesso abbiamo tra le mani una matrice di Cartan degenere $A$, operiamo l'embedding \eqref{embys} $G(A)\rightarrow G(A')$. In questo modo possiamo comunque definire la mappa lineare $L\rightarrow G_0(A')$, con $L$ associato ad $A$, per restrizione della mappa $L'\rightarrow G_0(A')$, con $L'$ associato ad $A'$. In realtà faremo vedere a breve che l'immagine della mappa $L\rightarrow G_0(A')$ così definita è proprio $G_0(A)$.

\begin{proposition}
Sia $\alpha\in\Gamma$. Allora $$[e_\alpha e_{-\alpha}]=(e_\alpha,e_{-\alpha})h_\alpha.$$
\end{proposition}
\begin{proof}
Non è restrittivo supporre $A$ non degenere, a meno di embedding. Ma allora ci basta mostrare che, per $\alpha\in \Gamma$, $$([e_\alpha e_{-\alpha}]-(e_\alpha,e_{-\alpha})h_\alpha,h_j)=0\ \ \forall\ j\le n.$$
D'altronde $$([e_\alpha e_{-\alpha}],h_j)-(e_\alpha,e_{-\alpha})(h_\alpha,h_j)=(e_\alpha, [e_{-\alpha}h_j])-(e_\alpha,e_{-\alpha})\alpha(h_j)\stackrel{\eqref{033}}{=}0\ \ \forall\ j.$$
\end{proof}
\begin{observation}
Questa osservazione e il fatto che per $\alpha\in\Delta$ si ha $(G_\alpha,G_{-\alpha})\ne 0$, (perché altrimenti sia $G_\alpha$ che $G_{-\alpha}$ starebbero nel radicale), ci permette di dire che $h_\alpha$ è contenuto nell'algebra di Lie generata da $G_\alpha$ e $G_{-\alpha}$. Nello specifico risolve la questione che avevamo lasciato in sospeso: se $A$ è una matrice degenere e $A'$ è una sua estensione non degenere, la mappa 
\begin{align}
L&\rightarrow G_0(A')\\ x&\mapsto h_x
\end{align}
ha immagine in $G_0(A)$, cioè è un isomorfismo con $G_0(A)$ per una questione di dimensioni.
\end{observation}
Andiamo ora a definire il gruppo di Weyl.
\begin{definition}
Preso $i\le n$ tale che $A_{ii}=2$, sia $s_i:L\rightarrow L$ la trasformazione lineare operante nel seguente modo:
$$s_i(x)=x-\alpha(h_i)\eps_i\ \ \forall\ \alpha\in L.$$$s_i$ è detta \textit{riflessione semplice} di centro $i$. Si verifica immediatamente che $s_i^2=\Id$. Chiamiamo \textit{gruppo di Weyl} il gruppo di automorfismi $W(A)$ generato dalle riflessioni semplici.
\end{definition}
\begin{observation}
Il prodotto scalare su $G_0(A)$ è invariante per azione di $W(A)$, infatti presi due qualsiasi indici $a,b\le n$ e scelto $i$ con $A_{ii}=2$ si ha che $$(s_i(h_a),s_i(h_b))-(h_a,h_b)=(h_a-\eps_i(h_a)h_i\ ,\ h_b-\eps_i(h_b)h_i)-(h_a,h_b)=$$$$=A_{ai}A_{bi}(h_i,h_i)-A_{bi}(h_a,h_i)-A_{ai}(h_i,h_b)\stackrel{\eqref{023}}{=}$$$$=2A_{ai}A_{bi}(e_i,f_i)-A_{bi}A_{ai}(e_i,f_i)-A_{ai}A_{bi}(e_i,f_i)=0.$$
\end{observation}
Se $A$ è non degenere e identifichiamo $G_0$ con il duale di $L$ (dove l'accoppiamento è dato da $\langle x,h\rangle = x(h)$) possiamo considerare anche la rappresentazione duale di $W(A)$ su $L^*=G_0$, data da 
$$\langle x, s_i(h)\rangle=\langle x, h\circ s_i^{-1}\rangle=\langle x,h\rangle-\langle\eps_i,h\rangle\langle x,h_i\rangle\ \ \forall\ h\in G_0,$$o in altre parole \begin{equation}\label{034}
s_i(h)=h-\eps_i(h)h_i
\end{equation}
Quanto appena detto e il lemma \eqref{embys} ci permettono di affermare che, anche quando $A$ è degenere, \eqref{034} ci fornisce un'azione di $W(A)$ su $G_0$.
\begin{observation}\label{038}
Presi $w\in W(A),x\in L,\ h\in G_0$, si ha che $$(wx)(wh)=x(h)$$
\end{observation}
\begin{proof}
Segue direttamente dalla definizione dell'azione di $W$ su $G_0$, come rappresentazione duale.
\end{proof}
\begin{observation}
Preso $x\in L$ e $s_i$ riflessione semplice, si ha che $$h_{s_ix}=s_i(h_x).$$
\end{observation}
\begin{proof}
Basta ragionare nel caso $A$ non degenere. In questo caso è sufficiente mostrare che $(s_i(h_x),h)=(s_ix)(h)\ \ \forall\ h\in G_0,\ x\in L$. Per \eqref{038} questo equivale a $(h_x,s_ih)=(s_ix)(h)$, il che è vero in quanto $(h_x,s_ih)=x(s_ih)=(s_ix)(h)$. 
\end{proof}
\begin{proposition}
In $G(A)$ si ha che, scelti due indici $i\ne j$ con $A_{ii}=2$,
\begin{equation}
    (\Ad e_i)^{-A_{ij}+1}e_j=0,\ \ \ \ \ \ (\Ad f_i)^{-A_{ij}+1}f_j=0.
\end{equation}
\end{proposition}
\begin{proof}
Per \eqref{036} sappiamo che $G'(A)$ è transitiva. Detto $E_{ij}:=(\Ad e_i)^{-A_{ij}+1}e_j,$ ci basta quindi verificare che $[f_sE_{ij}]=0\ \ \forall\ s$ (per la seconda uguaglianza si ragiona in modo analogo). Per la proposizione \eqref{gradz} gli unici casi interessanti sono $s\in\{i,j\}$. Supponendo $s=i$, l'osservazione \eqref{032} ci permette di dire che $$[f_iE_{ij}]=\sum_{l=0}^{-A_{ij}}(\Ad e_i)^{-A_{ij}-l}[h_i((\Ad e_i)^le_j)]\stackrel{\eqref{033}}{=}\sum_{l=0}^{-A_{ij}}(l\eps_i+\eps_j)(h_i)(\Ad e_i)^{-A_{ij}}e_j=$$$$=-(A_{ij}+1)A_{ij}+2\sum_{l=0}^{-A_{ij}}l=0.$$
Se $s=j$ e $A_{ij}< 0$, sempre per \eqref{032}  si ha che $$[f_jE_{ij}]=(\Ad e_i)^{-A_{ij}-1}[e_i[e_ih_j]]=0.$$ Se invece $A_{ij}=0$, allora $A_{ji}=0$ quindi $[f_jE_{ij}]=[e_ih_j]=0$.
\end{proof}
\begin{observation}
Quanto appena dimostrato ci dice che per $k$ abbastanza grande $(\Ad \pm e_i)^k$ agisce banalmente su $G_1$. D'altronde si osserva subito che se $k\ge 3$, allora $(\Ad \pm e_i)^k$ agisce banalmente anche su $G_0$ e su $G_{-1}$. Ma esso è l'iterata di una derivazione di algebre di Lie, quindi agirà banalmente su tutta $G(A)$ (in quanto generata dalla parte locale). Un discorso analogo vale con $\Ad \pm f_i$ al posto di $\Ad\pm e_i$.
\end{observation}
Grazie a questa osservazione, se $A_{ii}=2$ ha senso definire gli esponenziali $$\exp(\Ad\pm e_i):=\sum_{n=0}^\infty\frac{(\Ad \pm e_i)^n}{n!},$$ che saranno uno inverso dell'altro (facendo un prodotto di esponenziali di applicazioni che commutano, il risultato è l'esponenziale della somma degli argomenti). Gli esponenziali $\exp(\Ad\pm f_i)$ si definiscono in modo analogo.

Preso $i$ tale che $A_{ii}=2$ andiamo quindi a considerare la seguente applicazione, che è anche un automorfismo di $G(A)$ come algebra di Lie: $$\sigma_i=\exp(\Ad f_i)\exp(\Ad -e_i)\exp(\Ad f_i)$$
\begin{lemma}\label{040}
Preso $i$ con $A_{ii}=2$ valgono le seguenti relazioni:
    $$\sigma_i(h)=s_i(h)\ \ \forall\ h\in G_0,\ \ \ \ \ \ \ \ \ \ \ \ \sigma_i(G_\alpha)=G_{s_i(\alpha)}\ \ \forall\alpha\in\Delta.$$Da ciò segue nello specifico che $\Delta$ è invariante per azione di $W(A)$.
\end{lemma}
\begin{proof}
La prima relazione si verifica con un semplice calcolo: possiamo limitarci a considerare l'espansione \virg{al prim'ordine} del termine $\exp(\Ad f_i)$ a destra, \virg{al secondo ordine} di $\exp(\Ad -e_i)$ e di $\exp(\Ad f_i)$ a sinistra. Per mostrare adesso la seconda relazione, possiamo come al solito ridurci al caso $A$ non degenere. Si prenda l'identità di \eqref{033} e si applichi $\sigma_i$ da entrambe le parti, ottenendo 
\begin{gather}[\sigma_i(h)\sigma_i(e_\alpha)]=\alpha(h)\sigma_i(e_\alpha)\ \ \forall\ h\in G_0\Rightarrow\\ \Rightarrow [h\sigma_i(e_\alpha)]=\alpha(s_i h)\sigma_i(e_\alpha)\stackrel{\eqref{038}}{=}(s_i\alpha)(h)\sigma_i(e_\alpha)\ \ \forall\ h\in G_0.\label{039}\end{gather}
Se adesso scriviamo $\sigma_i(\alpha)=\sum_{\beta\in \Gamma} e_{\beta}$ (dove gli $e_\beta$ sono tutti nulli a meno di un numero finito), si ha $$\sum_{\beta\in \Gamma}(s_i\alpha)(h)e_\beta=[h\sigma_i(e_\alpha)]\stackrel{\eqref{033}}{=}\sum_{\beta\in \Gamma}\beta(h)e_\beta\ \ \forall\ h\in G_0.$$Questo significa che per ogni $\beta$ con $e_\beta\ne 0$ si deve avere $(s_i\alpha)(h)=\beta(h)\ \ \forall\ h\in G_0$, cioè necessariamente $\beta=s_i\alpha$ poiché $A$ è non degenere.

L'invarianza di $\Delta$ tramite $W(A)$ segue immediatamente da $\sigma_i(G_\alpha)=G_{s_i(\alpha)}\ \ \forall\ i:\ A_{ii}\ne 0$.
\end{proof}
\begin{observation}\label{037}
$\Delta$ è anche invariante per la mappa $\alpha\mapsto -\alpha$. Infatti l'automorfismo di $G(A)$ che si ottiene estendendo quello della parte $1$-locale dato da $e_i\mapsto f_i,\ f_i\mapsto e_i,\ h_i\mapsto -h_i\ \ \forall\ i\le n$ (un tale automorfismo esiste per la proposizione \eqref{roar}) manda $G_\alpha$ in $G_{-\alpha}$ per ogni $\alpha\in\Delta$.
\end{observation}