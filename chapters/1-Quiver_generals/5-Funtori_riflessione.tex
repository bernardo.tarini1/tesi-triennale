Concludiamo il capitolo parlando di funtori di riflessione, uno strumento estremamente utile per creare un ponte tra rappresentazioni con stesso grafo $S$ e dimensioni coniugate tramite gruppo di Weyl.

\bigskip
Sia $(S,O)$ un quiver. Diciamo che un suo vertice $i\in I$ è un \textit{pozzo} (risp. \textit{sorgente}) se ha solo archi entranti (risp. uscenti). Se ci troviamo in uno di questi due casi diciamo che il vertice $i$ è \textit{ammissibile} e denotiamo con $r_i(O)$ l'orientazione ottenuta capovolgendo gli archi che coinvolgono $i$. Si noti che un vertice ammissibile è per definizione senza loop.

\begin{definition}
    Sia $i$ un vertice sorgente di $(S,O)$. Definiamo il \textit{funtore di riflessione} $r_i^-$ da $\Repr(S,O,\Fb)$ in $\Repr(S,r_i(O),\Fb)$ nel modo seguente:
    \begin{itemize}
        \item Data la rappresentazione $(V,\phi)\in\Repr(S,O,\Fb)$ poniamo $$r_i^-(V)_j:=V_j\ \ \forall\ j\ne i,\ \ \ \ \ \ \ \ \ \ r_i^-(V)_i:=\Coker\ (V_i\xrightarrow{\bigoplus \phi_h}\bigoplus_{\substack{h:\ i\rightarrow l}}V_l)$$
        Se $h:\ i\rightarrow j$ in $(S,O)$, nel nuovo quiver avremo $h:\ j\rightarrow i$ e porremo come $r_i^-(\phi)_h$ la composizione $$V_j\rightarrow\bigoplus_{s:\ i\rightarrow l} V_l\rightarrow\Coker (V_i\rightarrow \bigoplus_{\substack{s:\ i\rightarrow l}}V_l),$$dove la prima è un'inclusione nell'$h$-esimo fattore e la seconda è una proiezione. Se invece $h$ non coinvolge $i$ poniamo semplicemente $r_i^-(\phi)_h=\phi_h$.

        \item Sia ora $f$ un morfismo di rappresentazioni di $(S,O)$ da $V$ in $U$. Poniamo $$r_i^-(f)_j=f_j\ \ \forall\ j\ne i,$$mentre $r_i^-(f)_i$ sarà la mappa indotta dal passaggio al quoziente illustrato nel diagramma commutativo a righe esatte qui sotto:
        % https://q.uiver.app/?q=WzAsMTAsWzEsMCwiVl9pIl0sWzIsMCwiXFxiaWdvcGx1c197aDpcXCBpXFxyaWdodGFycm93IGp9Vl9qIl0sWzMsMCwicl9pXisoVilfaSJdLFsxLDEsIlVfaSJdLFsyLDEsIlxcYmlnb3BsdXNfe2g6XFwgaVxccmlnaHRhcnJvdyBqfVVfaiJdLFszLDEsInJfaV4rKFUpX2kiXSxbMCwwLCIwIl0sWzAsMSwiMCJdLFs0LDEsIjAiXSxbNCwwLCIwIl0sWzAsMV0sWzEsMl0sWzAsMywiZl9pIl0sWzEsNCwiXFxiaWdvcGx1cyBmX2oiXSxbMiw1LCJyX2leKyhmKV9pIiwwLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzMsNF0sWzQsNV0sWzYsMF0sWzcsM10sWzUsOF0sWzIsOV1d
\[\begin{tikzcd}
	{V_i} & {\bigoplus_{h:\ i\rightarrow j}V_j} & {r_i^-(V)_i} & 0 \\
	{U_i} & {\bigoplus_{h:\ i\rightarrow j}U_j} & {r_i^-(U)_i} & 0
	\arrow[from=1-1, to=1-2]
	\arrow[from=1-2, to=1-3]
	\arrow["{f_i}", from=1-1, to=2-1]
	\arrow["{\bigoplus f_j}", from=1-2, to=2-2]
	\arrow["{r_i^-(f)_i}", dashed, from=1-3, to=2-3]
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-2, to=2-3]
	\arrow[from=2-3, to=2-4]
	\arrow[from=1-3, to=1-4]
\end{tikzcd}\]
    Segue direttamente dalla commutatività del diagramma che $r_i(f)$ è un morfismo di rappresentazioni in $(S,r_i(O))$. Allo stesso modo seguono banalmente anche le proprietà funtoriali di $r_i^-$.
    \end{itemize}
\end{definition}
\begin{definition}
    Sia adesso $i$ un vertice pozzo del quiver $(S,O)$. Definiamo il \textit{funtore di riflessione} $r_i^+$ da $\Repr(S,O)$ in $\Repr(S,r_i(O))$ nel modo seguente:
    \begin{itemize}
        \item Data la rappresentazione $(V,\phi)\in\Repr(S,O,\Fb)$ poniamo $$r_i^+(V)_j:=V_j\ \ \forall\ j\ne i,\ \ \ \ \ \ \ \ \ \ r_i^+(V)_i:=\Ker\ ( \bigoplus_{h:\ l\rightarrow i}V_l\xrightarrow{\bigoplus \phi_h} V_i)$$Se $h:\ j\rightarrow i$ in $(S,O)$, nel nuovo quiver avremo $h:\ i\rightarrow j$ e porremo come $r_i^+(\phi)_h$ la composizione $$\Ker\ (\bigoplus_{\substack{s:\ i\rightarrow l}}V_l\rightarrow V_i)\rightarrow\bigoplus_{s:\ i\rightarrow l} V_l\rightarrow V_j,$$dove la prima è un'inclusione e la seconda è una proiezione sul $h$-esimo fattore. Se invece $h$ non coinvolge $i$ poniamo semplicemente $r_i^+(\phi)_h=\phi_h$.

        \item Sia ora $f$ un morfismo di rappresentazioni di $(S,O)$ da $V$ in $U$. Poniamo $$r_i^+(f)_j:=f_j\ \ \forall\ j\ne i,$$mentre $r_i^+(f)_i$ sarà la mappa che estende il seguente diagramma commutativo a righe esatte:
        % https://q.uiver.app/?q=WzAsMTAsWzEsMCwicl9pXisoVilfaSJdLFsyLDAsIlxcYmlnb3BsdXNfe2g6XFwgbFxccmlnaHRhcnJvdyBpfVZfbCJdLFszLDAsIlZfaSJdLFsxLDEsInJfaV4rKFUpX2kiXSxbMiwxLCJcXGJpZ29wbHVzX3toOlxcIGxcXHJpZ2h0YXJyb3cgaX1VX2wiXSxbMywxLCJVX2kiXSxbMCwwLCIwIl0sWzAsMSwiMCJdLFs0LDEsIjAiXSxbNCwwLCIwIl0sWzAsMV0sWzEsMl0sWzAsMywicl9pXisoZilfaSIsMix7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFsxLDQsIlxcYmlnb3BsdXMgZl9sIiwyXSxbMiw1LCJmX2kiLDJdLFs2LDBdLFs3LDNdLFszLDRdLFs0LDVdLFs1LDhdLFsyLDldXQ==
\[\begin{tikzcd}
	0 & {r_i^+(V)_i} & {\bigoplus_{h:\ l\rightarrow i}V_l} & {V_i} \\
	0 & {r_i^+(U)_i} & {\bigoplus_{h:\ l\rightarrow i}U_l} & {U_i} 
	\arrow[from=1-2, to=1-3]
	\arrow[from=1-3, to=1-4]
	\arrow["{r_i^+(f)_i}"', dashed, from=1-2, to=2-2]
	\arrow["{\bigoplus f_l}"', from=1-3, to=2-3]
	\arrow["{f_i}"', from=1-4, to=2-4]
	\arrow[from=1-1, to=1-2]
	\arrow[from=2-1, to=2-2]
	\arrow[from=2-2, to=2-3]
	\arrow[from=2-3, to=2-4]
\end{tikzcd}\]

        \end{itemize}
    Anche in questo caso segue dalla commutatività del diagramma che $r_i^+(f)$ è un morfismo di rappresentazioni di $(S,r_i(O))$, e che le proprietà funtoriali sono soddisfatte.
\end{definition}
Date $U,V\in\Repr(S,O.\Fb)$, è immediato verificare che $r_i^{\pm}(U\oplus V)\simeq r_i^{\pm}(U)\oplus r_i^{\pm}(V)$. È altrettanto facile notare che $r_i^{\pm}(V)=0\Leftrightarrow V_j=0\ \ \forall\ j\ne i$. Grazie a questi due semplici fatti e al prossimo lemma, arriveremo a dimostrare che i funtori di riflessione $r_i^{\pm}$ mandano indecomponibili diversi da $S_i$ in indecomponibili.
\begin{lemma}\label{caccolone}
    Se $i$ è sorgente di $(S,O)$, e una rappresentazione $V$ è tale che sia iniettiva la mappa $V_i\xrightarrow{\bigoplus\phi_h}\bigoplus_{h:\ i\rightarrow l}V_l$, allora si ha un isomorfismo naturale $$r_i^+r_i^-(V)\simeq V.$$Se $i$ è pozzo e $V$ è tale che $\bigoplus_{h:\ l\rightarrow i}V_l\xrightarrow{\bigoplus\phi_h}V_i$ sia surgettiva, allora si ha un isomorfismo naturale $$r_i^-r_i^+(V)\simeq V.$$
\end{lemma}
\begin{proof}
    \ 
    
    \begin{enumerate}[label=\textbf{Caso} \arabic*:]
    \item $i$ sorgente.
        
    Costruiamo l'isomorfismo naturale di rappresentazioni $f$ ponendo innanzitutto $f_j=\Id\ \ \forall\ j\ne i$. Notiamo adesso che per definizione $$r_i^+r_i^-(V)_i=\Ker\ \ \bigoplus_{h:\ i\rightarrow l}V_l\ \ \xrightarrow{\bigoplus r_i^-(\phi)_h}\ \ r_i^-(V)_i$$(scrivere $h:\ i\rightarrow l$ o $h:\ l\rightarrow i$ nei pedici delle somme dirette fa poca differenza, quindi usiamo sempre l'orientazione $O$).
    
    Poiché abbiamo la successione esatta $$0\longrightarrow V_i\xrightarrow{\bigoplus\phi_h}\bigoplus_{h:\ i\rightarrow l} V_l\longrightarrow\Coker(V_i\longrightarrow\bigoplus_{h:\ i\rightarrow l}V_l)=r_i^-(V)_i$$possiamo scegliere come isomorfismo tra $V_i$ e $r_i^+r_i^-(V)_i$ $$f_i=\bigoplus_{h:\ i\rightarrow l}\phi_h$$Per verificare che la $f$ così definita sia in effetti un morfismo ci basta osservare che fissato $h:\ i\rightarrow j$ si ha $\phi_h=p\circ f_i$, dove $p:\ \bigoplus_{s:\ i\rightarrow l}V_l\rightarrow V_j$ è la proiezione sull'$h$-esimo fattore. $r_i^+r_i^-(\phi)_h$ è invece il risultato dell'inclusione di $\Ker \bigoplus_{h:\ i\rightarrow l}V_l\longrightarrow r_i^-(V)_i$ in $\bigoplus_{h:i\rightarrow l}V_l$ composta con la proiezione $p$, quindi in effetti $\Id\circ\ \phi_h=r_i^+r_i^-(\phi)_h\circ f_i$.
    \item $i$ pozzo.

    Stavolta è più comodo definire $f$ da $r_i^-r_i^+(V)$ in $V$. Anche qui è naturale porre $f_j=\Id\ \ \forall\ j\ne i$. Ora, per definizione $$r_i^-r_i^+(V)_i=\Coker\ r_i^+(V)_i\ \ \xrightarrow{\bigoplus r_i^+(\phi)_h}\ \ \bigoplus_{h:\ l\rightarrow i}V_l$$
    Dal diagramma con riga esatta 
    % https://q.uiver.app/?q=WzAsNSxbMCwwLCJyX2leKyhWKV9pPVxcS2VyXFwgKFxcYmlnb3BsdXNfe2g6XFwgbFxccmlnaHRhcnJvdyBpfVZfbFxccmlnaHRhcnJvdyBWX2kpIl0sWzEsMCwiXFxiaWdvcGx1c197aDpcXCBsXFxyaWdodGFycm93IGl9Vl9sIl0sWzIsMCwiVl9pIl0sWzMsMCwiXFxidWxsZXQiXSxbMSwxLCJyX2leLXJfaV4rKFZfaSkiXSxbMCwxXSxbMSwyLCJcXGJpZ29wbHVzIFxccGhpX2giXSxbMiwzLCIwIl0sWzEsNCwicSIsMl0sWzQsMiwiZl9pIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV1d
\[\begin{tikzcd}
	{r_i^+(V)_i=\Ker\ (\bigoplus_{h:\ l\rightarrow i}V_l\rightarrow V_i)} & {\bigoplus_{h:\ l\rightarrow i}V_l} & {V_i} & 0 \\
	& {r_i^-r_i^+(V_i)}
	\arrow[from=1-1, to=1-2]
	\arrow["{\bigoplus \phi_h}", from=1-2, to=1-3]
	\arrow[from=1-3, to=1-4]
	\arrow["p"', from=1-2, to=2-2]
	\arrow["{f_i}"', dashed, from=2-2, to=1-3]
\end{tikzcd}\]
    si ricava che possiamo scegliere come $f_i$ l'isomorfismo indotto da $\bigoplus_{h:l\rightarrow i}\phi_h$ passato al quoziente $p$. Per verificare infine che la $f$ così definita sia effettivamente un morfismo sia $h:\ j\rightarrow i$. Notiamo che $r_i^-r_i^+(\phi)_h$ è per definizione l'inclusione $q$ di $V_j$ nell'$h$-esimo fattore di $\bigoplus_{s:l\rightarrow i}V_l$ seguita dalla proiezione $p$. Se poi componiamo con $f_i$ otteniamo proprio $   (\bigoplus_{s:l\rightarrow i}\phi_s)\circ q$, quindi vale $f_i\circ r_i^-r_i^+(\phi)_h=\phi_h\circ\Id$.
    \end{enumerate}
\end{proof}
Notiamo subito che data una qualsiasi rappresentazione $V$, allora $r_i^{\pm}(V)$ soddisfa le ipotesi del lemma \eqref{caccolone}, e di conseguenza non si può decomporre in due rappresentazioni una delle quali sia completamente concentrata nel vertice $i$.
\begin{observation}
    Se $V$ è una rappresentazione che soddisfa le ipotesi del lemma \eqref{caccolone}, $\dim r_i^\pm V=s_i(\dim V)$.
\end{observation}
\begin{proof}
    È chiaramente sufficiente concentrarci sulla dimensione dello spazio vettoriale di $r_i^\pm (V)_i$. Sia nel caso $i$ sorgente con $V_i\xrightarrow{\bigoplus\phi_h}\bigoplus_{h:i\rightarrow l}V_l$ iniettiva, che nel caso $i$ pozzo con $\bigoplus_{h:l\rightarrow i}V_l\xrightarrow{\bigoplus\phi_h}V_i$ suriettiva, segue dalla definizione di $(r_i^\pm V)_i$ che $$\dim (r_i^\pm V)_i=-\dim V_i+\bigoplus_{h:i\rightarrow l}\dim V_l=(\dim V)_i-2(\dim V,\eps_i)\eps_i.$$
\end{proof}
\begin{theorem}
    Sia $i$ vertice ammissibile per $(S,O)$, e $V\ne S_i$ una rappresentazione indecomponibile. Allora $r_i^{\pm}V$ è indecomponibile e $\dim(r_i^{\pm}V)=s_i(\dim V)$.
\end{theorem}
\begin{proof}
    Notiamo che $V$ deve soddisfare le ipotesi del lemma \eqref{caccolone}. Se infatti $i$ è sorgente e $V_i\rightarrow\bigoplus_{h:i\rightarrow l} V_l$ non è iniettiva allora detto $K$ il suo kernel, e $H$ tale che $K\oplus H\simeq V_i$ avremmo che $V$ si decompone in $U\oplus W$ con $$U_j=V_j\ \ \forall\ j\ne i,\ \ \ U_i=H;\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ W_j=0\ \ \forall\ j\ne i,\ \ \ W_i=K.$$Analogamente se $i$ è pozzo e $\bigoplus_{h:l\rightarrow i}V_l\rightarrow V_i$ non è suriettiva allora posta $H$ la sua immagine e $K$ tale che $K\oplus H=V_i$ avremmo che $V$ si decompone in $U\oplus W$ definiti come sopra.

    In luce di questo, la seconda parte del teorema è una diretta conseguenza dell'osservazione precedente. Supponiamo adesso per assurdo che $r_i^{\pm}(V)$ si decomponga in $A\oplus B$. Sicuramente $A$ e $B$ non sono concentrate nel vertice $i$, di conseguenza $V\simeq r_i^{\mp}r_i^{\pm}(V)\simeq r_i^{\mp}(A)\oplus r_i^{\mp}(B)$ sarebbe una decomposizione non banale di $V$, assurdo.
\end{proof}
\begin{observation}\label{lollo}
    Se $i$ è ammissibile per $(S,O)$ e $\alpha\in\Gamma_+\setminus\{\eps_i\}$, l'esistenza di un'indecomponibile di dimensione $\alpha$ ci garantisce che $s_i\alpha\in\Gamma_+$.
\end{observation}