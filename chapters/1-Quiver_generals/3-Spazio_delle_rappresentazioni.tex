Dato un quiver $(S,O)$, si fissi una dimensione $\alpha\in\Gamma_+$. Indichiamo con $\mf^\alpha(S,O,\Fb)$ lo spazio delle rappresentazioni $V$ su $\Fb$, con $V_i=\Fb^{\alpha_i}\ \ \forall\ i\in I$.

\begin{observation}
    Poiché una rappresentazione di $\mf^\alpha(S,O,\Fb)$ è univocamente determinata dalla scelta delle applicazioni associate agli archi, appare naturale fare la seguente identificazione:$$\mf^\alpha(S,O,\Fb)=\bigoplus_{h:\ i\rightarrow j}\Hom(V_i,V_j)\simeq \bigoplus_{h:\ i\rightarrow j}V_i^*\otimes V_j=\bigoplus_{h:\ i\rightarrow j}(\Fb^{\alpha_i})^*\otimes \Fb^{\alpha_j}$$In questo modo evidenziamo la struttura di spazio vettoriale finito di $\mf^\alpha(S,O,\Fb)$, quindi di varietà algebrica definita su $\Fb$.
\end{observation}
Siano adesso $$\GL(\alpha):=\bigoplus_{i\in I}\GL(\alpha_i,\overline{\Fb}),\ \ \ \ \ \ \ \ \ \ G^{\alpha}:=\frac{\GL(\alpha)}{\overline{\Fb}(\Id_i)_{i\in I}}.$$
Allo scopo di studiare le classi di isomorfismo degli elementi di $\mf^\alpha(S,O,\Fb)$, consideriamo l'azione del gruppo algebrico $\GL(\alpha)$ su $\mf^\alpha(S,O,\overline{\Fb})$, definita su $\Fb$, data da 
$$g\cdot (\phi_h)=\left(g_{t(h)}\circ\phi_h\circ g_{s(h)}^{-1}\right)\ \ \forall\ g=(g_i)\in\GL(\alpha),\ (\phi_h)\in\mf^\alpha(S,O,\overline{\Fb}).$$Poiché gli elementi di $\overline{\Fb}(\Id_i)$ agiscono banalmente, si ha un'azione indotta del gruppo algebrico $G^\alpha$.
Segue direttamente dalla definizione di isomorfismo di rappresentazioni che due elementi di $\mf^\alpha(S,O,\Fb)$ sono isomorfi se e solo se stanno nella stessa $G^\alpha(\Fb)$-orbita. 

\medskip
Siamo adesso già in grado di mostrare una prima freccia del teorema di Gabriel.
\begin{theorem}\label{002}
    Sia $\Fb$ un campo algebricamente chiuso. Se un quiver $(S,O)$ ha un numero finito di rappresentazioni indecomponibili su $\Fb$ (a meno di isomorfismo), allora è di tipo Dynkin.
\end{theorem}
\begin{proof}
    Se esistono finite indecomponibili allora, fissata una dimensione $\alpha\in\Gamma_+$, anche le classi di isomorfismo in $\mf^\alpha(S,O,\Fb)$ devono essere finite in numero: ognuna di esse corrisponde a una somma diretta di indecomponibili che ha al più $|\alpha|$ fattori, ma ogni fattore può essere scelto in finiti modi quindi le combinazioni possibili sono limitate.
    Questo significa che il numero di orbite dell'azione di $G^\alpha(\Fb)$ è finito, quindi per l'irriducibilità di $\mf^\alpha(S,O,\Fb)$ esiste un'orbita  densa $G^\alpha\cdot x$. Equivalentemente, il morfismo di varietà algebriche $f:G^\alpha\rightarrow \mf^\alpha(S,O,\Fb)$ con $g\mapsto g\cdot x$ è dominante, quindi $$\dim G^\alpha\ge \dim\mf^\alpha(S,O,\Fb)=\sum_{h:i\rightarrow j}\alpha_i\alpha_j.$$Adesso però $G^\alpha$ ha una dimensione facilmente calcolabile poiché $$\dim G^\alpha=\dim\GL(\alpha)-\dim \Fb(\Id_i)=-1+\sum_{i\in I}\alpha_i^2$$da cui, ricordando la definizione di $(\ ,\ )$, si ha che $(\alpha,\alpha)\ge 1$. Dall'arbitrarietà di $\alpha$ concludiamo che il prodotto scalare $(\ ,\ )$ è definito positivo, quindi che il grafo $S$ è di tipo Dynkin.
\end{proof}
Rimandiamo l'altra implicazione del teorema di Gabriel a un momento successivo; sarebbe futile affrontarla adesso poiché sarà una semplice conseguenza della teoria che svilupperemo per dimostrare il teorema di Kac.

\medskip
L'ultimo obiettivo questa sezione è di cercare di tradurre in qualche modo la nozione di indecomponibilità di una rappresentazione $x\in\mf^\alpha(S,O,\Fb)$ in termini dell'azione di $G^\alpha$ su $\mf^\alpha(S,O,\overline{\Fb})$. 
\begin{definition}\label{0.4.3}
    Sia $G$ un gruppo lineare algebrico che agisce su una varietà algebrica affine $X$, dove $G$, $X$ e l'azione sono definiti su $\Fb$. Diremo che un punto $x\in X(\Fb)$ è \textit{$\Fb$-quasilibero}
    se $G_x$ (i.e. lo stabilizzatore di $x$) non contiene tori $\Fb$-split non banali.
\end{definition}
\begin{proposition}\label{006}
    $x\in\mf^\alpha(S,O,\Fb)$ è $\Fb$-indecomponibile se e solo se è un punto $\Fb$-quasilibero rispetto all'azione di $G^\alpha$.
\end{proposition}
\begin{proof}
    Una freccia è chiara: se $x$ è una rappresentazione su $\Fb$ decomponibile in $y\oplus z$, allora il suo stabilizzatore in $\GL(\alpha)$ contiene il toro $\Fb$-split $\overline{\Fb}^*\Id_y\times \overline{\Fb}^*\Id_z$. Questo, passato al quoziente, diventa un toro $1$-dimensionale $\Fb$-split contenuto in $(G^\alpha)_x$. 
    
    Viceversa, supponiamo esista un toro $\Fb$-split non banale in $(G^\alpha)_x$. Per il lemma \eqref{001}, la sua preimmagine (definita su $\Fb$) tramite la proiezione $\GL(\alpha)\rightarrow G^\alpha$ è un toro $\Fb$-split $T$, contenente strettamente $\overline{\Fb}(\Id_i)_{i\in I}$. Consideriamo la naturale rappresentazione di $T(\Fb)$ su $\bigoplus_{i\in I}V_i$: essa deve avere  almeno $2$ componenti isotipiche, perché altrimenti $T$ sarebbe interamente contenuto in $\overline{\Fb}(\Id_i)$ (ciascun suo elemento agirebbe moltiplicando tutto lo spazio per uno stesso scalare). Il fatto che il toro sia nello stabilizzatore della rappresentazione significa che l'azione di $T(\Fb)$ su $\bigoplus_{i\in I}V_i$ commuta con quella di $A$ (l'algebra dei cammini su campo $\Fb$), quindi le sue componenti isotipiche sono sottospazi invarianti rispetto all'algebra di cammini. Segue che $x$ è decomponibile come $A$-modulo, e questo conclude.
\end{proof}