

\begin{definition}
Un \textit{quiver} è una coppia $(S,O)$, dove $S$ è un multigrafo, cioè il dato di un insieme di vertici $I$ e un insieme di archi $\Omega$ con estremi in $I$; $O$ è un'orientazione su $S$, cioè una coppia di funzioni $s,t:\Omega\rightarrow I$ che specificano rispettivamente quale tra i due estremi di un dato arco è la \textit{sorgente} e quale il \textit{target}. Scriveremo compattamente $h:i\rightarrow j$ per dire che l'arco $h$ ha sorgente $i$ e target $j$.
\end{definition}
Noi lavoreremo sempre con quiver con numero finito di vertici e di archi, ma ammettiamo l'esistenza di loop, cioè di archi con estremi coincidenti. Non sarà inoltre restrittivo per i nostri scopi supporre $S$ connesso.


\begin{definition}
Una \textit{rappresentazione} (finita) $V$, o più formalmente $(V,\phi)$, su campo $\Fb$ di un quiver $(S,O)$ è il dato di una famiglia di $\Fb$-spazi vettoriali (finiti) indicizzata dai vertici $$V=(V_i)_{i\in I},$$
e di una famiglia di applicazioni lineari indicizzata dagli archi$$\phi=\left(\phi_h:\ V_{s(h)}\rightarrow V_{t(h)}\right)_{h\in \Omega}.$$Indichiamo con $\Repr(S,O,\Fb)$ la classe delle rappresentazioni finite su $\Fb$ di $(S,O)$.
\end{definition}
D'ora in poi supporremo tacitamente che le rappresentazioni siano finite.
\begin{definition}
    Un \textit{morfismo} di rappresentazioni $f:(V,\phi)\rightarrow (W,\psi)$ è una famiglia di applicazioni indicizzata dai vertici $$f=(f_i:V_i\rightarrow W_i)_{i\in I}\ \ \ \ \text{dove }\ \ f_{t(h)}\circ\phi_h=\psi_h\circ f_{s(h)}\ \ \forall\ h\in\Omega.$$
\end{definition}
E' un facile esercizio osservare che i morfismi appena definiti danno una struttura di categoria a $\Repr(S,O,\Fb)$.
\begin{definition}
    Un \textit{cammino} $h$ di lunghezza $l\ge 0$ con sorgente $i\in I$ e target $j\in I$ è una sequenza ordinata di archi $h=(h_l,h_{l-1},\dots,h_1)$ (vuota se $l=0$) tale per cui, se $l\ne 0$, $$s(h_1)=i,\ t(h_l)=j,\ \ \ \ s(h_{k+1})=t(h_k)\ \ \forall\ 1\le k<l.$$E' naturale richiedere che, nel caso di cammini banali (di lunghezza $0$), sorgente e target coincidano. Estendiamo le funzioni $s$ e $t$ ai cammini, ponendo $s(h)=i$ e $t(h)=j$.
\end{definition}
Denotiamo con $e_i$ il cammino banale centrato in $i\in I$.

Dati due cammini $h=(h_l,\dots h_1)$ e $h'=(h'_{l'},\dots,h'_1)$ definiamo l'operazione $*$ di concatenazione tra essi ponendo $$h*h':=\begin{cases}(h_l,\dots,h_1,h'_{l'}\dots,h'_1),\ \text{ con }\ s(h*h')=s(h'),\ t(h*h')=t(h)\ \text{ se }\ t(h')=s(h),\\0\ \text{altrimenti.}\end{cases}$$
\begin{definition}
    Chiamiamo \textit{algebra dei cammini} di $(S,O)$ lo spazio vettoriale $A$ con base formale composta da tutti i cammini (compresi i banali), dotato del prodotto ottenuto estendendo per bilinearità l'operazione (associativa) di concatenazione appena definita.

    L'identità è l'elemento $\sum_{i\in I}e_i$ in quanto $e_i*e_j=\delta_{ij}e_i$.
\end{definition}
Sia $V\in\Repr(S,O,\Fb)$ una rappresentazione qualsiasi. Si ha una naturale struttura di $A$-modulo sinistro su $\bigoplus_{i\in I} V_i$ definendo l'azione di $A$ nel modo seguente: poniamo $$\forall\ i\in I\ \ \ \ \ \ e_i\cdot (v+w):=v\ \text{ se }\ v\in V_i\ \text{ e }\ w\in\bigoplus_{j\ne i}V_j,$$$$\forall\ h\in\Omega,\ \ \ \ \ \ h\cdot (v+w):= \phi_h(v)\ \text{ se }\ v\in V_{s(h)}\ \text{ e }\ w\in\bigoplus_{j\ne s(h)}V_j.$$Estendiamo poi l'azione a tutti i cammini per associatività, e infine a tutti gli elementi di $A$ per linearità. In effetti $1_A$ agisce banalmente.

\medskip
In realtà, come vedremo adesso, le rappresentazioni di $(S,O)$ sono soltanto un modo comodo per vedere i moduli sinistri sull'algebra di cammini, di dimensione finita come spazi vettoriali.
\begin{proposition}
    Esiste un'equivalenza di categorie $\Repr(S,O,\Fb)\stackrel[G]{F}{\rightleftarrows} A$-moduli sinistri di dimensione finita.
\end{proposition}
\begin{proof}
    Costruiamo il funtore $F$:
    \begin{itemize}
    \item Data $V\in\Repr(S,O,\Fb)$, poniamo $F(V)=\bigoplus_{i\in I} V_i$ dotato della struttura di $A$-modulo appena descritta.
    \item Dato un morfismo di rappresentazioni $f:(V,\phi)\rightarrow (W,\psi)$ poniamo $F(f)=\bigoplus_{i\in I}f_i:F(V)\rightarrow F(W)$, che oltre ad essere un'applicazione lineare è anche un omomorfismo di $A$-moduli in quanto per definizione $f_{t(h)}\circ\phi_h(v)=\psi_h\circ f_{s(h)}$.
    \end{itemize}
    Le proprietà funtoriali sono una banale verifica.
    
    Costruiamo adesso il funtore $G$:
    \begin{itemize}
        \item Dato $M$ un $A$-modulo con $\dim_{\Fb}M<\infty$, poniamo $F(M)=(U,\phi)$ dove $$U_i=e_iM\ \ \forall\ i\in I,\ \ \ \ \ \ \phi_h(v)=hv\ \ \forall\ h\in\Omega,\ v\in U_{s(h)}$$
        \item Dato $f:M\rightarrow N$ omomorfismo di $A$-moduli, poniamo $F(f)_i:e_iM\rightarrow e_iN$ l'applicazione indotta da $f$. E' una buona definizione in quanto $f(e_iN)=e_if(N)\subset e_iM$.
    \end{itemize}
    Anche in questo caso le proprietà funtoriali sono pressoché immediate, come è immediato che $G\circ F(V)\simeq_{\text{nat}}V$ e che $F\circ G(M)\simeq_{\text{nat}}M$.
\end{proof}
Per quanto appena visto, non ci sorprende che operazioni di somma diretta, kernel e immagine di omomorfismi, moduli quoziente e sottomoduli, teoremi di omomorfismo, trovino i loro analoghi nel linguaggio delle rappresentazioni:
\begin{itemize}
    \item[]\textbf{Somme dirette.}\\ $(V\oplus W)_i:=V_i\oplus W_i$ con $\phi_h^{V\oplus W}:=\phi_h^V\oplus \phi_h^W$.
    \item[]\textbf{Sottorappresentazioni e quozienti.}\\ $V\subset W$ se $V_i\subset W_i$ e $\phi_h^V=\restriction{\phi_h^W}{V_{s(h)}}$. Sotto queste ipotesi definiremo $(W/V)_i:=W_i/V_i$ con le relative applicazioni lineari che saranno il passaggio al quoziente delle applicazioni $\phi_h^W$.
    \item[]\textbf{Kernel e immagini.}\\ Dato $f:V\longrightarrow W$ omomorfismo di rappresentazioni, $\left(\Ker f\right)_i:=\Ker f_i$ mentre $(\Img f)_i:=\Img f_i$ con le relative $\phi_h$ definite restringendo $\phi_h^V$ e $\phi_h^W$ una volta notato che $\phi_h^V(\Ker f_{s(h)})\subset\Ker f_{t(h)}$ e che $\phi_h^W(\Img f_{s(h)})\subset\Img f_{t(h)}$.
    \item[]\textbf{Teoremi di omomorfismo.}\begin{itemize}
    \item[$\bullet$] Dato un morfismo suriettivo $f:V\twoheadrightarrow W$, si ha $W\simeq V/\Ker f$.
    \item[$\bullet$] Date $U\subset V\subset W$ rappresentazioni, $W/V\simeq (W/U)/(V/U)$.
    \item[$\bullet$] Date $U,V\subset W$ rappresentazioni, $(U+V)/V\simeq U/(U\cap V)$.
    \end{itemize}

\end{itemize}
\begin{definition}
Chiamiamo \textit{dimensione} di una rappresentazione $V$ il vettore $$\dim V:=(\dim V_1,\dim V_2\,\dots,\dim V_n)\in\Nb^I.$$
Dato un vettore dimensionale $\alpha$, si definisce \textit{supporto} di $\alpha$, abbreviato $\Supp(\alpha)$, il sottografo completo di $S$ costituito dai vertici $i$ tali che $\alpha_i\ne 0$. Scriviamo $|\alpha|$ per indicare $\sum_{i\in I}\alpha_i$, detta \textit{altezza} di $\alpha$.
\end{definition}
\begin{definition}
    Se $i\in I$ è un vertice di $(S,O)$ senza loop, denotiamo $S_i$ l'unica rappresentazione di $(S,O)$ dove tutti gli spazi vettoriali sono banali tranne l'$i$-esimo, che ha dimensione $1$. Tale rappresentazione è detta \textit{rappresentazione semplice di centro $i$}.
\end{definition}
\begin{definition}
    Una rappresentazione $V\in\Repr(S,O,\Fb)$ si dice \textit{indecomponibile} se non è banale, e non è isomorfa a una somma diretta di rappresentazioni non banali su $\Fb$. In altre parole, è equivalente all'indecomponibilità come $A$-modulo.
\end{definition}
\begin{definition}
    Una rappresentazione $V\in\Repr(S,O,\Fb)$ è detta \textit{assolutamente indecomponibile} se $\overline{\Fb}\otimes_{\Fb}V\in\Repr(S,O,\overline{\Fb})$ è indecomponibile, cioè se $V$ è indecomponibile vista come rappresentazione su $\overline{\Fb}$.
\end{definition}
Ovviamente le rappresentazioni assolutamente indecomponibili sono anche indecomponibili.

\medskip
Possiamo cominciare a porci una serie di domande naturali del tipo: 
\begin{itemize}
\item[]\virg{Per quali quiver esistono un numero finito di rappresentazioni indecomponibili a meno di isomorfismo? Fissato uno di questi quiver, riesco a caratterizzare le indecomponibili in qualche modo?}
\item[]\virg{Fissato un quiver $(S,O)$, per quali $\alpha\in\Nb^I\setminus\{0\}$ esistono rappresentazioni indecomponibili di dimensione $\alpha$? Nel caso quante sono? La risposta dipende dall'orientazione $O$?}
\end{itemize}


Il teorema di Gabriel (1972), dimostrato dal matematico francese Pierre Gabriel, dà una risposta esauriente alla prima domanda nel caso di campo base algebricamente chiuso. Il teorema di Kac (1980), dovuto al matematico russo Victor Kac (da non confondersi con il polacco Mark Kac) risponde invece alla seconda domanda, nell'ipotesi di campi algebricamente chiusi o di campi finiti.