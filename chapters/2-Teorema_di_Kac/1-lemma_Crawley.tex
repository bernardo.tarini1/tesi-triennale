    In questa sezione ci occuperemo dello studio delle rappresentazioni 
    indecomponibili su campo $\Fb$ algebricamente chiuso, di dimensione 
    $\alpha$, sotto la restrizione $\alpha\in M$ (i.e. $\Supp(\alpha)$ 
    connesso, e $(\alpha,\eps_i)\le 0\ \ \forall\ i\in I$).

\medskip
    Il caso $\Supp\alpha$ Euclideo e $\alpha\in\Delta_+^{im}$ - cioè della forma $k\delta$, con $\delta$ definito in figura \eqref{figu} - è stato ampiamente studiato (si veda ad esempio \cite{[8]}), ma noi ne enunciamo solo il principale risultato, senza dimostrazione.
    \begin{theorem}\label{cojs}
        Sia $(S,O)$ un quiver, e $\alpha\in\Delta_+^{im}$ a supporto Euclideo, quindi della forma $k\delta$. Allora su campi algebricamente chiusi esiste una famiglia a un parametro di rappresentazioni indecomponibili di dimensione $\alpha$ non isomorfe tra loro.
    \end{theorem}

    \begin{lemma}\label{wyu}
        Supponiamo che $\alpha=\beta^1+\dots+\beta^r$ dove $r\ge 2$ e i 
        $\beta^j\ge 0$ non nulli. Allora vale una delle seguenti:
        \begin{itemize}
            \item[$1)$] $\ \ \ (\alpha,\alpha)<\sum (\beta^i,\beta^i);$
            \item[$2)$] $\ \ \ \Supp(\alpha)\ \text{è Euclideo, }
            (\alpha,\alpha)=0,\ \ \Qb\alpha=\Qb\beta^i\ \ \forall\ i\le r.$
        \end{itemize}
    \end{lemma}
    \begin{proof}
        Supponiamo che la condizione $1)$ non valga, cioè che $(\alpha,\alpha)-\sum (\beta^i,\beta^i)\ge 0$. Riarrangiando si ottiene
        \begin{equation}\label{aaa}
        \sum_i (\alpha-\beta^i,\beta^i)\ge 0
        \end{equation}
        quindi esiste un $\beta$ scelto fra i $\beta^i$ per cui $(\alpha-
        \beta,\beta)\ge0$. Manipoliamo un po' questo prodotto scalare, indicando con $J$ l'insieme dei vertici di $\Supp\alpha$:
        $$(\alpha-\beta,\beta)=\sum_{i\in J}(\alpha_i-
        \beta_i)\beta_i(\eps_i,\eps_i)+\sum_{(i\ne j)\in J}(\alpha_j-
        \beta_j)\beta_i(\eps_i,\eps_j)=$$$$=\sum_{i\in J}(\alpha_i-
        \beta_i)\beta_i\frac{(\alpha,\eps_i)}{\alpha_i}+\sum_{i\ne j\in 
        J}\left(\frac{\beta_i^2}{\alpha_i^2}-\frac{\beta_j}
        {\alpha_j}\frac{\beta_i}
        {\alpha_i}\right)\alpha_i\alpha_j(\eps_i,\eps_j),$$che è uguale, 
        simmetrizzando la seconda sommatoria, a $$\sum_{i\in J}(\alpha_i-
        \beta_i)\beta_i\stackrel[\le 0]{}{(\alpha,\eps_i)}/\alpha_i+\frac{1}
        {2}\sum_{i\ne j\in J}\left(\frac{\beta_i}{\alpha_i}-\frac{\beta_j}
        {\alpha_j}\right)^2\alpha_i\alpha_j\stackrel[\le 0]{}
        {(\eps_i,\eps_j)}\ge 0.$$
        Quindi tutti i termini di ciascuna sommatoria devono annullarsi. In 
        particolare quando i vertici $i$ e $j$ sono connessi da almeno un arco $\frac{\beta_i}{\alpha_i}=\frac{\beta_j}
        {\alpha_j}$ in quanto $(\eps_i,\eps_j)<0$. Ricordando che $\Supp\alpha$ è connesso, si ottiene così che $\Qb\beta=\Qb\alpha$. Ma allora $\alpha_i-\beta_i>0\ \ \forall\ i\in J$ 
        quindi $(\alpha,\eps_i)=0\ \ \forall\ i\in J$, da cui segue che 
        $\Supp(\alpha)$ è Euclideo (il prodotto scalare in $J$ è degenere), e che $(\alpha,\alpha)=(\alpha-
        \beta,\beta)=0$. Abbiamo dimostrato che nessun addendo della sommatoria 
        in \eqref{aaa} può essere positivo, quindi sono tutti nulli e il 
        discorso fatto si può riapplicare a uno qualsiasi dei $\beta^i$, cioè 
        la richiesta del punto $2)$ è soddisfatta.
    \end{proof}    
    Grazie a questo lemma possiamo adesso affermare che, nel caso di supporto 
    non Euclideo, \virg{quasi tutte} le rappresentazioni di dimensione $\alpha$ 
    sono indecomponibili.
    \begin{theorem}\label{012}
        Sia $(S,O)$ un quiver e $\Fb$ un campo algebricamente chiuso. Se $\alpha\in\Gamma_+$ sta nella camera fondamentale e ha supporto non Euclideo allora esiste un aperto non banale di $\mf^\alpha_{\ind}(S,O,\Fb)$ fatto di indecomponibili.
    \end{theorem}
    \begin{proof}
        Siano $\beta,\gamma\ge 0$ non nulli tali che $\beta+\gamma=\alpha$. Consideriamo il morfismo di varietà algebriche  
        \begin{align*}
        f_{\beta\gamma}:\ \GL(\alpha)\times\mf^\beta(S,O,\Fb)\times\mf^\gamma(S,O,\Fb)&\longrightarrow \mf^\alpha(S,O,\Fb)\\
        (g,u,v)&\longmapsto g\cdot (u\oplus v)
        \end{align*}
        Se vediamo $\GL(\beta)\times \GL(\gamma)$ immerso (come matrici diagonali a blocchi) in 
        $\GL(\alpha)$, possiamo definire una sua azione algebrica sul dominio di 
        $f_{\beta\gamma}$ ponendo $$h\cdot (g,u,v)=(gh^{-1},h_1\cdot u,h_2\cdot 
        v)\ \ \text{dove } h=(h_1,h_2)\in \GL(\beta)\times \GL(\gamma).$$Questa 
        azione è chiaramente libera quindi la chiusura di 
        ciascuna sua orbita ha dimensione $d=\dim \GL(\beta)+\dim \GL(\gamma)$. 
        Inoltre $f_{\beta\gamma}(h\cdot(g,u,v))=f_{\beta\gamma}(g,u,v)$, di 
        conseguenza le fibre di $f_{\beta\gamma}$ hanno tutte dimensione almeno 
        $d$. Questo, unito al fatto che $$\dim \mf^\lambda(S,O,\Fb)-\dim \GL(\lambda)=-(\lambda,\lambda)\ \ \forall\ \lambda\in\Gamma_+,$$ ci permette di dire che 
        $$\dim\overline{\Img(f_{\beta\gamma})}\le\dim 
        \GL(\alpha)+\dim\mf^\beta(S,O,\Fb)+\dim\mf^\gamma(S,O,\Fb)-d\Rightarrow$$$$
        \dim\mf^\alpha(S,O,\Fb)-\dim\overline{\Img(f_{\beta\gamma})}\ge -
        (\alpha,\alpha)+(\beta,\beta)+(\gamma,\gamma)\stackrel{\eqref{wyu}}{>}0$$
        Per concludere osserviamo che un oggetto decomponibile di 
        $\mf^\alpha(S,O,\Fb)$ deve essere contenuto in 
        $$\bigcup_{\beta+\gamma=\alpha}\Img(f_{\beta\gamma}),$$che per quanto 
        detto ha chiusura di dimensione strettamente inferiore a $\dim 
        \mf^\alpha(S,O,\Fb)$.
    \end{proof}
    \begin{corollary}\label{cor}
        Sia $(S,O)$ un quiver. Se $\alpha\in\Gamma_+$ sta nella camera fondamentale allora esistono infinite rappresentazioni indecomponibili di dimensione $\alpha$ su un campo $\Fb$ algebricamente chiuso, non isomorfe tra loro.
    \end{corollary}
    \begin{proof}
        Il caso supporto Euclideo è una diretta conseguenza del teorema \eqref{cojs}. Altrimenti, nello stesso stile della dimostrazione \eqref{002}, supponiamo per assurdo che esistano finite $G^\alpha$-orbite di indecomponibili in $\mf^\alpha(S,O,\Fb)$. Allora una di queste, $G^\alpha\cdot x$, deve contenere nella sua chiusura l'aperto (irriducibile) fatto di indecomponibili ottenuto nel teorema appena mostrato; in altre parole dev'essere densa. Tuttavia questo significa che la mappa $G\rightarrow \mf^\alpha(S,O,\Fb)$ tale che $g\mapsto g\cdot x$ è dominante e quindi che $\dim G^\alpha\ge \dim \mf^\alpha(S,O,\Fb)$. Ma allora $(\alpha,\alpha)-1\ge 0$, che è assurdo perché $\alpha\in M$.
    \end{proof}