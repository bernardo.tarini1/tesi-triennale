\begin{proposition}\label{015}
    Sia $\Fb$ un campo algebricamente chiuso qualsiasi. Allora le rappresentazioni indecomponibili definite su $\Fb$ restano indecomponibili in una qualsiasi estensione $\Eb\supset\Fb$. Inoltre rappresentazioni non isomorfe su $\Fb$ restano non isomorfe su $\Eb$.
\end{proposition}
\begin{proof}
    Sia $x=(x_h:V_{s(h)}\longrightarrow V_{t(h)})_{h\in\Omega}$ una rappresentazione in $\mf^\alpha(S,O,\Fb)$ indecomponibile. Supponiamo per assurdo che $x\simeq y\oplus z$ su $\Eb$, e chiamiamo $g=(g_i)_{i\in I}\in\GL(\alpha)(\Eb)$ questo isomorfismo. Sia $\widetilde{\Fb}$ il campo finitamente generato su $\Fb$ dalle entrate delle $y_h$, delle $z_h$, delle $g_i$ (le entrate delle $g_i^{-1}$ stanno in questo campo). Possiamo scrivere $\widetilde{\Fb}=\Fb(\xi_1,\dots,\xi_h)(\gamma_1,\dots,\gamma_r)$ dove le $\xi_i$ sono algebricamente indipendenti su $\Fb$ e i $\gamma_i$ algebrici su $\Fb(\xi_1,\dots,\xi_h)$, che generano $\widetilde{\Fb}$ come $\Fb(\xi_1,\dots,\xi_h)$-spazio vettoriale. Sia $A=\Fb[\xi_1,\dots,\xi_h][\alpha_1,\dots,\alpha_r]$. Esso si ottiene quozientando $\Fb[x_1,\dots,x_{h+r}]$ per un certo ideale primo $P$ e ha campo delle frazioni $\widetilde{\Fb}$. Sia $a\ne0$ un elemento di $A$ tale che le entrate di tutte le matrici $y_h$, $z_h$, $g_i$ e $g_i^{-1}$ moltiplicate per $a$ vadano in $A$. Mostriamo che esiste un omomorfismo $f$ di specializzazione da $A$ in $\Fb$ che non mandi $a$ in $0$: detta $\overline{a}$ una preimmagine di $a$ tramite la proiezione $\Fb[x_1,\dots,x_{h+r}]\rightarrow A$, basta trovare un omomorfismo di specializzazione $\overline{f}:\Fb[x_1,\dots,x_{h+r}]\rightarrow \Fb$ tale che $\overline{f}(P)=0$ e $\overline{f}(\overline{a})\ne 0$. Questo è immediato considerato che $\overline{a}\not\in P$ e quindi esiste un punto $(\beta_1,\dots,\beta_h,\eta_1,\dots,\eta_r)$ in $\mathcal{V}(P)\setminus\mathcal{V}((a))$. In conclusione la $f$ definita valutando $\xi_i\mapsto\beta_i\in\Fb$ e $\gamma_j\mapsto\eta_j\in\Fb$ è ben definita e fa al caso nostro. Sia $f_a:A_a\rightarrow\Fb$ la localizzazione di $f$.

    A questo punto è facile rendersi conto che $f_a(y)\oplus f_a(z)\simeq f_a(x)=x$ tramite l'isomorfismo dato da $f_a(g)$ con inversa $f_a(g^{-1})$, dove $f_a$ opera componente per componente su tutte le matrici. Questo è assurdo perché avevamo supposto $x$ indecomponibile su $\Fb$.
    
    Nello stesso identico stile, supponiamo che due rappresentazioni $x,y\in\mf^\alpha(S,O,\Fb)$ siano non isomorfe su $\Fb$, ma che su $\Eb$ esista un isomorfismo $g=(g_i)_{i\in I}:x\rightarrow y$. Sia $\widetilde{\Fb}$ il campo finitamente generato su $\Fb$ dalle entrate delle $g_i$ (le entrate delle $g_i^{-1}$ stanno in questo campo). Esattamente come prima, sfruttando che $\Fb=\Fbb$ si può trovare un omomorfismo $\Fb$-lineare $s:R\rightarrow \Fb$, dove $R$ è una $\Fb$-algebra in $\Eb$ contenente tutte le entrate delle $g_i$ e delle $g_i^{-1}$. Si conclude notando che $(s(g_i))_{i\in I}$ è un isomorfismo tra $s(x)=x$ e $s(y)=y$ con inversa $(s(g_i^{-1}))_{i\in I}$.
\end{proof}
\begin{theorem}\label{8333}
    Sia $(S,O)$ un quiver, $\Fb$ un campo algebricamente chiuso, $\Char\Fb=p$ e $\alpha\in\Gamma_+$. Allora valgono le seguenti:
    \begin{itemize}
    \item[a)] Se $\alpha\in\Delta_+$ si ha che $\Ic^\alpha(S,O,\Fb)>0$.
    \item[b)] Se $\alpha\in\Delta_+^{im}$ si ha che $\Ic^\alpha(S,O,\Fb)=\infty$.
    \end{itemize}
\end{theorem}
\begin{proof}
    È una diretta conseguenza della proposizione appena dimostrata e del teorema \eqref{013}.
\end{proof}

Ciò che ci manca da dimostrare sono i teoremi di esistenza nel caso di campi algebricamente chiusi di caratteristica $0$. Enunciamo una versione del teorema di Chevalley (si veda \cite{stacks-project}, 29.22.3) che sarà utile per i nostri scopi.
\begin{theorem}
    Sia $f:\ X\rightarrow Y$ un morfismo di schemi affini di tipo finito su un anello noetheriano. Allora l'immagine di $f$ è un costruibile.
\end{theorem}
Dato un qualsiasi $\delta\in\Gamma_+$,  $\mf^\delta(S,O,\Fb)$ può essere visto come insieme degli $\Fb$-punti di un certo schema affine (di tipo finito) $\mf^\delta$ su $\Zb$, con anello delle coordinate $R^\delta$. Anche $\GL(\delta)$ può essere visto come uno schema affine (di tipo finito) di gruppo su $\Zb$, con anello delle coordinate  $$Q^\delta=\Zb\left[\textbf{X}_1,\cdots,\textbf{X}_n,(\det\textbf{X}_1)^{-1},\dots,(\det\textbf{X}_n)^{-1}\right],$$dove le $\textbf{X}_i$ sono matrici di indeterminate con rispettive dimensioni $\delta_i$. Con questo nuovo punto di vista possiamo adesso dare una definizione un po' più generale di $\Fb$-punti assolutamente indecomponibili in $\mf^\alpha$: sono tutti e soli gli $\Fb$-punti che non stanno nell'immagine di (i.e. non fattorizzano tramite) nessun morfismo del tipo $$h_{\beta\gamma}:\ \GL(\alpha)^\alpha\stackrel[\Spec\Zb]{}{\times}\mf^\beta\stackrel[\Spec\Zb]{}{\times}\mf^\gamma\longrightarrow \mf^\alpha,$$dove $\beta,\gamma\in\Gamma_+$ sono tali che $\beta+\gamma=\alpha$ e $h_{\beta\gamma}$ a livello di punti manda $(g,x,y)\mapsto (g\cdot (x\oplus y))$. Per il teorema di Chevalley, il complementare $C^\alpha$ dell'unione di queste immagini, al variare di $\beta+\gamma=\alpha$, deve essere un costruibile. A meno di raffinamento possiamo quindi dire che $C^\alpha$ è ricoperto da un certo insieme finito di schemi affini di tipo finito su $\Zb$, $\{C_k^\alpha\}$. 
\begin{theorem}
    Sia $\Fb$ un campo algebricamente chiuso di caratteristica $0$. Allora:
    \begin{itemize}
        \item[] \center Se $\alpha\in\Delta_+$ si ha che $\mathcal{I}^\alpha(S,O,\Fb)>0$.\footnote{In realtà con un po' più di attenzione si può dimostrare anche che, per $\alpha\in \Delta_+^{im}$, le classi di isomorfismo di indecomponibili su $\Fb$ di dimensione $\alpha$ sono infinite, analogamente a quanto fatto in caratteristica $p$.}
    \end{itemize}
\end{theorem}
\begin{proof}
Grazie alla proposizione \eqref{015} possiamo limitarci ad analizzare il caso $\Fb=\overline{\Qb}$. Per il teorema \eqref{8333}, 
%unito alla proposizione \eqref{015}, 
se $\alpha\in\Delta_+$, $C^\alpha$ contiene almeno un $\Fpp$-punto per ogni primo $p$, e questo è anche 
definito su $\Fb_p$. Di conseguenza possiamo supporre senza perdita di generalità che $C_1^\alpha$ contenga un $\Fpp$-punto per infiniti $p$. In altre parole, il suo anello di coordinate ridotto $A^\alpha$ è tale per cui 
\begin{equation}\label{7coc}\dim_{\Fpp}\Hom_{\Zb}(A^\alpha,\ \Fpp)=\dim_{\Fpp}\Hom_{\Fb_p}(\Fb_p\stackrel[\Zb]{}{\otimes}A^\alpha,\Fpp)>0\ \ \forall\ \alpha\in\Delta_+
%\ \ \text{se}\ \ \alpha\in\Delta_+\\\infty\ \ \text{se}\ \ \alpha\in\Delta_+^{im}\end{cases}
\end{equation}per infiniti $p$. 
Di conseguenza si ha che $$\dim_{\Fb_p}\Fb_p\stackrel[\Zb]{}{\otimes}A^\alpha>0\ \ \forall\ \alpha\in\Delta_+.
%\ \ \text{se}\ \ \alpha\in\Delta_+\\\infty\ \ \text{se}\ \ \alpha\in\Delta_+^{im}\end{cases}
$$
Per dimostrare il punto a) del teorema ci basterebbe far vedere che $$\dim_{\Qbb}\Qbb\stackrel[\Zb]{}{\otimes}A^\alpha>0\ \ \forall\ \alpha\in\Delta_+,
%\ \ \text{se}\ \ \alpha\in\Delta_+\\\infty\ \ \text{se}\ \ \alpha\in\Delta_+^{im}\end{cases}
\ \ \ \text{cioè che}\ \ \  \dim_{\Qb}\Qb\stackrel[\Zb]{}{\otimes}A^\alpha>0\ \ \forall\ \alpha\in\Delta_+,
%\ \ \text{se}\ \ \alpha\in\Delta_+\\\infty\ \ \text{se}\ \ \alpha\in\Delta_+^{im}\end{cases}
$$in quanto $A^\alpha$ è ridotto e quindi lo sono anche le tensorizzazioni per $\Qb$ e $\Qbb$ (si vedano come localizzazioni).

%Concentriamoci prima sul punto a). 
Supponiamo per assurdo che $\Qb\otimes_\Zb A^\alpha=0$ per un certo $\alpha\in\Delta_+$, cioè che $(\Zb^*)^{-1}A^\alpha=0$. Questo significa che, detta $i:\Zb\rightarrow A^\alpha$ la mappa associata allo schema su $\Zb$, $i(m)=0$ per qualche intero $m>0$. Ma questo significa che per tutti i primi $p$ che non dividono $m$ vale $\Fb_p\otimes_\Zb A^\alpha=0$, in contraddizione con \eqref{7coc}.

%Veniamo al punto b). Indichiamo con $\overline{a}$ l'immagine di un qualsiasi $a\in A$ visto in $\Qb\otimes_\Zb A^\alpha$, e fissiamo $\{x_1,\dots,x_t\}$ un sistema finito di generatori di $A^\alpha$ come anello. Supponiamo per assurdo che per un certo $\alpha\in\Delta_+^{im}$ si abbia $\dim_{\Qb}\Qb\otimes_\Zb A^\alpha<\infty,$. Allora esiste $\{f_1,\dots,f_r\}\subset A$ tale che $\{\overline{f}_1,\dots,\overline{f}_r\}$ è un sistema finito di generatori per $\Qb\otimes_\Zb A^\alpha$ come spazio vettoriale. Si deve quindi avere che $$M\overline{f}_i\overline{f}_j=\sum_k c_{ijk}\overline{f}_k\ \ \forall\ i,j\le r,\ \ \ \ \ \ \ \ M\overline{x}_l=\sum_k d_k\overline{f}_k\ \ \forall\ l\le t,$$dove $c_{ijk}$, $d_i$ e $M$ sono opportune costanti intere, con $M>0$. Vedendo la tensorizzazione con $\Qb$ come una localizzazione per $i(\Zb\setminus\{0\})$, deduciamo perciò che esiste un intero $N$ per il quale$$NMf_if_j=N\sum_k c_{ijk}f_k\ \ \forall\ i,j\le r,\ \ \ \ \ \ \ \ NMx_l=N\sum_k d_kf_k\ \ \forall\ l\le t.$$Questo è assurdo perché significa che per ogni primo $p$ non divisore di $NM$ l'insieme $\{f_1,\dots,f_r\}\subset \Fb_p\otimes_\Zb A^\alpha$ è un sistema di generatori per $\Fb_p\otimes_\Zb A^\alpha$ come spazio vettoriale.
\end{proof}