In questa sezione andremo a studiare lo spazio delle rappresentazioni di quiver assumendo che il campo $\Fb$ di definizione sia un campo finito $\Fb_q$ (dove $q=p^t)$. Vedremo che la trattazione di questo caso, oltre ad essere già interessante di per sé, sarà essenziale per ottenere risultati anche per campi algebricamente chiusi. Ma andiamo con ordine.

\medskip
Indichiamo con $\mathcal{I}^\alpha(S,O,q)$ il numero di classi di isomorfismo di rappresentazioni di $(S,O)$, di dimensione $\alpha$, indecomponibili su $\Fb_q$, o equivalentemente come il numero di orbite di $G^\alpha(\Fb_q)$ su $\mf^\alpha(S,O,\Fqq)$, con un rappresentante in $\Fb_q$ e $\Fb_q$-indecomponibile. I tre lemmi che seguiranno hanno l'obiettivo di mostrare che $\Ic^\alpha(S,O,q)$ non dipende dall'orientazione $O$.

\begin{lemma}\label{004}
    Sia $G$ un gruppo finito e sia $V\simeq\Fb_q^n$ una sua rappresentazione su $\Fb_q$. Allora detta $V^*$ la rappresentazione duale, il numero di orbite di $G$ in $V$ è pari al numero di orbite in $V^*$.
\end{lemma}
\begin{proof}
    Denotiamo con $A$ (risp. $A^*$) lo spazio vettoriale delle funzioni (di insiemi) da $V$ (risp. $V^*$) in $\Cb$, con la ovvia azione di $G$ data da $g\cdot f=f(g^{-1}\bullet)\ \ \forall\ g\in G,\ f\in A$ (risp. $A^*$). Le funzioni $G$-invarianti di $A$ e $A^*$ corrispondono alle funzioni definite su $V/G$ e $V^*/G$, quindi hanno dimensione - come spazi vettoriali - pari al numero di orbite di $G$ su $V$ e $V^*$ rispettivamente. Siamo perciò alla ricerca di un isomorfismo di spazi vettoriali tra $A^G$ e $\left(A^*\right)^G$.
    Sembra fatto ad hoc l'accoppiamento dato dalla trasformata di Fourier $\widehat{}\ :\ A\rightarrow A^*$ così definita:
    $$\widehat{f}(\xi)=q^{-\frac{n}{2}}\sum_{v\in V}f(v)\chi(\xi(v)),\ \ \forall\ \xi\in V^*,\ f\in A,$$
    dove $\chi$ è un qualsiasi carattere complesso non banale del gruppo additivo $\Fb_q\simeq\left(\Zb/p\Zb\right)^t$. Innanzitutto è evidente che mandi funzioni $G$-invarianti in funzioni $G$-invarianti. Inoltre, usando l'isomorfismo canonico $V\simeq V^{**}$ e quindi $A\simeq A^{**}$, possiamo definire analogamente anche la trasformata $\ \widehat{}\ :A^*\rightarrow A$ e ottenere che $\forall\ f\in A$ vale
    $$\widehat{\widehat{f\,}}(v)=q^{-\frac{n}{2}}\sum_{\xi\in V^*}\widehat{f}(\xi)\chi(v(\xi))=q^{-n}\sum_{\substack{\xi\in V^*\\w\in V}}f(w)\chi\left(\xi(v+w)\right)=$$
    $$q^{-n}\sum_{\xi\in V^*}f(-v)+q^{-n}\sum_{u\in V\setminus\{0\}}f(u-v)\sum_{\xi\in V^*}\chi(\xi(u))=f(-v)$$ in quanto $$\sum_{\xi\in V^*}\chi\left(\xi(u)\right)=q^{n-1}\sum_{x\in\Fb_q}\chi(x)=0$$ perché somma di radici $p$-esime dell'unità tutte con molteplicità $|\ker\chi|=p^{t-1}$.
    
    Ovviamente i medesimi passaggi ci fanno ottenere che $\widehat{\widehat{f\,}}(v)=f(-v)$ anche nel caso in cui $f\in A^*$ e $v\in V^*$, da cui segue che la trasformata è un isomorfismo tra $A$ e $A^*$, e quindi un isomorfismo tra $A^G$ e $\left(A^*\right)^G$, che è proprio ciò che cercavamo.
\end{proof}
\begin{definition}
    Siano $G$ un gruppo lineare algebrico su $\Fqq$, e $V$ una sua rappresentazione finita definita su $\Fb_q$. Sia $T$ un toro $\Fb_q$-split di $G$. Denotiamo con $d(T,V)$ il numero di orbite $G(\Fb_q)\cdot x$, con $x\in V(\Fb_q)$, di $G(\Fb_q)$ su $V(\Fb_q)$ tali che la classe di $G(\Fb_q)$-coniugio di un toro $\Fb_q$-split massimale in $G_x$ corrisponda a quella di $T$ (in questo caso diciamo che $x$ \textit{massimizza} $T$).
\end{definition}
        $d(T,V)$ è ben definito in quanto gli stabilizzatori $G_y$ sono tutti $G(\Fb_q)$-coniugati al variare di $y\in G(\Fb_q)\cdot x$, così come i tori $\Fb_q$-split massimali in $G_x$ sono $G(\Fb_q)$-coniugati tra loro (\cite{[2]}, Cap. XX).
\begin{lemma}\label{007}
    Riprendendo la notazione della precedente definizione, e supponendo l'azione di $G$ fedele, sia $T$ un toro $\Fb_q$-split di $G$. Allora $d(T,V)=d(T,V^*)$.
\end{lemma}
\begin{proof}

    Procediamo per induzione su $\dim V$. Il caso base in dimensione $0$ è banale, assumiamo quindi l'ipotesi induttiva. Denotiamo con $W=N_G(T)/T$, $\mathfrak{c}=V^T$, $\mathfrak{c}'=(V^*)^T$ (si noti che tutti questi oggetti sono definiti su $\Fb_q$). Chiaramente se $x\in V(\Fb_q)$ (risp. $V^*(\Fb_q)$) massimizza $T$ allora esiste un punto $y\in G(\Fb_q)\cdot x\cap\mathfrak{c}$ (risp. $y\in G(\Fb_q)\cdot x\cap\mathfrak{c}'$).
    
    Supponiamo in un primo momento $T$ non banale. Si vede subito che $\mathfrak{c}$ e $\mathfrak{c}'$ sono sottospazi stretti (l'azione è fedele) e che sono $N_G(T)$-invarianti. Poiché inoltre $T$ agisce banalmente su entrambi, viene indotta un'azione di $W$ definita su $\Fb_q$. 

    Mostriamo che $x\in\mathfrak{c}(\Fb_q)$ (o $\mathfrak{c'}(\Fb_q)$) è $\Fb_q$-quasilibero rispetto a $W$ se e solo se $T$ è un toro split massimale in $G_x$. In effetti se $S\subset G_x$ è un toro split contenente strettamente $T$, $S/T$ è un toro split non banale di $W$ (per il lemma \eqref{003}). Viceversa se esiste un toro split non banale $S\subset W$, Detta $N'$ la sua preimmagine tramite $N_G(T)\rightarrow W$ si ha la successione esatta (definita su $\Fb_q$) $1\rightarrow T\rightarrow N'\rightarrow S\rightarrow 1$, che per il lemma \eqref{001} ci permette di dire che $N'$ è un toro split non banale.

    \medskip
    Ora, presi $x,y$ punti quasiliberi in $\mathfrak{c}(\Fb_q)$ (o $\mathfrak{c'}(\Fb_q)$) rispetto a $W$, tali che $y=g\cdot x$ per qualche $g\in G(\Fb_q)$, si ha che stanno nella stessa orbita anche tramite $N_G(T)(\Fb_q)$ (e quindi tramite $W(\Fb_q)$). Infatti $T$ e $gTg^{-1}$ sono tori $\Fb_q$-split massimali in $G_y$, quindi sono coniugati tramite $g_1\in G_y(\Fb_q)$. Di conseguenza $g_1g$ è un elemento di $N_G(T)(\Fb_q)$ che manda $x$ in $y$.

    Abbiamo quindi stabilito che le orbite di punti in $\mathfrak{c}(\Fb_q)$ (o $\mathfrak{c}'(\Fb_q)$), $\Fb_q$-quasiliberi rispetto a $W$, corrispondono alle orbite di punti $x$ che massimizzano $T$, quindi sono in numero $d(T,V)$ e $d(T,V^*)$ rispettivamente. Affermiamo che $\mathfrak{c}$ e $\mathfrak{c}'$ sono duali una dell'altra come rappresentazioni di $N_G(T)$, quindi di $W$. In effetti, per un discorso identico a quello fatto in \eqref{001}, $N_G(T)$ centralizza $T$, quindi vedendo $V$ e $V^*$ come $T$-rappresentazioni, possiamo scomporle in componenti isotipiche $N_G(T)$-invarianti $$V=\bigoplus_{m\in\Zb^n}R_m\ \ \ V^*=\bigoplus_{m\in\Zb^n}S_m,$$ dove $S_m=(R_{-m})^*$. Da qui segue immediatamente che $\mathfrak{c}=R_0$ e $\mathfrak{c'}=S_0$ sono duali una dell'altra. Essendo $\dim\mathfrak{c}<\dim V$ concludiamo applicando l'ipotesi induttiva per il toro banale. 
    
    \medskip
    Per concludere il passo induttivo occorre analizzare infine il caso $T=\{e\}$. Calcoliamo $d(e,V)$ per differenza: esso è uguale al numero di orbite di $G(\Fb_q)$ su $V(\Fb_q)$ meno $\sum d(S,V)$, dove la somma è fatta al variare delle classi di $G(\Fb_q)$-coniugio di tori $\Fb_q$-split non banali. Per quanto appena dimostrato e per il lemma \eqref{004} tutto ciò è uguale a $d(e,V^*)$.
\end{proof}
    Si noti l'importanza di questo lemma applicato al caso del toro banale: ci sta dicendo che le $G(\Fb_q)$-orbite di punti $\Fb_q$-quasiliberi in $V(\Fb_q)$ e $V^*(\Fb_q)$ sono le stesse in numero. Questo è per noi di grande interesse nel caso dell'azione di $G^{\alpha}$ sullo spazio $\mathfrak{m}^\alpha(S,O,\Fqq)$: per il lemma \eqref{006} le $G^\alpha(\Fb_q)$-orbite $\Fb_q$-quasilibere corrispondono alle rappresentazioni indecomponibili a meno di isomorfismo.

    Ci serve un ultimo piccolo raffinamento:
    \begin{lemma}
        Siano $V_1,\ V_2$ rappresentazioni finite di $G$, definite su $\Fb_q$. Allora $d(T,V_1\oplus V_2)=d(T,V_1\oplus V_2^*)$.
    \end{lemma}
    \begin{proof}
        Dato $x\in V_1$ sia $d_x$ la funzione definita come $d$, ma con $G_x$ al posto di $G$. Consideriamo un sistema di rappresentanti per le orbite su $V_1(\Fb_q)\oplus V_2(\Fb_q)$ $$P=\{(x_i,y_{ij})\in V_1(\Fb_q)\oplus V_2(\Fb_q):\ j\le a_i\in\Zb_{>0},\ i\le k\}$$ costruito così: $R=\{x_1,\dots,x_k\}$ sono rappresentanti delle $G(\Fb_q)$-orbite su $V_1(\Fb_q)$, mentre $\{y_{i,1},\dots,y_{i,a_i}\}$ sono rappresentanti delle orbite di 
        $G_{x_i}(\Fb_q)$ su $V_2(\Fb_q)$. È chiaro che questo sia un sistema completo di rappresentanti. 
        Calcolare $d(T,V_1\oplus V_2)$ equivale a contare quanti di questi punti 
        massimizzano $T$. Chiaramente è necessario che la prima coordinata stia 
        nell'insieme $S$ degli $x\in V_1$ tali che $G_x$ contiene $g_xTg_x^{-1}$ per un 
        qualche $g_x\in G(\Fb_q)$. Poiché $G_{(a,b)}=(G_a)_b$, fissando $x_i\in S\cap R$ si ha 
        che $(x_i,y_{ij})$ massimizza $T$ in $G$ se e solo se $y_{ij}$ massimizza $T$ 
        in $G_{x_i}$. Quindi $$d(T,V_1\oplus V_2)=\sum_{x\in 
        S\cap R}d_x(g_xTg_x^{-1},V_2)\stackrel{\eqref{007}}{=}\sum_{x\in 
        S\cap R}d_x(g_xTg_x^{-1},V_2^*)=d(T,V_1\oplus V_2^*)$$
        \end{proof}
        Possiamo finalmente dire che $\mathcal{I}^\alpha(S,O,q)$, cioè $d(\{e\},\mf^\alpha(S,O,\Fqq))$, non 
        dipende dall'orientazione $O$: capovolgere un arco equivale a fare un duale in 
        uno degli addendi della somma diretta $$\mf^\alpha(S,O,\Fqq)=\bigoplus_{h:\ i\rightarrow j}V_i^*\otimes V_j.$$ 

        Adesso siamo pronti per affermare i seguenti importanti risultati su campo finito.
        \begin{theorem}\label{010}
            Sia $(S,O)$ un quiver, $\alpha\in\Gamma_+$, e sia $\Fb=\Fb_q$ ($q=p^t$) un campo finito. Valgono i seguenti fatti:
            \begin{itemize}
                \item[a)] $\mathcal{I}^\alpha(S,O,q)$ non dipende da $O$. Se è non nullo e $\alpha\ne\eps_i$, con $i$ senza loop, allora $s_i(\alpha)\in\Gamma_+$ e $\mathcal{I}^\alpha(S,q)=\mathcal{I}^{s_i(\alpha)}(S,q)$. 
                \item[b)] Se $\alpha\not\in\Delta_+$ tutte le rappresentazioni di dimensione $\alpha$ sono decomponibili.
                \item[c)] Se $\alpha\in\Delta_+^{re}$ esiste un'unica rappresentazione indecomponibile di dimensione $\alpha$.
%                \item[d)] (USA LANG-WEIL ???) Per $\alpha\in\Delta_+^{im}$ si ha   
%               $$\lim_{t\rightarrow\infty}\frac{\mathfrak{n}^{\alpha}(S,p^t)}{p^{(1-
%               (\alpha,\alpha))t}}\ge 1$$
            \end{itemize}
        \end{theorem}
        \begin{proof}
            La prima parte del punto a) è già stata fatta. Adesso possiamo quindi invertire gli archi in modo che il vertice $i$ diventi una sorgente di $(S,O')$; poiché esiste una rappresentazione indecomponibile di $(S,O')$ (e $\alpha\ne\eps_i$) il funtore di riflessione manda $\alpha$ in $s_i(\alpha)\in\Gamma_+$ (osservazione \eqref{lollo}) e ci permette di dire che $\mathcal{I}^\alpha(S,q)=\mathcal{I}^{s_i(\alpha)}(S,q)$.

            Veniamo al punto b). Supponiamo che $\alpha\not\in\Delta_+^{re}$ sia tale che $\Ic (S,q)>0$. Usando ripetutamente il punto a) si ottiene che $w(\alpha)\in\Gamma_+$ e $\mathcal{I}^{w(\alpha)}(S,q)=\mathcal{I}^\alpha(S,q)\ne 0\ \ \forall\ w\in W$. Preso $\beta=\sum_i k_i\eps_i\in W\cdot \alpha$ di altezza minima si ha chiaramente che $(\beta,\eps_i)\le 0\ \ \forall\ i$: in caso contrario $\eps_i$ sarebbe radice semplice, e applicando $s_i$ contraddiremmo la minimalità di $\beta$. Inoltre il supporto di $\beta$ è connesso altrimenti ovviamente $\mathcal{I}^{\beta}(S,q)=0$. Segue che $\beta\in M\Rightarrow \alpha\in\Delta_+^{im},$ e questo conclude. 
            
            Per il punto c) ci basta notare che per definizione di $\Delta_+^{re}$ esiste un elemento $w=s_{i_l}s_{i_{l-1}}\cdots s_{i_1}\in W$ con $l\ge 0$ minimo tale che $w(\eps_k)=\alpha$, per un qualche $\eps_k\in\Pi$ radice semplice. Se $i_1\ne k$, per il punto a) si avrebbe $\mathcal{I}^{\eps_k}(S,q)=\mathcal{I}^{s_{i_1}(\alpha)}(S,q)=1$. A questo punto potremmo applicare più volte il punto a) per arrivare a $\mathcal{I}^{\eps_i}(S,q)=\mathcal{I}^\alpha(S,q)=1$ sfruttando il fatto che $s_{i_r}s_{i_{r-1}}\cdots s_1(\eps_k)\not\in\Pi\ \ \forall\ 2\le l,$ per minimalità di $w$. Supponiamo per assurdo che $i_1=k$, quindi che $s_{i_1}(\eps_k)=-\eps_k\in\Delta_-^{re}$. Osserviamo che vale la seguente ovvia modifica di quanto mostrato nel punto a):
            \begin{itemize}
                \item[a')]Sia $\alpha\in\Gamma_-$ e $\mathcal{I}^{-\alpha}(S,O,q)\ne 0$. Se $\alpha\ne-\eps_i$, con $i$ senza loop, allora $s_i(\alpha)\in\Gamma_-$.
            \end{itemize}
            Ma allora, detto $r\ge 2$ il minimo indice tale che $s_{i_r}s_{i_{r-1}}\cdots s_{i_1}(\eps_i)\in\Gamma_+$, si dovrà necessariamente avere che $s_{i_{r-1}}s_{i_{r-2}}\cdots s_{i_1}(\eps_i)=-\eps_{i_r}$ e $s_{i_r}s_{i_{r-1}}\cdots s_{i_1}(\eps_i)=\eps_{i_r}$: il punto a') ci dice infatti che questo è l'unico modo per uscire $\Gamma_-$. Questo contraddice la minimalità di $l$.

%           Infine riguardo al punto d), grazie al punto a) possiamo limitarci al caso 
%           $\alpha\in M$ ricordando che $\Delta_+^{im}=W\cdot M$. A questo
%           punto in luce del lemma.. (2.5 nell'articolo di Kac) e il bound di Lang-
%           Weil si conclude immediatamente (??? serve davvero?).


        \end{proof}