Sia $G$ un gruppo lineare algebrico connesso che agisce su una varietà algebrica affine $X$, dove $G$, $X$ e l'azione sono definiti su $\Fb_q$. Sia \begin{align*}
\Fr:\Fqq&\longrightarrow \Fqq\\x&\longmapsto x^q
\end{align*}
l'automorfismo di Frobenius. Sappiamo che questo agisce (non per forza algebricamente) anche sui punti di $X$ e $G$, tenendo fissi tutti e soli gli $\Fb_q$-punti. Sappiamo inoltre che l'azione del Frobenius sui punti commuta con gli $\Fb_q$-morfismi di $\Fb_q$-varietà. Nello specifico $\Fr(gh)=\Fr(g)\Fr(h)\ \ \forall\ g,h\in G$ e $\Fr(g\cdot x)=\Fr(g)\cdot\Fr(x)\ \ \forall\ x\in X,\ g\in G$, quindi il Frobenius manda $G$-orbite in $G$-orbite. In altre parole l'azione del gruppo di Galois sui punti di $X$ ne induce una sull'insieme delle $G$-orbite. Non si faccia quindi troppa confusione quando in seguito parleremo di \virg{orbite di $G$-orbite}.
\begin{definition}
    Diciamo che $x\in X$ \textit{ha un rappresentante} su $\Fb_q$ se $G\cdot x$ contiene un punto in $X(\Fb_q)$.    
\end{definition}
Prima di proseguire ricordiamo il teorema di Lang.
\begin{theorem}
    Se $G$ è un gruppo algebrico connesso definito su $\Fb_q$ allora la mappa $g\mapsto g^{-1}\Fr(g)$ è surgettiva, o equivalentemente (a meno di scambiare $G$ con $G^{op}$) è surgettiva la mappa $g\mapsto \Fr(g)g^{-1}$.
\end{theorem}
Grazie a questo seguono facilmente una serie di osservazioni:
\begin{proposition}\label{008}
    Un punto $x\in X$ ha un rappresentante su $\Fb_q$ se e solo se $\Fr(G\cdot x)=G\cdot x$.
\end{proposition}
\begin{proof}
    Chiaramente se $x=g\cdot y$ con $y\in X(\Fb_q)$ allora la sua orbita rimane fissa, in quanto $\Fr(y)=y$. Viceversa, supponiamo che $\Fr(x)=g\cdot x$ (cioè che l'orbita di $x$ sia $\Fr$-invariante). Cerchiamo $h\in G$ tale che $h\cdot x\in X(\Fb_q)$, cioè tale che $\Fr(h\cdot x)=h\cdot x$. Ma $\Fr(h\cdot x)=\Fr(h)g\cdot x$, e per Lang possiamo scegliere $h$ tale che $\Fr(h)^{-1}h=g$. Questo conclude.
\end{proof}
\begin{proposition}\label{005}
    Siano $x,y\in X(\Fb_q)$, con $G_x$ connesso. Allora $y\in G\cdot x$ se e solo se $ y\in G(\Fb_q)\cdot x$.
\end{proposition}
\begin{proof}
    Un'implicazione è banale. Supponiamo adesso che $y=g\cdot x$ per qualche $g\in G$. Allora $g\cdot x=y=\Fr(y)=\Fr(g\cdot x)=\Fr(g)\cdot x$, ovvero $\Fr(g)^{-1}g\in G_x$. Ma allora per Lang applicato allo stabilizzatore esiste $h\in G_x$ tale che $\Fr(h)h^{-1}=\Fr(g)^{-1}g$, ovvero tale che $gh\in G(\Fb_q)$, e questo conclude poiché $gh\cdot x=y$.
\end{proof}
\begin{observation}
    Nel caso che interessa a noi, ovvero $G=G^{\alpha}$ e $X=\mf^\alpha(S,O,\Fqq)$ possiamo applicare le proposizioni appena dimostrate: l'ipotesi di $G^\alpha$ connesso è chiaramente verificata, infatti è quoziente di $\GL(\alpha)$, che è irriducibile perché aperto di uno spazio vettoriale. Presa $x\in\mf^\alpha(S,O,\Fqq)$ rappresentazione qualsiasi, anche $G_x$ sarà connesso in quanto isomorfo a un quoziente di $\Aut(x)$, e quest'ultimo è irriducibile perché aperto dello spazio vettoriale $\End(x)$.
\end{observation}

\bigskip
Denotiamo adesso con $\Ac^\alpha(S,O,q)$ il numero di classi di isomorfismo di rappresentazioni su $\Fb_q$, assolutamente indecomponibili, di dimensione $\alpha\in\Gamma_+$. Il nostro prossimo obiettivo è dimostrare che questo numero non dipende dall'orientazione $O$.
\begin{definition}
    Sia $x\in\mf^\alpha(S,O,\Fpp)$. Allora un campo in $\Fb\subseteq\Fpp$ è detto \textit{campo minimo} di $x$ se è minimo tra i campi su cui $x$ ha un rappresentante. 
\end{definition}
Chiaramente se il campo minimo esiste allora è invariante per isomorfismo di $x$. Inoltre possiamo cercarlo in un'estensione finita di $\Fb_p$, in quanto è contenuto nell'estensione finitamente generata dalle entrate delle $(\phi_h)_{h\in\Omega}$ viste come matrici.
\begin{proposition}
    Sia $x\in\mf^\alpha(S,O,\Fpp)$. Allora il suo campo minimo esiste ed è il campo fissato da $H\subset\Gal(\Fpp/\Fb_p)$ definito nel seguente modo:
    $$H=\{\sigma\in\Gal(\Fpp/\Fb_p):\ \sigma(G^\alpha\cdot x)=G^\alpha\cdot x\}.$$
\end{proposition}
\begin{proof}
    È un'immediata conseguenza della proposizione \eqref{008}. 
\end{proof}
\begin{proposition}\label{ob9}
    $\Fb_{p^t}$ è il campo minimo di $x$ se e solo se l'orbita di $(G^\alpha\cdot x)$, tramite l'azione di $\Gal(\Fpp,\Fb_p)$ sulle $G^\alpha$-orbite, ha $t$ elementi. 
\end{proposition}
\begin{proof}
    Supponiamo $\Fb_{p^t}$ sia campo minimo. Grazie alla proposizione \eqref{008} si ha che $\Gal(\Fpp,\Fb_{p^t})$ agisce banalmente su $(G^\alpha\cdot x)$. Inoltre il kernel dell'azione sull'orbita di $(G^\alpha\cdot x)$ consiste negli elementi $\sigma\in\Gal(\Fpp,\Fb_p)$ tali che $\sigma(x)\simeq x$ che, per definizione di campo minimo, sono tutti in $\Gal(\Fpp,\Fb_{p^t})$, e quindi hanno immagine banale in $\Gal(\Fb_{p^t},\Fb_p)$. Di conseguenza l'azione indotta di quest'ultimo sull'orbita di $(G^\alpha\cdot x)$ è fedele. 
    
    L'implicazione opposta adesso è banale in quanto se $x$ avesse campo minimo $\Fb_{p^s}$ con $s\ne t$ allora l'orbita di $(G^\alpha\cdot x)$ avrebbe $s$ elementi.
\end{proof}
Denotiamo con $\Acc^\alpha(S,O,p^t)$ il numero di assolutamente indecomponibili a meno di isomorfismo in $\mf^\alpha(S,O,\Fpp)$ che hanno $\Fb_{p^t}$ come campo minimo. Con $\Icc^\alpha(S,O,p^t)$ indichiamo invece il numero di $\Fb_{p^t}$-indecomponibili a meno di isomorfismo, che hanno $\Fb_{p^t}$ come campo minimo.

Scriviamo $\alpha$ nella forma $c\beta$ con $\beta\in\Gamma_+$ non divisibile e $c\in\Zb_{>0}$. %\begin{proposition} Dati due interi $s,t$ si denoti con $r_s(t)$ il massimo divisore di $t$ con fattori primi in $s$. Allora $$\Acc^{\alpha}(S,O,p^t,p^{ts})=\sum_{\Lcm(d,t)=ts}\Acc^{\alpha}(S,O,p,p^{d})=\sum_{d\mid r_s(t)}\Acc^{\alpha}(S,O,p,p^{ds})=(h_s*\delta_s)(t),$$dove abbiamo posto $h_s(x,y)=\Acc^{\alpha}(S,O,p,p^{\frac{ys}{x}})$ e $$\delta_s()=$$
%\end{proposition}
%\begin{proof}
 %   La prima uguaglianza è una diretta conseguenza di \eqref{ob1}. In quanto alla seconda ci basta mostrare che $\mathcal{S}:=\{l:\ \Lcm(l,t)=st\}$ è uguale a $\{sd:\ d\mid r_s(t)\}$. In effetti $\Lcm(l,t)=lt/(l,t)$ quindi $l\in\mathcal{S}\Leftrightarrow l=(l,t)\cdot s$. Di conseguenza, preso $d\mid t$, si ha che $ds\in\mathcal{S}\Leftrightarrow (ds,t)=d\Leftrightarrow d\mid r_s(t)$.
%\end{proof}
Una semplice osservazione che segue direttamente dalle definizioni è la seguente:
    \begin{align*}
        \Ac^\alpha(S,O,p^t)=\sum_{d\mid t}\Acc^{\alpha}(S,O,p^d),\\ \Ic^\alpha(S,p^t)=\sum_{d\mid t}\Icc^{\alpha}(S,O,p^d).
    \end{align*}
    La prima uguaglianza ci dice che se $\Acc^\alpha$ non dipende dall'orientazione allora neanche $\Ac^\alpha$ può dipendere. Tramite inversione di Moebius si ottiene
    \begin{align}\label{eqa}
        %\Acc^\alpha(S,O,p^t)=\sum_{d\mid t}\Ac^\alpha(S,O,p^d)\mu\left(\frac{t}{d}\right),
        %:=(a_{c}*\mu)(t),\\
        \Icc^\alpha(S,O,p^t)=\sum_{d\mid t}\Ic^\alpha(S,p^d)\mu\left(\frac{t}{d}\right).
        %:=(i_{c}*\mu)(t).
    \end{align}
    %dove abbiamo posto $a_{c}(d):=\Ac^{c\beta}(S,O,p^d)$, $i_c(d):=\Ic^{c\beta}(S,O,p^d)$, e $*$ denota il prodotto di convoluzione.
Questo significa che $\Icc^\alpha$ non dipende dall'orientazione. Ciò che manca da fare adesso è trovare una qualche relazione che esprima $\Acc$ in funzione di $\Icc$. Se ci riusciamo potremo dire che né $\Acc^\alpha$, né $\Ac^\alpha$ dipendono da $O$.

\begin{proposition}
Siano $x,y\in\mf^\alpha(S,O,\Fpp)$ assolutamente indecomponibili con campo minimo $\Fb_{q^s}$, dove $q=p^t$. Allora detto $\Sigma=\{\sigma_1,\dots,\sigma_s\}=\Gal(\Fb_{q^s}/\Fb_q)$ valgono i seguenti fatti:
\begin{itemize}
    \item[a)]$x'=\sigma_1(x)\oplus\sigma_2(x)\oplus\dots\oplus\sigma_s(x)$ ha un rappresentante su $\Fb_q$.
    \item[b)]Tutti i $\sigma_i(x)$ sono a due a due non isomorfi.
    \item[c)]$x'$ è $\Fb_q$-indecomponibile, e ha $\Fb_q$ come campo minimo.
    \item[d)]$x'\simeq y'$ se e solo se esiste $\sigma\in\Sigma$ tale che $y\simeq\sigma(x)$.
\end{itemize}
\end{proposition}
\begin{proof}
    Il punto a) è una facile conseguenza della proposizione \eqref{008}, infatti per costruzione $\sigma(x')\simeq x'\ \ \forall\ \sigma\in\Sigma$ quindi $x$ ha un rappresentante su $\Fb_q$. Il punto b) è una diretta conseguenza della proposizione \eqref{ob9}. Riguardo al punto c), supponiamo per assurdo che $a\oplus b\simeq x'$ con $a$ e $b$ definite su $\Fb_q$. Ma allora per Krull-Schmidt esiste un insieme di indici non vuoto $S\subsetneq\{1,\dots,s\}$ tale che $a\simeq\bigoplus_{i\in S}\sigma_i(x)$ e $b\simeq\bigoplus_{i\in S^c}\sigma_i(x)$. Questo è impossibile perché vorrebbe dire che $\sigma(a)\simeq a\ \ \forall\ \sigma\in\Sigma$, ma l'azione di $\Sigma$ su $\{\sigma_i(x)\}_{i\le s}$ è transitiva quindi $a$ ammetterebbe decomposizioni in indecomponibili distinte da quella scritta sopra. Il campo minimo di $x'$ non può essere più piccolo di $\Fb_q$, infatti per il punto b) applicato al caso $q=p$ si ha che $\sigma(x)\not\simeq\sigma'(x)\ \ \forall\ \sigma\ne\sigma'\in\Gal(\Fb_{q^s}/\Fb_p)$ e questo per Krull-Schmidt significa che tutti gli elementi di $\Gal(\Fb_{q^s}/\Fb_p)\setminus\Sigma$ mandano $x$ in una rappresentazione non isomorfa. Infine anche il punto d) è un'immediata conseguenza del teorema di Krull-Schmidt.
\end{proof}
\begin{proposition}
    Sia $x\in\mf^\alpha(S,O,\Fpp)$ una rappresentazione con campo minimo $\Fb_q$ e $\Fb_q$-indecomponibile. Sia $x=x_1\oplus x_2\oplus\dots\oplus x_s$ lo spezzamento in indecomponibili di $x$ su $\Fpp$. Allora gli $x_i$ formano un'unica orbita dell'azione di $\Gal(\Fpp,\Fb_q)$, e il campo minimo di ciascuno di essi è $\Fb_{q^s}$.
\end{proposition}
\begin{proof}
    Per Krull-Schmidt, poiché $G^\alpha\cdot x$ è invariante tramite l'azione di $\Gal(\Fpp,\Fb_q)$ (\eqref{008}), possiamo dire che l'insieme $\{x_1,\dots,x_s\}$ viene partizionato da tale azione in un certo numero di orbite, che non fuoriescono da esso. Ma non ci può essere più di un'orbita in quanto per la proposizione \eqref{005} ciascuna di esse dà luogo a un fattore della decomposizione di $x$ definito su $\Fb_q$, il che contraddice l' $\Fb_q$-indecomponibilità. Ora $\Fb_{q^s}$ è campo minimo di ciascuno degli $x_i$ per la proposizione \eqref{ob9}.
\end{proof}
Queste due proposizioni messe insieme ci dicono che per ogni classe di isomorfismo $G^\alpha(\Fb_q)\cdot x$ di rappresentazioni $\Fb_q$-indecomponibili in $\mf^\alpha(S,O,\Fb_q)$ con campo minimo $\Fb_q$, esiste un unico $d\in\Zb_{>0}$ divisore di $\alpha$ e un'unica orbita di $\Gal(\Fpp,\Fb_{q^d})$ fatta di $d$ rappresentazioni (o meglio, composta da $d$ $G^{\frac{\alpha}{d}}$-orbite di rappresentazioni) in $\mf^{\frac{\alpha}{d}}(S,O,\Fb_{q^d})$ associata ad esso, data dalla decomposizione in $\Fpp$-indecomponibili di $x$. Inoltre questa corrispondenza è una bigezione. Di conseguenza si ha che\begin{equation}\label{eq1}\Icc^{c\beta}(S,p^t)=\sum_{d\mid c}\frac{1}{d}\Acc^{\frac{c\beta}{d}}(S,O,p^{dt}),
%\stackrel{\eqref{eqa}}{=}\sum_{d\mid c}\frac{1}{d}\sum_{r\mid dt}\Ac^{\frac{c\beta}{d}}(S,O,p^r)\mu\left(\frac{dt}{r}\right),
\end{equation}
%\stackrel{}{=}\frac{1}{d}(_{}*\mu)(\frac{c}{d})$ (con $d\mid c$).
Adesso con una semplice induzione sull'altezza di $\alpha$ si può mostrare che $\Acc^\alpha(S,O,p^t)$ è una funzione di $\Icc$: supponiamo che tutti gli elementi di altezza minore di un certo $\gamma=k\gamma'$ (con $\gamma'$ non divisibile) siano funzioni di $\Icc$. Se poniamo $c=k,\ \beta=\gamma'$ in \eqref{eq1} e riarrangiamo i termini si ottiene 
\begin{equation*}
    \Acc^{k\gamma'}(S,O,p^t)=\hspace{0.1cm}\Icc^{k\gamma'}(S,p^t)-\sum_{\substack{d\mid k\\d\ne k}}\frac{1}{d}\Acc^{\frac{k\gamma'}{d}}(S,O,p^{dt})
\end{equation*}
che per ipotesi induttiva è una funzione di $\Icc$. 

\begin{proposition}\label{011}
    $\Ac^\alpha(S,O,p^t)$ e $\Acc^\alpha(S,O,p^t)$ non dipendono dall'orientazione $O$. Di conseguenza (nello stesso stile del punto a) del teorema \eqref{010}) $$\Ac^\alpha(S,q)=\Ac^{s_i(\alpha)}(S,q)\ \ \forall\ i\in I\ \text{ senza loop}.$$
\end{proposition}
\begin{proof}
    È già stato tutto dimostrato. L'unica considerazione aggiuntiva da fare, per poter attuare un ragionamento uguale a quello del punto a) di \eqref{010}, è che i funtori di riflessione mandano assolutamente indecomponibili in assolutamente indecomponibili. Ma questa è una banale osservazione, in quanto mandano somme dirette in somme dirette.
\end{proof}

\medskip
\begin{theorem}\label{013}
    Sia $(S,O)$ un quiver, $\alpha\in\Gamma_+$ e sia $\Fb=\Fpp$.
    \begin{itemize}
        \item[a)] $\Ic^\alpha(S,O,\Fpp)$ non dipende da $O$. Se è non nullo e $\alpha\ne\eps_i$ con $i$ senza loop, allora $s_i(\alpha)\in\Gamma_+$ e $\Ic^\alpha(S,\Fpp)=\Ic^{s_i(\alpha)}(S,\Fpp).$
        \item[b)] Per $\alpha\in\Delta_+^{im}$ si ha che $\Ic^\alpha(S,\Fpp)=\infty$.
        \item[c)] Per $\alpha\in\Delta_+^{re}$ si ha che $\Ic^\alpha(S,\Fpp)=1$, e la rappresentazione indecomponibile ha un rappresentante su $\Fb_p$.
    \end{itemize}
\end{theorem}
\begin{proof}
    Poiché ogni indecomponibile su $\Fpp$ ha un campo minimo, su cui è assolutamente indecomponibile, si ha $\Ic^\alpha(S,O,\Fpp)=\sum_{t\ge1}\Acc^\alpha(S,p^t)$, quindi non dipende dall'orientazione. La seconda parte del punto a) si fa nello stesso stile del punto a) del teorema \eqref{010}. Anche i rispettivi punti c) sono analoghi; per mostrare che ha rappresentanti su $\Fb_p$ basta osservare che, preso $w=s_{i_l}s_{i_{l-1}}\cdots s_{i_1}\in W$ di lunghezza minima tale che $w(\eps_k)=\alpha$ per qualche $\eps_k\in\Pi$, si ha $\Ac^{\eps_k}(S,p)=\Ac^{\alpha}(S,p)=1$ (si proceda come in \eqref{010}). 
    
    Riguardo al punto b), se $\alpha\in\Delta_+^{im}$ sia $w\in W$ nel gruppo di Weyl tale che $w(\alpha)\in M$. Ma $\Ic^{w(\alpha)}(S,O,\Fpp)=\Ic^\alpha(S,O,\Fpp)$ è infinito per il corollario \eqref{cor}, quindi per il punto a) $\Ic^{w(\alpha)}(S,\Fpp)=I^\alpha(S,\Fpp)$.
\end{proof}
\begin{observation}
    Grazie a questo teorema possiamo adesso affermare che $W(M)\subset\Gamma_+$, quindi nella definizione di $\Delta_+^{im}$ era superfluo intersecare con $\Gamma_+$.
\end{observation}