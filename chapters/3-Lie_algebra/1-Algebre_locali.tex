In questa sezione andremo a richiamare alcune proprietà delle algebre di Lie graduate, partendo da un punto di vista un po' più generale.

\begin{definition}
    Sia $V$ uno spazio vettoriale su campo $K$ e $A\subset V\times V$ un sottospazio invariante per la riflessione $(v,w)\mapsto (w,v)$, e chiuso per combinazioni bilineari, cioè tale che dati $(v,w),(v',w)\in A$ si abbia $(kv+hv',w)\in A\ \ \forall\ k,h\in K$. Data un'operazione bilineare parzialmente definita $[\ ,\ ]:A\rightarrow V$, chiamiamo \textit{prespazio} la terna $(V,A,[\ ,\ ])$ (o semplicemente $V$ se l'operazione sarà chiara dal contesto).
\end{definition}
 Dato un prespazio $(V,A,[\ ,\ ])$ e dati $X_1,\dots,X_n\subset V$ sottoinsiemi qualsiasi di elementi, utilizzeremo spesso le seguenti notazioni:
\begin{itemize}
    \item $[X_1X_2]$, o equivalentemente $(\Ad X_1) (X_2)$, indicherà lo span (eventualmente banale) degli $[xy]$ al variare di $(x,y)\in A\cap (X_1\times X_2)$.
    \item $[X_1X_2\dots X_n]:=[\dots[[X_1X_2]X_3]\dots X_n]$.
    \item $(X_1)^k:=[\underbrace{X_1X_1\dots X_1}_{k\ \text{volte }X}]$ 
\end{itemize}
\begin{definition}
    Un omomorfismo di prespazi $f:(V,A,[\ ,\ ])\rightarrow (W,B,[\ ,\ ])$ è un'applicazione lineare tale che $(f\times f)(A)\subset B$ e $f([vw])=[f(v)f(w)]$ per ogni $(v,w)\in A$.
\end{definition}
\begin{definition}
    Dato un prespazio $(V,A,[\ ,\ ])$, si dice \textit{sottoprespazio} un sottospazio vettoriale $W$ tale che $[WW]\subset W$.
\end{definition}
\begin{definition}
    Dato un prespazio $(V,A,[\ ,\ ])$, un sottospazio vettoriale $I$ è detto \textit{ideale} se $[IV],[VI]\subset I$. 
\end{definition}
\begin{proposition}
    Dato un prespazio $(V,A,[\ ,\ ])$ e un suo ideale $I$, lo spazio vettoriale $V/I$ ha una naturale struttura di prespazio indotta da $[\ ,\ ]$, con il dominio della nuova operazione sarà l'immagine di $A$ tramite $\pi\times\pi:V\times V\rightarrow V/I\times V/I$.

    Inoltre $\pi$ è un omomorfismo di prespazi.
\end{proposition}
\begin{proof}
    Dato $(v,w)\in A$ e $\overline{v}=\pi(v),\ \overline{w}=\pi(w)$, si ponga $[\overline{v}\overline{w}]=\overline{[vw]}$. La buona definizione, la bilinearità e il fatto che $\pi$ sia omomorfismo di prespazi sono una semplice verifica.
\end{proof}
\begin{proposition}
    I kernel di omomorfismi di prespazi sono ideali, e valgono gli usuali teoremi di omomorfismo (a patto di chiamare \virg{suriettivo} un omomorfismo $f$ che sia suriettivo nel senso standard, e tale che $f\times f$ sia suriettiva come mappa tra gli insiemi di definizione delle rispettive operazioni).
\end{proposition}
\begin{proof}
    È un semplice esercizio.
\end{proof}
\begin{observation}
    Le definizioni appena date si applicano anche ai prespazi con identità, ovvero tali che esista un elemento $1_V\in V$ con $\{1\}\times V\cup V\times\{1\}\subset A$ e con $[1_Vv]=[v1_V]=v\ \ \forall\ v\in V$. Le uniche richieste aggiuntive che facciamo sono che gli omomorfismi e le sottoalgebre preservino l'identità.
\end{observation}
\begin{definition}
    Un prespazio $\textit{graduato}$ è un prespazio $V$ dotato di una decomposizione in spazi vettoriali $$V=\bigoplus_{i\in\Zb} V_i$$ tale che valgano le seguenti proprietà:
    \begin{itemize}       
        \item[(P1)] $\dim V_i<\infty$.
        \item[(P2)] $[V_iV_j]\subset V_{i+j}$.
        \item[(P3)] $V_{-1}\oplus V_0\oplus V_1$ genera tutto $V$ come prespazio.
    \end{itemize}
    Se non si richiede (P1) si parla più genericamente di prespazio graduato infinito.
    $\Phi:V\rightarrow W$ è detto un omomorfismo di prespazi graduati (infiniti) se è un omomorfismo di prespazi tale che $\Phi(V_i)\subset W_{\phi(i)}$ dove $\phi=\pm\Id$.
\end{definition}
\begin{definition}
    Dato un prespazio graduato $V=\bigoplus V_i$, un elemento $x$ è detto omogeneo se è contenuto in uno dei $V_i$. Un sottospazio vettoriale $W$ è detto omogeneo se $W=\bigoplus W\cap V_i$.
\end{definition}
\begin{observation}
    I sottospazi vettoriali omogenei sono tutti e soli quelli generati da elementi omogenei.
    
    Gli ideali omogenei sono tutti e soli gli ideali generati (come ideali) da elementi omogenei.
\end{observation}
\begin{observation}
    Quozientare per un ideale omogeneo mantiene in modo naturale la struttura graduata. Inoltre il kernel di un omomorfismo tra prespazi graduati è un ideale omogeneo.
\end{observation}
\begin{definition}
    Sia $k>0$ un intero e $(V,A,[\ ,\ ])$ un prespazio graduato con gradazione concentrata tra $-k$ e $k$, cioè tale che $V=\bigoplus_{i=-k}^kV_i$. Se $A$ è lo spazio chiuso per combinazioni bilineari generato da $$\bigcup_{|i+j|\le k}V_i\times V_j,$$ allora $V$ è detto \textit{prespazio $k$-locale}.
\end{definition}
\begin{definition}
    Un'algebra associativa (risp. algebra di Lie) è un prespazio con operazione $[\ ,\ ]$ globalmente definita e tale che sia associativa con identità (risp. antisimmetrica e che soddisfi l'identità di Jacobi $[[xy]z]+[[zx]y]+[[yz]x]=0\ \ \forall\ x,y,z\in G$).
\end{definition}
\begin{definition}
    Sia $k>0$ un intero e $(V,A,[\ ,\ ])$ un prespazio $h$-locale. Se $[\ ,\ ]$ è antisimmetrica e soddisfa l'identità di Jacobi ogni qualvolta tutti e $6$ i bracket in essa siano definiti, allora $V$ è detto algebra di Lie \textit{$k$-locale}.
\end{definition}
Per le algebre (di Lie o associative) graduate e per quelle $k$-locali, possiamo definire gli omomorfismi, gli ideali e i sottospazi omogenei nel senso dei prespazi graduati. I quozienti sono un po' più delicati nel caso delle algebre $k$-locali, in quanto non è necessariamente vero che l'associatività o Jacobi continuino a valere.
%i quozienti, 

%\begin{observation}
%    Non è del tutto scontato a priori che quozienti di omomorfismi di algebre (associativa o di Lie) $k$-locali preservino la struttura di algebra (associativa o di Lie) $k$-locale. Tuttavia in un caso l'associatività e nell'altro Jacobi, continuano ad essere rispettati nel quoziente.
%\end{observation}
%\begin{proof}
%    Sia $(G,A,\cdot)$ un'algebra associativa $k$-locale con identità, e $I$ un suo ideale omogeneo. Ci chiediamo se in $G/I$ continui a valere l'associatività. Ci basta verificarla per terne di elementi omogenei, per questioni di linearità e per come è fatto l'insieme di definizione $A$. Si considerino tre elementi $(a+I),$ $(b+I)$ e $(c+I)$ non nulli nel quoziente, per cui siano definiti $((a+I)(b+I))(c+I)$ e $(a+I)((b+I)(c+I))$. Questo significa innanzitutto che esistono $a'\in a+I$; $b'\in b+I$ per cui è definito il prodotto $a'b'$ in $G$, il che mi garantisce che $|\deg a+\deg b|\le k$, cioè che il prodotto tra $a$ e $b$ è definito (stiamo usando in maniera sostanziale che $a$ e $b$ sono omogenei). Con un ragionamento analogo si ottiene che devono essere definiti i prodotti tra $ab$ e $c$, tra $b$ e $c$, tra $a$ e $bc$. Ma allora per associatività in $G$ vale che $(ab)c=a(bc)$, quindi finalmente possiamo affermare che $((a+I)(b+I))(c+I)=(a+I)((b+I)(c+I))$.
%    
%    Sia ora $(G,A,[\ ,\ ])$ un'algebra di Lie $k$-locale, e $I$ un suo ideale omogeneo. Ci chiediamo se in $G/I$ continui a valere Jacobi. Come prima, si considerino tre elementi $(a+I),$ $(b+I)$ e $(c+I)$ non nulli nel quoziente, per cui siano definiti $[(a+I)(b+I)](c+I)$, $[(c+I)(a+I)](b+I)$ e $[(b+I)(c+I)](a+I)$. Con un ragionamento identico a prima si ottiene che 
%\end{proof}
Per comodità di notazione talvolta parleremo di algebra (di Lie) $\infty$-locale per indicare semplicemente un'algebra (di Lie) graduata.
\begin{observation}
Fissato $k\in\Zb_{>0}\cup\{\infty\}$ e fissato un intero $0<h<k,$ a ogni algebra (di Lie) $k$-locale $G=\bigoplus_{i=-k}^k G_i$ si può associare l'algebra (di Lie) $h$-locale $^h\widehat{G}=\bigoplus_{i=-h}^h G_i$, con operazione indotta, detta \textit{parte $h$-locale di $G$}.
\end{observation}
\begin{observation}
    Sia $(G,\cdot)$ un'algebra associativa. Possiamo darle una struttura di algebra di Lie $k$-locale ponendo semplicemente $[xy]=xy-yx$. Quando ci riferiremo a $G$ con questa struttura, la indicheremo con $G_L$.
\end{observation}
\begin{definition}
    Fissato $k\in\{1,\infty\}$, sia $G$ un'algebra di Lie $k$-locale. Diremo che un'algebra graduata $U$ dotata di un'inclusione (tra prespazi graduati) $i:G\rightarrow U_L$ è l'\textit{algebra inviluppante universale} di $G$ se data una qualsiasi algebra $V$ e un omomorfismo $f:G\rightarrow V_L$, esiste ed è unica $f:U\rightarrow V$ tale che $g\circ i=f$.
\end{definition}
\begin{proposition}
    L'inviluppante universale esiste ed è unico a meno di isomorfismo.
\end{proposition}
\begin{proof}
    L'unicità segue dalla proprietà universale, quindi concentriamoci sull'esistenza.
    Detta $S$ l'algebra tensoriale $$\bigoplus_{i\ge0} \underbrace{G\otimes G\otimes \dots\otimes G}_{i\text{ volte}},$$
%    All'interno dell'algebra tensoriale $$\bigoplus_{i\ge0} \underbrace{G\otimes G\otimes \dots\otimes G}_{i\text{ volte}}$$ consideriamo il sottospazio vettoriale $S$ generato da elementi della forma 
%    \begin{equation}\label{ddd}
%    x_1\otimes x_2\otimes\cdots\otimes x_l
%    \end{equation}
%    dove gli $x_i$ sono omogenei e la somma dei loro gradi è $\le h$. È evidente che $S$, %dotato della gradazione indotta da $G$ è un'algebra $h$-locale.
%    La prossima affermazione è meno immediata, ma affrontabile con una semplice induzione:
%    \begin{observation}\label{eee}
%        $S$ è l'algebra $h$-locale generata dallo spazio vettoriale graduato $G$.
%    \end{observation}
%    \begin{proof}
%        L'unica cosa che occorre mostrare è che un prodotto arbitrario $a_1a_2\cdots a_n$ di %elementi omogenei contenuti nell'algebra $h$-locale generata da $G$ (chiamiamola %$\overline{G}$), con grado complessivo $\le h$, è anch'esso in $\overline{G}$. Procediamo per %induzione sulla lunghezza $n$ di questo prodotto. Per $n\le 2$ siamo a posto. Per $n>2$ %supponiamo (wlog) che il grado di $a_1a_2\cdots a_{n}$ sia $\ge 0$. Allora $a_n$ ha grado $< %0$ altrimenti $a_1\cdots a_{n-1}\in\overline{G}$ per ipotesi induttiva, e si conclude perché %ci siamo ricondotti a un prodotto a due termini. Sia $m$ minimo tale che $\deg a_j<0\ \ %\forall\ j> m$. Ovviamente $m\ge 1$. Ma allora $a_ma_{m+1}$ sta in $\overline{G}$; si %conclude per ipotesi induttiva.
%    \end{proof}
    sia $I$ l'ideale di $S$ generato da elementi della forma 
    \begin{equation}\label{ccc}
        [xy]-x\otimes y+y\otimes x
    \end{equation}
    per $x,y$ tali che il bracket abbia senso. 
    %(si noti che in questo caso l'elemento in \eqref{ccc} sta effettivamente in $S$). 
    È una facile verifica notare che $I$ è generato da elementi omogenei.
    \begin{observation}\label{fff}
        Fissata una base omogenea di $G$ data da $(x_\alpha)_{\alpha\in A}$, $I$ è generato come spazio vettoriale da elementi della forma 
        \begin{equation*}
        -x_{\alpha_1}\otimes\dots \otimes x_{\alpha_n}+x_{\alpha_1}\otimes\dots \otimes x_{\alpha_{j+1}}\otimes x_{\alpha_j}\otimes\dots\otimes x_{\alpha_n}+x_{\alpha_1}\otimes\dots \otimes [x_{\alpha_j}x_{\alpha_{j+1}}]\otimes\dots\otimes x_{\alpha_n},
        \end{equation*}
        dove $\alpha_i\in A\ \ \forall\ i$ e il bracket $[x_{\alpha_j}x_{\alpha_{j+1}}]$ è definito in $G$.
        %e dove il grado complessivo è $\le h$.
    \end{observation}
    %\begin{proof}
    %    L'unica cosa non banale da verificare è che questi elementi siano effettivamente elementi di $I$. Per fare ciò basta fare un ragionamento nello stesso identico stile di \eqref{eee}.
    %\end{proof}

    Sia $U=S/I$ (che per quanto osservato in precedenza
    %resta un'algebra $h$-locale, 
    preserva la gradazione, essendo $I$ omogeneo). Affermiamo che $U$, dotato della naturale mappa $i: G\rightarrow U_L$, è l'inviluppante universale di $G$. In effetti osserviamo innanzitutto che $i$ è omomorfismo (di prespazi graduati), infatti $$i([xy])\equiv x\otimes y - y\otimes x\pmod{I}$$Data poi un'algebra 
    %$h$-locale 
    $V$ dotata di un omomorfismo $j:G\rightarrow V_L$, possiamo estendere $j$ a un omomorfismo (di algebre associative
    %$h$-locali
    ) $e:S\rightarrow V$ mandando $x_1\otimes\dots\otimes x_l$ in $j(x_1)\cdots j(x_l)$. Ma questo omomorfismo fattorizza tramite $U$, infatti preso un generatore di $I$ della forma in \eqref{ccc}, si ha che $$e([xy]-x\otimes y+y\otimes x)=[e(x)e(y)]+e(x)\otimes e(y)+e(y)\otimes e(x)=0$$infine l'unicità dell'estensione segue dal fatto che $G$ genera $U$ come algebra associativa.
    %$h$-locale, che è una diretta conseguenza dell'osservazione \eqref{eee}. 
    Questo conclude.
\end{proof}