Per questa sezione fissiamo $k$ un campo qualsiasi e $K$ la sua chiusura algebrica. 

\begin{definition}
    Sia $X$ un $K$-schema affine, cioè isomorfo a $\Spec(A)$ con $A$ una $K$-algebra. Una $k$-\textit{forma} di $X$ è un $k$-schema affine $\Spec(A_0)$, con $A_0$ una $k$-algebra, tale che $$K\otimes_k\Spec(A_0)\simeq \Spec(A).$$ Nel seguito diremo, un po' impropriamente, che un $K$-schema è \textit{definito su} $k$ per intendere che è munito di una $k$-forma, che spesso sarà evidente nei casi che studieremo.
    
    Sia $B$ un altro $K$-schema definito su $k$, con $k$-forma $\Spec(B_0)$. Diremo che un morfismo $$F:\Spec(A)\rightarrow\Spec(B)$$ è \textit{definito su} $k$ se induce un morfismo $$f:\Spec(A_0)\rightarrow\Spec(B_0)\ \ \text{tale che}\ \ F=K\otimes_k f,$$o equivalentemente se $F^*(A_0)\subset B_0$.
    
    Un $K$-schema di gruppo $G$ è \textit{definito su} $k$ se è munito di una $k$-forma tale che il prodotto, l'inverso e l'identità siano tutte mappe definite su $k$.
\end{definition}

\medskip
\begin{definition}
    Un toro $n$-dimensionale è un $K$-schema di gruppo $T$ isomorfo allo schema di gruppo $$\Spec(\underbrace{K[t_1,t_1^{-1},\dots,t_n,t_n^{-1}}_{A}])$$con inverso, prodotto e identità descritti, a livello di algebra di Hopf, da
    \begin{align*}
        A&\rightarrow A\ &\ \ A&\rightarrow A\otimes A\ &\ \ A&\rightarrow K\\t_i&\mapsto t_i^{-1}\ &\ \ t_i&\mapsto t_i\otimes t_i\ &\ \ t_i&\mapsto 1
    \end{align*}
\end{definition}
\begin{definition}
    Sia $T$ un toro $n$-dimensionale definito su $k$, con $k$-forma isomorfa a un $k$-schema di gruppo $\Spec(A_0)$. Se vale che $\Spec(A_0)\simeq \Spec(k[t_1,t_1^{-1},\dots,t_n,t_n^{-1}])$ come schemi di gruppo, diremo che $T$ è $k$-split.
\end{definition}

\begin{lemma}
    Sia $T$ un toro $n$-dimensionale $k$-split. Allora $\Aut(T)\simeq\GL(n,\Zb)$, e gli automorfismi sono tutti definiti su $k$.
\end{lemma}
\begin{proof}
    Cercare gli automorfismi di $T$ equivale a cercare tutti gli isomorfismi di $K[T]$ come $K$-algebra di Hopf, dove ricordiamo che le operazioni di inverso, prodotto e identità sono rispettivamente\begin{align*}
        K[T]&\rightarrow K[T]\ &\ \ K[T]&\rightarrow K[T]\otimes K[T]\ &\ \ K[T]&\rightarrow K\\t_i&\mapsto t_i^{-1}\ &\ \ t_i&\mapsto t_i\otimes t_i\ &\ \ t_i&\mapsto 1
    \end{align*}
    Sia $f\in\Aut(T)$, e $f^*$ il relativo isomorfismo di algebre di Hopf. Se postcomponiamo $f$ con $p_i$, proiezione sull'$i$-esimo fattore, otteniamo a livello di anelli di coordinate una mappa $f^*\circ p_i^*:\ R\rightarrow K[T]$ ($R=K[t_1,t_1^{-1}]$) che è omomorfismo di algebre di Hopf. Detto $r_i=f^*\circ p_i^*(t_1)$ affermiamo che deve essere necessariamente un monomio monico $t^{v_i}$ con $v_{i}$ multi-indice a valori in $\Zb$. In effetti invertibili devono andare in invertibili quindi $r_i(t)$ è effettivamente un monomio. Inoltre dovendo commutare il diagramma 
    % https://q.uiver.app/?q=WzAsNCxbMCwwLCJSIl0sWzIsMCwia1tUXSJdLFswLDEsIlJcXG90aW1lcyBSIl0sWzIsMSwia1tUXVxcb3RpbWVzIGtbVF0iXSxbMCwxXSxbMCwyXSxbMSwzXSxbMiwzXV0=
    \[\begin{tikzcd}
	R && {K[T]} \\
	{R\otimes R} && {K[T]\otimes K[T]}
	\arrow[from=1-1, to=1-3]
	\arrow[from=1-1, to=2-1]
	\arrow[from=1-3, to=2-3]
	\arrow[from=2-1, to=2-3]
    \end{tikzcd}\]
    segue immediatamente che $r_i(t)$ deve essere anche monico. Ora, per la suriettività di $f$, gli $r_i(t)$ e i loro inversi devono generare tutti i monomi, ovvero i vettori di esponenti $v_i$ devono generare $\Zb^n$ come $\Zb$ modulo. In altre parole la matrice $A_{ij}=(v_i)_j$ deve essere invertibile in $\Zb$, cioè stare in $\GL(n,\Zb)$. Viceversa si verifica facilmente che una tale richiesta è sufficiente per avere un isomorfismo di algebre di Hopf, definito su $k$.
\end{proof}

\begin{lemma}\label{003}
    Sia $T$ un toro $k$-split, e $S$ un sottotoro definito su $k$ (con $k$-forma indotta) di dimensioni $l$ e $s$ rispettivamente. Allora $S$ è $k$-split e $T/S$ è un toro $k$-split di dimensione $l-s$.
\end{lemma}
\begin{proof}
    Per prima cosa sappiamo che l'omomorfismo di algebre di Hopf associato 
    all'inclusione $i^*:K[T]\rightarrow K[S]$ è surgettivo. Inoltre, per quanto già osservato $i^*(t_j)=r_j=t^{v_j}$ monomio monico 
    ($v_j\in\Zb^s$), e per suriettività la mappa indotta a livello di esponenti $a:\Zb^l\rightarrow \Zb^s$ ammette una retrazione $b$ che include $\Zb^s$ in $\Zb^l$. A meno di comporre con un automorfismo di $\Zb^l$ (cioè a livello di gruppi algebrici con un automorfismo di $T$), possiamo supporre che tale retrazione mandi $e_i$ in $c_ie_i$ per ogni $i\le s$, con $c_i\in\Zb_{>0}$ e $e_i$ vettore della base canonica. Se per assurdo una delle costanti, facciamo $c_1$, fosse maggiore di $1$, allora $i^*(t_1^{c_1})=t_1$ che è chiaramente assurdo in quanto $t_1$ non è una radice $c_1$-esima dell'unità nell'anello dei polinomi, quindi non lo è neanche in $K[S]$ per il lemma di Gauss. Quindi abbiamo $$i^*:t_j\mapsto t_j\ \ \forall\ j\le s$$ e perciò, sempre a meno di comporre con un automorfismo di $\Zb^l$, possiamo supporre anche che $$i^*:t_j\mapsto 0\ \ \forall\ j>s.$$
    Ci siamo quindi ricondotti tramite automorfismi di $T$ (definiti su $k$) all'inclusione standard (quella sulle prime coordinate) del toro $s$-dimensionale nel toro $l$-dimensionale. In questo caso il sottotoro e il quoziente sono ovviamente $k$-split.
\end{proof}

\begin{lemma}\label{001}
    Siano $S$ e $T$ due tori $k$-split di dimensioni $s$ e $l$, e $N$ un qualsiasi gruppo algebrico lineare definito su $k$ tale che esista una successione esatta definita su $k$ $$1\rightarrow S\rightarrow N\rightarrow T\rightarrow 1$$(i.e. $S$ è isomorfo come gruppo algebrico alla sua immagine in $N$, e $T$ al quoziente di $N$ per l'immagine di $S$). Allora valgono le seguenti:
    \begin{itemize}
    \item[a)] $N$ è isomorfo a $S\times T$, cioè è un toro $s+l$-dimensionale.
    \item[b)]Se $k$ è un campo perfetto il toro $N$ (definito su $k$) è $k$-split.
    \end{itemize}
\end{lemma}
\begin{proof}
    La soluzione del punto a) è strutturata in $3$ step:
    \begin{enumerate}[label=\textbf{Step} \arabic*:]
    \item
    Mostriamo innanzitutto che $N$ centralizza $S$. Notiamo subito che $N$ è connesso. Sia $f:N\times S\rightarrow S$ l'azione per coniugio di $N$ su $S$, con omomorfismo di anelli di coordinate associato \begin{align*}
        f^*:K[S]&\rightarrow K[N]\otimes K[S]\\
        t_i&\mapsto \sum_{v\in\Zb^s} f_{i,v}\otimes t^v
    \end{align*} (le $f_{i,v}$ sono tutte nulle a meno di un numero finito).
    
    Restringendo a una mappa da $\{x\}\times S$ in $S$ (che corrisponde a valutare in $x\in N$ una funzione di $K[N]\otimes K[S]$) si ottiene un automorfismo $f^*_x$ di $S$, che a livello di anelli di coordinate deve perciò mandare $t_i\mapsto t^{v_{x,i}}$. Questo significa che tutte le $f_{i,v}$ tranne $f_{i,v_{x,i}}$ (che varrà $1$) si devono annullare in $x$. Analogamente sull'aperto dove non si annulla $f_{i,v_{x,i}}$ devono annullarsi tutte le altre $f_{i,v}$, e $f_{i,v_{x_i}}$ deve valere $1$. Quindi tale aperto è anche un chiuso non vuoto, ovvero tutto $N$ per connessione. Ma esso corrisponde per definizione a tutti gli $y\in N$ con $v_{y,i}=v_{x,i}$, quindi in particolare ponendo $x=1$ si ha $v_{y,i}=v_{1,i}=(e_i)\ \ \forall\ y\in N,\ i\le s$, dove $e_i$ è il vettore $i$-esimo della base canonica. Dunque l'azione per coniugio di $N$ su $S$ è banale.

\item
    Nello stesso stile si mostra che $N$ è commutativo: consideriamo l'azione di $N$ su se stesso per coniugio. Poiché $S$ agisce banalmente questa si traduce in un'azione di $N/S\simeq T$ su $N$, e quest'ultima è banale per un discorso identico a quello fatto poco fa.
\item

    Ora, la rappresentazione standard di $N$ su $K[N]$ ne induce una di $S$ che si scompone in sottorappresentazioni definite su $k$ $$\bigoplus_{m\in\Zb^s}R_m,\ \ \ \text{dove}\ \ x\cdot r=x^mr\ \ \forall\ x\in S,r\in R_m,$$ in quanto $S$ linearmente riduttivo e $k$-split. Dal fatto che $K[N]$ si surietti su $K[S]$ e dal lemma di Schur segue che nessuno degli $R_m$ è banale. Notiamo che $R_m$ è anche $N$-invariante (con l'azione di $N$ definita su $k$), infatti presi $x\in S,\ y\in N,\ r\in R_m$ si ha per commutatività che $x\cdot (y\cdot r)=y\cdot (x\cdot r)=x^m(y\cdot r)$ da cui $y\cdot r\in R_m$. Da questo segue che in $R_m$ esiste un $r_m$ autovettore comune a tutti gli $y\in N$: per dirlo si consideri il sottospazio invariante $V$ di dimensione minima. Se su esso tutti gli $y\in N$ agiscono come multipli dell'identità abbiamo concluso, altrimenti si consideri un autospazio $W\subsetneq V$ di un qualche $y$. Esso è $N$-invariante per commutatività di $N$, contro l'ipotesi di minimalità di $V$.

\medskip
    $R_0$ è lo spazio dove $S$ agisce banalmente, quindi contiene la copia di $K[T]\subset K[N]$ data dall'omomorfismo di anelli di coordinate associato al morfismo $S$-invariante $N\rightarrow T$. Siano quindi $f_j\in K[T],\ j\le l$ definiti su $k$ tali che $x\cdot f_j=x_jf_j$ per ogni $x=(x_1,\dots,x_l)\in T,\ j\le l$. Consideriamo infine la sottorappresentazione, definita su $k$, data da $$V=\Span(r_{e_1},\dots,r_{e_s},f_1,\dots,f_l).$$ 
    
    Questa è chiaramente fedele. Poiché le fibre hanno dimensione $0$, l'immagine in $\GL(V)$ ha stessa dimensione del toro massimale ma deve essere anche chiusa e contenuta in esso, quindi è proprio $(K^*)^n$. Un morfismo bigettivo con codominio un toro è un isomorfismo, in quanto il toro è una varietà normale (ad esempio perché il suo anello di coordinate è a fattorizzazione unica). Altrimenti, vale più in generale che morfismi di gruppi bigettivi sono anche isomorfismi.
    \end{enumerate}
    Veniamo adesso al punto b). È un fatto di semplice verifica che il toro $N$ definito su $k$ campo perfetto è $k$-split se e solo se $\Gal(K/k)$ agisce banalmente sui suoi caratteri (visti come elementi dell'anello di coordinate). Infatti, detta $\Spec(A_0)$ la $k$-forma di $N$, e detto $f^*$ l'isomorfismo da $K[t_1,t_1^{-1},\dots,t_{s+l}]\stackrel{f^*}{\longrightarrow} K\otimes_kA_0$, abbiamo che $\Gal(K/k)$ agisce banalmente sui $t_i$ se e solo se $f^*(t_i)\in A_0\ \ \forall\ i$, che implica che $A_0\simeq k[t_1,t_1^{-1},\dots,t_{s+l},t_{s+l}^{-1}]$. 

    In realtà per far vedere che $N$ è $k$-split ci basta mostrare che un sottoreticolo di dimensione $s+l$ (i.e. di indice finito) del reticolo dei caratteri sta nell'anello di coordinate della $k$-forma di $N$ (cioè è fissato da $\Gal(K/k)$). Infatti in tal caso per ogni carattere $x$ esisterebbe $l$ intero tale che $x^l$ è un punto fisso di $\Gal(K/k)$. Ma preso $\sigma\in\Gal(K/k)$ si ha che $\sigma(x)^l=\sigma(x^l)=x^l\Leftrightarrow \sigma(x)=x$ per via della struttura di reticolo, quindi anche $x$ è punto fisso.

    \medskip
    Il toro $S$ è $k$-split, quindi $\Gal(K/k)$ agisce banalmente sui suoi caratteri (visti come elementi dell'anello di coordinate). Di conseguenza $\sigma(R_m)=R_m\ \ \forall\ \sigma\in\Gal(K/k),\ m\in\Zb^s$. Indichiamo ora con $\widetilde{k}/k$ un'estensione finita di $k$ in cui è definito $r_{e_i}\ \ \forall\ i\le s$. Si ponga $$\widetilde{r}_i:=\prod_{\sigma\in\Gal(\widetilde{k})}\sigma(r_{e_i}).$$Per quanto appena osservato, $\widetilde{r}_i\in R_{e_i}^{d}=R_{de_i}$, dove abbiamo indicato con $d$ il grado dell'estensione $\widetilde{k}/k$. Per costruzione $\widetilde{r}_i$ è un elemento definito su $\widetilde{k}$ che viene lasciato fisso da $\Gal(\widetilde{k}/k)$, quindi è un elemento definito su $k$. Consideriamo adesso la seguente rappresentazione di $N$ definita su $k$: $$\widetilde{V}=\Span(\widetilde{r}_{1},\dots,\widetilde{r}_s,f_1,\dots,f_l).$$

    Questa rappresentazione non è necessariamente fedele, ma possiamo star certi che le fibre devono essere tutte finite: si osserva facilmente che il $\Ker$ di questa rappresentazione è contenuto in $S$ ed è dato da tutti gli elementi $x=(x_1,\dots,x_s)\in S$ tali che $x_i^{d}=1\ \ \forall\ i\le s$, che sono chiaramente in numero finito. Avendo le fibre dimensione nulla, ed essendo l'immagine della rappresentazione contenuta nel toro massimale di $\GL(\widetilde{V})$, con un discorso identico a quello del punto a) si ottiene che $N$ si deve suriettare nel toro massimale di $\GL(\widetilde{V})$. Abbiamo quindi un'inclusione dell'anello di coordinate della $k$-forma del toro massimale di $\GL(\widetilde{V})$ in quello della $k$-forma di $N$. Ma tale inclusione è anche un omomorfismo di algebre di Hopf, quindi per quanto già osservato nei lemmi precedenti manda caratteri in caratteri mantenendo la struttura di reticolo. Questo ci permette di concludere, perché l'immagine del reticolo dei caratteri di $\GL(\widetilde{V})$ sta nell'anello di coordinate della $k$-forma di $N$, ed è un sottoreticolo di dimensione $s+l$ del suo reticolo dei caratteri.
\end{proof}