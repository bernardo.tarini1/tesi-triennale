.PHONY: all clean

all:
	latexmk main.tex

clean:
	latexmk -C
	rm -f main.bbl main.run.xml
