%\makeatletter\let\newcommand\renewcommand\input{size10.clo}
Un quiver $(S,O)$ è un grafo finito $S$ dove sono ammessi loop e più archi tra due stessi vertici, munito di un'orientazione $O$ sugli archi. Chiamiamo $I$ l'insieme dei vertici e $\Omega$ l'insieme (finito) degli archi. Dare una rappresentazione di $S$ su campo $\Fb$ (che indicheremo compattamente con $(V,\phi)$) significa far corrispondere a ciascun vertice $i\in I$ un $\Fb$-spazio vettoriale (finito) $V_i$, e a ciascun arco orientato $h\in\Omega$ un'applicazione lineare $\phi_h$.
Possiamo vedere le rappresentazioni su $\Fb$ di $(S,O)$ come oggetti di una categoria $\Repr(S,O,\Fb)$, dove un morfismo $f:(V,\phi)\rightarrow (U,\psi)$ tra due rappresentazioni è ragionevolmente una collezione di applicazioni $(f_i:V_i\rightarrow U_i)_{i\in I}$ che rispettino le relazioni di commutatività $f_j\circ\phi_h=\psi_h\circ f_i$ per ogni $i,j\in I,\ h\in\Omega$ con $h:i\rightarrow j$.

\smallskip
%A meno di scegliere delle basi possiamo ad esempio pensare le applicazioni lineari come matrici; fissata %la dimensione degli spazi vettoriali su ciascun vertice, per descrivere una rappresentazione non si deve %quindi far altro che specificare delle matrici da associare agli archi. 
Scelte delle applicazioni lineari apparentemente molto diverse, potrebbero in realtà star descrivendo la stessa rappresentazione, a meno di isomorfismo.
%Scelte di matrici apperentemente molto diverse potrebbero in realtà star descrivendo sostanzialmente la %stessa rappresentazione: un cambio di base sugli spazi vettoriali fa sì che le applicazioni lineari %modifichino notevolmente le loro forme matriciali. 
Sorge quindi spontanea la domanda: \virg{È possibile ottenere qualche informazione sulle classi di isomorfismo in $\Repr(S,O,\Fb)$?}

\smallskip
Sarà ragionevole focalizzarci sulle rappresentazioni indecomponibili, cioè quelle non esprimibili come somma diretta non banale di rappresentazioni (e che non siano banali esse stesse). Una qualsiasi rappresentazione di dimensione finita può infatti essere decomposta in finite rappresentazioni indecomponibili (l'insieme degli indecomponibili, contati con molteplicità, non dipende peraltro dalla decomposizione scelta, per il teorema di Krull-Schmidt). Il motto alla base della trattazione che faremo sarà quindi: \virg{Capire le indecomponibili significa capire tutto}. Una rappresentazione di $(S,O)$ su $\Fb$ è detta \textit{assolutamente indecomponibile} se rimane indecomponibile anche in $\Fbb$.

\smallskip
Detto $\Gamma$ il reticolo $\Zb^I$ con base canonica $(\eps_i)_{i\in I}$, e $\Gamma_+=\{\alpha\in\Gamma\setminus\{0\};\ \alpha_i\ge 0\ \ \forall\ i\in I\}$, chiamiamo dimensione di una rappresentazione $V$ il vettore $\dim V=\sum k_i\eps_i\in\Gamma_+$ dove $k_i=\dim V_i$. Con $\mf^\alpha(S,O,\Fb)$ indicheremo lo spazio vettoriale delle rappresentazioni $U$ di dimensione $\alpha$, dove $U_i=\Fb^{\alpha_i}\ \ \forall\ i\in I$. A meno di fissare delle basi, ogni oggetto di $\Repr(S,O,\Fb)$ di dimensione $\alpha$ può essere descritto da un elemento in $\mf^\alpha(S,O,\Fb)$. Inoltre, due rappresentazioni in questo spazio sono isomorfe se e solo se sono coniugate tramite l'azione algebrica degli $\Fb$-punti di $$\GL(\alpha):=\bigoplus_{i\in I}\GL(\alpha_i,\Fbb),$$ descritta da $$(g_i)_{i\in I}\cdot (\phi_h)_{h\in\Omega}=(g_j\circ \phi_h\circ g_i^{-1};\ h:i\rightarrow j)_{h\in\Omega}.$$
%\begin{example}
%Si consideri il quiver di Jordan:
%
%\vspace{0.5cm}\hspace{5cm}
%\begin{tikzpicture}
%\node (A) at (0,0) {$\bullet$};
%\draw[->,shorten <=5pt,shorten >=5pt](.center)arc(-180:180:1);
%\end{tikzpicture}
%
%Al variare della base dello spazio vettoriale, la matrice che scegliamo come rappresentazione cambia tramite coniugio in $\GL(n,\Fb)$, quindi se $\Fb$ è un campo algebricamente chiuso, un sistema di rappresentanti completo per le possibili rappresentazioni a meno di isomorfismo è costituito dalle matrici in forma canonica di Jordan. Se c'è un solo blocco di Jordan allora la rappresentazione è indecomponibile.
%\end{example}

Il risultato principale che presenteremo in questa tesi sarà una caratterizzazione esplicita di tutti i possibili vettori dimensionali di rappresentazioni indecomponibili. Prima di enunciarlo dobbiamo introdurre due ingredienti fondamentali: il \textit{gruppo di Weyl} e il \textit{sistema di radici} associati a $S$. 

Definiamo una forma bilineare $\langle\ ,\ \rangle$ su $\Gamma$, con $$\langle\eps_i,\eps_j\rangle=\delta_{ij}-\#\{h\in\Omega\ h:\ i\rightarrow j\}.$$Sia $(a,b)=\frac{1}{2}(\langle a,b\rangle+\langle b,a \rangle)$ la corrispondente forma simmetrizzata, che non dipende dall'orientazione $O$. Chiamiamo \textit{supporto} di $\alpha\in\Gamma_+$ il sottografo completo di $S$ che ha per vertici tutti gli $i\in I$ per cui $\alpha_i>0.$ L'\textit{altezza} di $\alpha$ sarà il valore $|\alpha|:=\sum \alpha_i$.

Il \textit{gruppo di Weyl} $W$ di $S$ è il gruppo di automorfismi di $\Gamma$ generato dalle riflessioni semplici: se $i\in I$ è senza loop, definiamo \textit{riflessione semplice} di centro $i$ l'involuzione $$s_i(\alpha)=\alpha-2(\alpha,\eps_i)\eps_i\ \ \forall\ \alpha\in\Gamma.$$

Introduciamo ora la seguente notazione:
\begin{itemize}
    \item $\Pi$ è l'insieme degli $\eps_i$ \textit{radici semplici}, cioè tali che $i\in I$ sia senza loop;
    \item $M\subset\Gamma_+$ è il sottoinsieme degli $\alpha$ a supporto connesso per cui $(\alpha,\eps_i)\le0\ \ \forall\ i\in I$, detto \textit{camera fondamentale};
    \item $\widetilde{M}\subset\Gamma_+$ è il sottoinsieme degli $\alpha$ a supporto sconnesso per cui $(\alpha,\eps_i)\le0\ \ \forall\ i\in I$;
    \item $\Delta_+^{re}=W(\Pi)\cap\Gamma_+$ è l'insieme delle \textit{radici reali positive};
    \item $\Delta_+^{im}=W(M)\cap\Gamma_+$ rappresenta l'insieme delle \textit{radici immaginarie positive};
    \item $\Delta_+=\Delta_+^{re}\cup\Delta_+^{im}$ costituisce il \textit{sistema di radici positive} associato a $S$.
\end{itemize}
Il teorema di Kac per campi algebricamente chiusi ci permette di stabilire un sorprendente legame tra i possibili vettori dimensionali delle rappresentazioni indecomponibili e il sistema di radici positive associato a $(S,O)$:
\begin{theorem*}[Kac, 1980]
    Sia $(S,O)$ un quiver e $\alpha\in\Gamma_+$. Si fissi un campo base algebricamente chiuso.
    \begin{itemize}
        \item[a)] se $\alpha\not\in\Delta_+$, allora non esistono rappresentazioni indecomponibili di dimensione $\alpha$.
        \item[b)] se $\alpha\in\Delta_+^{re}$ allora esiste esattamente una rappresentazione indecomponibile di dimensione $\alpha$, a meno di isomorfismo.
        \item[c)] se $\alpha\in\Delta_+^{im}$ allora esistono infinite classi di isomorfismo di rappresentazioni indecomponibili di dimensione $\alpha$.\footnote{In realtà per campi base in caratteristica $0$ in questa tesi dimostreremo solo l'esistenza di almeno una rappresentazione indecomponibile.}
    \end{itemize}
    Si osservi che tutti e tre gli statement non dipendono dall'orientazione $O$.
\end{theorem*}
Partendo dalle osservazioni facili, è banale notare che per $\alpha\in\Pi$ esiste un'unica rappresentazione indecomponibile, e che tutte le rappresentazioni di dimensione $\alpha\in\widetilde{M}$ sono decomponibili. Con un po' più di lavoro, ma con un approccio abbastanza elementare, si riesce a trattare anche il caso di $\alpha\in M$ (rimandiamo a \cite{[8]} per la trattazione del caso particolare di supporto Euclideo), mediante un argomento di tipo dimensionale (vedasi sezione \eqref{crawy}) che possiamo riassumere nel seguente lemma:
\begin{lemma*}
    Se $\alpha\in M$ e $\Fb$ è algebricamente chiuso, allora esiste un aperto non banale di $\mf^\alpha(S,O,\Fb)$ composto da rappresentazioni indecomponibili.
\end{lemma*}

Un generico $\alpha\in\Gamma_+$ è coniugato tramite $W$ a una dimensione di $\Pi,M,$ o $\widetilde{M}$: se $\alpha\not\in\Delta_+^{re}\cup\Delta_+^{im}$, un qualsiasi $\beta\in W\cdot \alpha$ di altezza minima deve stare in $\widetilde{M}$. Sarebbe quindi estremamente utile trovare un collegamento tra gli indecomponibili in $\mf^\alpha(S,O,\Fb)$ e quelli in $\mf^{w(\alpha)}(S,O,\Fb)$, dove $w\in W$.

In quest'ottica sarà chiara l'estrema importanza dei funtori di riflessione, di cui parleremo nella sezione \eqref{reflex}: nel caso in cui $S$ abbia un vertice $i$ che è \textit{pozzo o sorgente} rispetto all'orientazione $O$ - cioè con solo archi uscenti o entranti - è possibile definire un funtore da $\Repr(S,O,\Fb)$ in $\Repr(S,r_i(O),\Fb)$, dove l'orientazione $r_i(O)$ si ottiene capovolgendo gli archi che coinvolgono $i$. I funtori di riflessione hanno il pregio di creare una corrispondenza biunivoca tra le rappresentazioni indecomponibili di dimensione $\alpha$ di $(S,O)$ e quelle di dimensione $s_i(\alpha)$ di $(S,r_i(O))$ (se $\alpha\ne\eps_i$).

Non dobbiamo farci subito prendere dall'entusiasmo in quanto a priori un'orientazione $O$ su $S$ potrebbe non generare nemmeno un pozzo o una sorgente. La chiave di volta sarà distogliere momentaneamente l'attenzione dai campi algebricamente chiusi per concentrarci sui campi finiti: su questi riusciremo a far vedere che il numero di indecomponibili e assolutamente indecomponibili non dipende dall'orientazione su $S$. Potremo così sceglierla liberamente in modo da poter applicare i funtori di riflessione che vogliamo e dimostrare il teorema di Kac per campi finiti:
\begin{theorem*}
    Sia $(S,O)$ un quiver, $\alpha\in\Gamma_+$. Se il campo base delle rappresentazioni è un campo finito $\Fb_q$ ($q=p^t)$ allora valgono le seguenti:
    \begin{itemize}
        \item[a)] Il numero di indecomponibili in $\mf^\alpha(S,O,\Fb_q)$ (a meno di isomorfismo) non dipende dall'orientazione $O$. Se $i$ è senza loop e $\alpha\ne\eps_i$, allora gli indecomponibili di dimensione $\alpha$ corrispondono in numero a quelli di dimensione $s_i(\alpha)$ (nello specifico non ce ne sono se $s_i(\alpha)\not\in\Gamma_+$).
        \item[b)] Se $\alpha\in\Delta_+^{re}$, esiste un'unica rappresentazione indecomponibile di dimensione $\alpha$.
        \item[c)] Se $\alpha\not\in\Delta_+$ allora non esistono rappresentazioni indecomponibili di dimensione $\alpha$.
    \end{itemize}
\end{theorem*}
Per mostrare l'indipendenza dall'orientazione del numero di indecomponibili sarà comodo tradurre la nozione di indecomponibilità di $x\in\mf^{\alpha}(S,O,\Fb_q)$ in termini dell'$\Fb_q$-rango riduttivo di $(\GL(\alpha))_x$, si veda \eqref{006} e \eqref{007}. A questo punto modificheremo opportunamente $O$ in modo da poter applicare un qualsiasi funtore di riflessione; il teorema di Kac su campi finiti sarà una conseguenza di queste osservazioni chiave. Ci sarà inoltre utile ottenere dei risultati simili per le rappresentazioni assolutamente indecomponibili su $\Fb_q$, per i quali saranno necessari degli argomenti di teoria di Galois, si veda sezione \eqref{Gal}.

Tornando ai campi algebricamente chiusi, potremo finalmente mostrare l'esistenza di indecomponibili di dimensione $\alpha\in\Delta_+$ anche su campo $\Fpp$, e più in generale per campi algebricamente chiusi di caratteristica finita. Per far ciò su campi algebricamente chiusi in caratteristica $0$ sfrutteremo un po' di teoria degli schemi.

Anche per dimostrare la decomponibilità delle rappresentazioni di dimensione $\alpha\not\in\Delta_+$ su $\Fb=\Fbb$ tornerà estremamente utile il risultato nel caso dei campi finiti: la nozione di indecomponibilità di una rappresentazione $(V,\phi)$ su $\Fb$ può essere formulata in modo equivalente dicendo che $V$ non ammette morfismi di proiezione su sottospazi propri, ovvero che il sistema di equazioni con matrici di indeterminate
\begin{equation}\label{0492}
    X_{j}\phi_h=\phi_h X_{i}\ \ \ \forall\ h:\ i\rightarrow j,\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ X_i^2=X_i\ \ \ \forall\ i\in I
\end{equation}
non ha soluzioni in $\Fb$ diverse dalle banali $(0,\dots,0),\ (\Id_{V_1},\dots,\Id_{V_n}).$ Per il Nullstellensatz ciò significa che l'ideale associato a questi due punti è uguale all'ideale generato dalle equazioni \eqref{0492}. Supporre per assurdo che esista una rappresentazione indecomponibile di dimensione $\alpha\not\in\Delta_+$ comporterebbe, mediante un argomento di specializzazione, l'esistenza di una rappresentazione indecomponibile di dimensione $\alpha$ su un campo finito opportuno.


\bigskip

Veniamo adesso alla seconda parte della tesi. 
Dato un grafo $S$ a $n$ vertici senza loop, gli associamo una matrice $A$ di dimensione $n$ tale che $$A_{ij}=2\delta_{ij}-\#\{h\in\Omega\ \text{con coppia di estremi }(i,j)\}.$$
La matrice $A$ così definita è una matrice di Cartan simmetrica, a partire dalla quale si può costruire un'algebra di Lie $G(A)$, detta \virg{algebra di Kac-Moody}. Essa è costruita a partire da una terna di sottospazi $G_{-1}\oplus G_0\oplus G_1$ con rispettive basi $(f_i)_{i\le n},\ (h_i)_{i\le n},\ (e_i)_{i\le n}$ nei quali valgono le relazioni $$[e_if_j]=\delta_{ij}e_i,\ \ \ [h_ie_j]=A_{ij}e_j,\ \ \ [h_if_j]=-A_{ij}f_j,\ \ \ [h_ih_j]=0\ \ \ \ \ \ \forall\ i,j\le n.$$ Nel capitolo \eqref{4chap} creeremo le basi per una definizione formale di $G(A)$, mentre nel capitolo \eqref{5chap} andremo a studiarne alcune proprietà: l'obiettivo principale sarà stabilire una corrispondenza tra le dimensioni delle rappresentazioni indecomponibili di $(S,O)$ su campi algebricamente chiusi ($O$ è un'orientazione qualsiasi), e gli \textit{spazi radice positivi} di $G(A)$. Nel caso in cui $A$ sia non degenere, gli spazi radice $G_\alpha(A)$ sono tutti i sottospazi non banali della forma $$G_\alpha(A):=\{x\in G(A):\ [hx]=\alpha(h)x\ \ \forall\ h\in G_0\},\ \ \alpha\in\Gamma\setminus\{0\}.$$
Nella tesi daremo una definizione di radici reali e immaginarie per le algebre di Kac-Moody, che dimostreremo essere coerente con la definizione data nel contesto dei quiver.

La conclusione principale a cui giungeremo è riassumibile nel seguente teorema:
\begin{theorem*}
Sia $(S,O)$ un quiver senza loop, $A$ la matrice di Cartan simmetrica associata, e $\alpha\in\Gamma_+$. Sia inoltre $\Fb$ un campo algebricamente chiuso. Allora vale che:
\begin{itemize}
    \item[] $\mf^\alpha(S,O,\Fb)$ contiene rappresentazioni indecomponibili se e solo se $G_\alpha(A)$ è uno spazio radice, cioè $G_\alpha(A)\ne 0$.
    %\item[b)] Se $\mf^\alpha(S,O,\Fb)$ contiene una e una sola classe di isomorfismo di indecomponibili, allora $G_\alpha(A)$ è uno spazio radice reale. In particolare $\dim_{\Rb}G_\alpha(A)=1$.
    %\item[c)] Se $\mf^\alpha(S,O,\Fb)$ contiene infinite rappresentazioni indecomponibili non isomorfe allora $G_\alpha(A)$ è uno spazio radice immaginario.
\end{itemize}
\end{theorem*}
%$$[e_if_j]=\delta_{ij}e_i,\ \ \ \ \ [h_ie_j]=A_{ij}=-[h_if_j],\ \ \ \ \ [h_ih_j]=0\ \ \forall\ i,j\le n.$$
%Possiamo vedere gli elementi di $\Gamma$ anche come elementi del duale di $G_0$, tramite l'omomorfismo %$\Gamma\rightarrow (G_0)^*$ definito da $$\eps_i(h_j)=A_{ji}.$$
%Dove abbiamo posto $G_0=G_0$, $G_\alpha=0$ se $\alpha\not\in \{0\}\cup \Gamma_+\cup\Gamma_-$, e
%$$G_\alpha=\Span([e_{i_1}e_{i_2}\cdots e_{i_k}];\ \sum_{l=1}^k \eps_{i_l}=\alpha)\ \ \forall\ 
%\alpha\in\Gamma_+.$$$$G_{\alpha}=\Span([f_{i_1}f_{i_2}\cdots f_{i_k}];\ \sum_{l=1}^k \eps_{i_l}=\alpha)\ \ %\forall\ \alpha\in\Gamma_-.$$
%Denotiamo con $\Delta\subset\Gamma$ il \textit{sistema di radici} di $G(A)$, ovvero l'insieme degli %$\alpha\in\Gamma\setminus\{0\}$ tali che $G_{\alpha}\ne 0.$
%Un fatto base che vedremo sarà l'invarianza di $\Delta$ tramite azione del gruppo di Weyl. Indichiamo con %$\Pi$ l'insieme delle \textit{radici semplici}, ovvero degli $\eps_i$ con $i$ tale che $A_{ii}=2.$ In modo %del tutto analogo a quanto fatto per i quiver definiamo la \textit{camera fondamentale} $M\subset\Gamma_+$ %come l'insieme degli $\alpha$ a supporto connesso nel grafo $S(A)$, tali che $\alpha(h_i)\ge 0\ \ \forall\ %i\le n$. Poniamo poi $\Delta^{re}:=W(\Pi)$ e $\Delta^{im}:=\Delta\setminus\Delta^{re}.$ \

%\bigskip

%La teoria delle rappresentazioni di quiver permette di formulare con un linguaggio uniforme una vasta gamma di problemi di algebra lineare. 
Guardare al sistema di radici di $S$ con questo nuovo punto di vista, oltre ad essere interessante di per sé, metterà in luce alcune sue proprietà strutturali, che sarebbero altrimenti state tutt'altro che scontate.